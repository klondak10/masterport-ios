//
//  AdsDetailViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class AdsDetailViewController: UIViewController {

    @IBOutlet weak var offerYourServicesBtn: UIButton!
    
    @IBOutlet weak var adsCardView: FJCustomLayerView!
    
    @IBOutlet weak var detailAdress: UILabel!
    @IBOutlet weak var endDateField: UITextField!
    @IBOutlet weak var gmail: UILabel!
    @IBOutlet weak var sumOfOrderField: UITextField!
    @IBOutlet weak var startDateField: UITextField!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var shortDescriptionTextView: UITextView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    @IBAction func offerSevices(_ sender: Any) {
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Название обьявления"
            
        offerYourServicesBtn.layer.cornerRadius = 16
        // Do any additional setup after loading the view.
    }
}
