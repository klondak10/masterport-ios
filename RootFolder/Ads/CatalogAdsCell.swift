//
//  CatalogAdsCell.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CatalogAdsCell: UITableViewCell {

    @IBOutlet weak var dateCatalogBtn: UIButton!
    @IBOutlet weak var dateCatalogLabel: UILabel!
    @IBOutlet weak var textAdsView: UITextView!
    @IBOutlet weak var summLabel: UILabel!
    @IBOutlet weak var adsNameLabel: UILabel!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dateCatalogBtn.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func dateCatalog(_ sender: UIButton) {
        
        if UserDefaults.standard.bool(forKey: "Requests") {
            
            NotificationCenter.default.post(name: RequestsTabViewController.notificationName, object: nil, userInfo:["RequestNotification": true])
     
        }
        else
        {
            NotificationCenter.default.post(name: AdsTabViewController.notificationName, object: nil, userInfo:["ads": true])
        }
      
    }
}
