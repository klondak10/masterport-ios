//
//  AdsTabViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class AdsTabViewController: UIViewController, UIScrollViewDelegate  {
    static let notificationName = Notification.Name("adsNotification")
    
    var isCatalogOpen:Bool?
    var isMyAdsOpen:Bool?
    
    @IBOutlet weak var adsScrollView: UIScrollView!
    @IBOutlet weak var adsBarView: UIView!
    @IBOutlet weak var myAdsButton: UIButton!
    @IBOutlet weak var catalogButton: UIButton!
    @IBOutlet weak var underline: UIView!
    
    @IBAction func loadAdsView(sender: UIButton) {
        
        _ = underline.frame.width
        
        switch sender.tag {
        case 0:
            self.scrollToPage(page: 0, animated: true)
        case 1:
            self.scrollToPage(page: 1, animated: true)
            
        default: break
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let barButtonItem = UIBarButtonItem(image: UIImage(named: "adjust"),
                                            style: .plain,
                                            target: self,
                                            action: #selector(filterButtonTapped))
        
        self.navigationItem.rightBarButtonItem = barButtonItem
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationItem.title = "Обьявления"
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: AdsTabViewController.notificationName, object: nil)
        
        adsScrollView.delegate = self
        self.addRightButton()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setScrollViewContent()
        UserDefaults.standard.set(false, forKey: "Requests") //Bool
    }
    
    func addRightButton(){
        
        let filterImage = UIImage(named: "filter")?
            .scaleTo(CGSize(width: 20, height: 20))
        
        let btnFilter = UIButton.init(type: .custom)
        btnFilter.setImage(filterImage, for: .normal)
        btnFilter.addTarget(self, action: #selector(filter), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem(customView: btnFilter)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    
    // Private action
    @objc fileprivate func filterButtonTapped() {
        
        let adsFilterVC = AdsFilterViewController(nibName: "AdsFilterViewController", bundle: nil)
         self.navigationController?.pushViewController(adsFilterVC, animated: true)

    }
    
    @objc func done() { // remove @objc for Swift 3
        
    }
    
    
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = adsScrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        adsScrollView.scrollRectToVisible(frame, animated: animated)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        
        if (pageIndex < 1)
        {
            myAdsButton.titleLabel?.textColor = UIColor.darkGreen
            catalogButton.titleLabel?.textColor = UIColor.lightGrayText
            
            underline.frame = CGRect(x: myAdsButton.center.x-underline.frame.width/2, y: underline.frame.origin.y, width:underline.frame.width, height: underline.frame.height)
            
        }
        else {
            
            myAdsButton.titleLabel?.textColor = UIColor.lightGrayText
            catalogButton.titleLabel?.textColor = UIColor.darkGreen
            
            underline.frame = CGRect(x: catalogButton.center.x-underline.frame.width/2, y: underline.frame.origin.y, width: underline.frame.width, height: underline.frame.height)
        }
    }
    
    @objc func filter()
    {
            let filterVC = AdsFilterViewController(nibName: "AdsFilterViewController", bundle: nil)
            self.navigationController?.pushViewController(filterVC, animated: true)
    }

    
    func sort()
    {
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("end scroll")
    }
    
    @objc func onNotification(notification:Notification)
    {
        let adsDetailViewController = AdsDetailViewController(nibName: "AdsDetailViewController", bundle: nil)
        self.navigationController?.pushViewController(adsDetailViewController, animated: true)
    }
    
    
    func setScrollViewContent() {
        
        let myAdsVC = MyAdsController(nibName: "MyAdsController", bundle: nil)
        
        // catalog tab
        let catalogAdsVC = CatalogViewController(nibName: "CatalogViewController", bundle: nil)
        
        let xCenterOrigin = self.view.frame.width
        
        // my ads tab
        myAdsVC.view.frame = CGRect(x:0, y: 0, width:adsScrollView.bounds.width, height: adsScrollView.bounds.height)
        
        // catalog tab
        catalogAdsVC.view.frame = CGRect(x: xCenterOrigin , y: 0, width:adsScrollView.bounds.width, height: adsScrollView.bounds.height)
        
        adsScrollView.addSubview(catalogAdsVC.view)
        adsScrollView.addSubview(myAdsVC.view)
        
        adsScrollView.contentSize = CGSize(width: self.view.frame.width * 2, height: self.adsScrollView.frame.height)
        
        // hide the scrol bar.
        adsScrollView?.showsHorizontalScrollIndicator = false
    }
}







