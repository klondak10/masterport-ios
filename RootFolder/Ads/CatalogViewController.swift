//
//  CatalogViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/26/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CatalogViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var catalogAdsTableView: UITableView!
    
    @IBOutlet weak var chooseCityAdsLabel: UILabel!
    @IBOutlet weak var chooseServiceAdsLabel: UILabel!
    
    @IBAction func chooseCity(_ sender: UIButton) {
        
    }
    
    @IBAction func chooseService(_ sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        catalogAdsTableView.delegate = self
        catalogAdsTableView.dataSource = self

        let nib = UINib.init(nibName: "CatalogAdsCell", bundle: nil)
        catalogAdsTableView.register(nib, forCellReuseIdentifier: "CatalogAdsCell")
    }
    

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = catalogAdsTableView.dequeueReusableCell(withIdentifier: "CatalogAdsCell", for: indexPath) as! CatalogAdsCell
        
        return cell
    }
}
