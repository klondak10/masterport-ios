//
//  AdsFilterViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class AdsFilterViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    let data: [String] = ["Город", "Услуга", "Стоимость", "Поиск по дате добавления", "Сроки выполнения"]
    
    let imData: [String] = ["pin-green", "list-green", "wallet-green", "calendar-green", "time-green"]
    
    @IBOutlet var tableList : UITableView!
    @IBOutlet var showResultsButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // the UIButton is simply there to capture touch up event on the entire bar button view.
        let button = UIButton.init(type: .custom)
        button.addTarget(self, action: #selector(closeFilter), for: .touchUpInside)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 7, width: 16, height: 16))
        imageView.image = UIImage(named: "close")
        let label = UILabel(frame: CGRect(x: 35, y: 0, width: 80, height: 30))
        label.text = "Фильтр"
        label.textColor = UIColor.white
        label.font = UIFont(name:"CustomFont-franklin", size: 20.0)
        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 30))
        button.frame = buttonView.frame
        buttonView.addSubview(button)
        buttonView.addSubview(imageView)
        buttonView.addSubview(label)
        buttonView.backgroundColor = UIColor.clear
        let barButton = UIBarButtonItem.init(customView: buttonView)
        self.navigationItem.leftBarButtonItem = barButton

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Очистить", style: .plain, target: self, action: #selector(cleanFilter))
        
        
        showResultsButton.layer.cornerRadius = 16
       // self.navigationItem.title = "Фильтр"
        
        
        let nib = UINib(nibName: "AdsFilterTableViewCell", bundle: nil)
        tableList.register(nib, forCellReuseIdentifier: "AdsFilterTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: AdsFilterTableViewCell = tableList?.dequeueReusableCell(withIdentifier: "AdsFilterTableViewCell") as! AdsFilterTableViewCell
        
        cell.filterAdsName?.text = self.data[indexPath.row]
        
        let imageString = self.imData[indexPath.row]
        cell.filterAdsImage.image  = UIImage(named: imageString)
        
        return cell
    }
    
    
    @IBAction func showFilterResults(sender: UIButton) {
        
    }
    
    @objc func cleanFilter()
    {
        
    }
    
    @objc func closeFilter ()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

