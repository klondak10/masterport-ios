//
//  AdsFilterTableViewCell.swift
//  Masterovik
//
//  Created by Ирина on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class AdsFilterTableViewCell: UITableViewCell {
   
    @IBOutlet weak var filterAdsImage: UIImageView!
    @IBOutlet weak var filterAdsName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        filterAdsName.tintColor = UIColor(named: "#505050")
        //        performedButton.titleLabel?.textColor = UIColor(named: "#505050")
        //
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
