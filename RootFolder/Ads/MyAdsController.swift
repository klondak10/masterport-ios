//
//  MyAdsController.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class MyAdsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
        @IBOutlet weak var cityLabel: UILabel!
        @IBOutlet weak var serviceView: FJCustomLayerView!
    
        @IBOutlet weak var closeServiceButton: UIButton!
    
        @IBOutlet weak var adsServiceView: FJCustomLayerView!
        @IBOutlet weak var addServiceView: FJCustomLayerView!
        @IBOutlet weak var addServiceButton: UIButton!
    
        @IBOutlet weak var myAdsTableView: UITableView!
    
    @IBAction func addAdsButton(_ sender: Any) {
        
    }
    
    @IBAction func closeServiceAction(_ sender: Any) {
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myAdsTableView.delegate = self
        myAdsTableView.dataSource = self
        
        let nib = UINib.init(nibName: "CatalogAdsCell", bundle: nil)
        myAdsTableView.register(nib, forCellReuseIdentifier: "CatalogAdsCell")
        
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myAdsTableView.dequeueReusableCell(withIdentifier: "CatalogAdsCell", for: indexPath) as! CatalogAdsCell
        
        return cell
    }
}
