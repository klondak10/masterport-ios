//
//  OldFeedbackViewController.swift
//  Masterovik
//
//  Created by Ирина on 5/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OldFeedbackViewController: UIViewController {
    
    @IBOutlet weak var sendToAdmin: UIButton!
    @IBOutlet weak var topicTextView: UITextView!
    
    @IBOutlet weak var textLetterTextView: UITextView!
    
    @IBAction func sendToAdmin(sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendToAdmin.layer.cornerRadius = 16
        topicTextView.layer.cornerRadius = 5
        topicTextView.layer.borderColor  = UIColor.lightGray.cgColor
        topicTextView.layer.borderWidth = 1.0
        textLetterTextView.layer.borderWidth = 1.0
        textLetterTextView.layer.cornerRadius = 5
        textLetterTextView.layer.borderColor  = UIColor.lightGray.cgColor
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(hideKeyboard))
        
        toolBar.setItems([doneButton], animated: true)
        topicTextView.inputAccessoryView = toolBar
        
        textLetterTextView.inputAccessoryView = toolBar
    }
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
}
