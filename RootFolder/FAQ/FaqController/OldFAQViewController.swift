//
//  OldFAQViewController.swift
//  Masterovik
//
//  Created by Ирина on 5/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OldFAQViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var faqTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        faqTableView.delegate = self
        faqTableView.dataSource = self
        let nib = UINib.init(nibName: "FAQTableViewCell", bundle: nil)
        faqTableView.register(nib, forCellReuseIdentifier: "FAQTableViewCell")
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = faqTableView.dequeueReusableCell(withIdentifier: "FAQTableViewCell", for: indexPath) as! FAQTableViewCell
           
        return cell
    }
}
