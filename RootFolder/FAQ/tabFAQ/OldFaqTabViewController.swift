//
//  FaqTabViewController.swift
//  Masterovik
//
//  Created by Ирина on 5/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OldFaqTabViewController: UIViewController , UIScrollViewDelegate  {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var menuBarView: FJCustomLayerView!
    @IBOutlet weak var faqButton: UIButton!
    @IBOutlet weak var feedbackButton: UIButton!
    
    @IBOutlet weak var underline: UIView!
    
    @IBAction func loadFaqView(sender: UIButton) {
        
         self.view.endEditing(true)
        
        _ = underline.frame.width
        
        switch sender.tag {
        case 0:
            self.scrollToPage(page: 0, animated: true)
        case 1:
            self.scrollToPage(page: 1, animated: true)
            
        default: break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationItem.title = "FAQ"
        
        scrollView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setScrollViewContent()
    }
 
    
    func setScrollViewContent() {
       
        // FAQVC - done tab
        let FAQVC = FAQViewController(nibName: "FAQViewController", bundle: nil)
    
        // FeedbackVC - done tab
        let FeedbackVC = FeedbackViewController(nibName: "FeedbackViewController", bundle: nil)
     
        scrollView.addSubview(FAQVC.view)
        scrollView.addSubview(FeedbackVC.view)
        
        let xCenterOrigin = self.view.frame.width
        
        // FAQVC tab
        FAQVC.view.frame = CGRect(x:0, y: 0, width:scrollView.bounds.width, height: scrollView.bounds.height)
        
        // FeedbackVC tab
        FeedbackVC.view.frame = CGRect(x: xCenterOrigin , y: 0, width:self.view.frame.width, height: scrollView.bounds.height)
     
        self.scrollView.contentSize = CGSize(width: self.view.frame.width * 2, height: self.scrollView.frame.height)
        
        // hide the scrol bar.
        scrollView?.showsHorizontalScrollIndicator = false
    }
    
    
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: animated)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        
        if (pageIndex < 1)
        {
            faqButton.titleLabel?.textColor = UIColor.darkGreen
            feedbackButton.titleLabel?.textColor = UIColor.lightBlack
            
            underline.frame = CGRect(x: faqButton.center.x-underline.frame.width/2, y: underline.frame.origin.y, width:underline.frame.width, height: underline.frame.height)
            
        }
        else {
            
            faqButton.titleLabel?.textColor = UIColor.lightBlack
            feedbackButton.titleLabel?.textColor = UIColor.darkGreen
            
            underline.frame = CGRect(x: feedbackButton.center.x-underline.frame.width/2, y: underline.frame.origin.y, width: underline.frame.width, height: underline.frame.height)
        }
        
    }

}






