//
//  CurrentOrderCell.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CurrentOrderCell: UITableViewCell {
    @IBOutlet weak var openOrderButton: UIButton!
    @IBOutlet weak var nameOrderLabel: UILabel!
    @IBOutlet weak var personDataLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var changesInputView: UIView!
    @IBOutlet weak var dateOfSentenceLabel: UILabel!
   
    @IBAction func openOrderCardView(sender: UIButton) {
        NotificationCenter.default.post(name: OrdersTabViewController.notificationName, object: nil, userInfo:["data": 42, "isImportant": true])
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
