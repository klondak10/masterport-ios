//
//  CurrentOrderViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CurrentOrderViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var currentTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
  
        let nib = UINib.init(nibName: "CurrentOrderCell", bundle: nil)
        currentTableView.register(nib, forCellReuseIdentifier: "CurrentOrderCell")
        currentTableView.delegate = self
        currentTableView.dataSource = self
        
        currentTableView.isScrollEnabled = false
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = currentTableView.dequeueReusableCell(withIdentifier: "CurrentOrderCell", for: indexPath) as! CurrentOrderCell

        return cell
    }
    
}


