//
//  FilterViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController, UITableViewDelegate , UITableViewDataSource{
    
    let data: [String] = ["Город", "Услуга", "Стоимость", "Поиск по заказу"]
    
    let imData: [String] = ["pin-green", "list-green", "wallet-green", "seacrh-green"]
    
    
    @IBOutlet var tblList : UITableView!
    @IBOutlet var showResultsButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        // the UIButton is simply there to capture touch up event on the entire bar button view.
        let button = UIButton.init(type: .custom)
        button.addTarget(self, action: #selector(closeFilter), for: .touchUpInside)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 7, width: 16, height: 16))
        imageView.image = UIImage(named: "close")
        let label = UILabel(frame: CGRect(x: 35, y: 0, width: 80, height: 30))
        label.text = "Фильтр"
        label.textColor = UIColor.white
        label.font = UIFont(name:"CustomFont-franklin", size: 20.0)
        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 30))
        button.frame = buttonView.frame
        buttonView.addSubview(button)
        buttonView.addSubview(imageView)
        buttonView.addSubview(label)
        buttonView.backgroundColor = UIColor.clear
        let barButton = UIBarButtonItem.init(customView: buttonView)
        self.navigationItem.leftBarButtonItem = barButton
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Очистить", style: .plain, target: self, action: #selector(cleanFilter))
        
        showResultsButton.layer.cornerRadius = 16
        
        let nib = UINib(nibName: "FilterOrderCellData", bundle: nil)
        tblList.register(nib, forCellReuseIdentifier: "FilterOrderCellData")
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: FilterOrderCellData = tblList?.dequeueReusableCell(withIdentifier: "FilterOrderCellData") as! FilterOrderCellData
        
        cell.filterName?.text = self.data[indexPath.row]
        
        let imageString = self.imData[indexPath.row]
        cell.filterImage.image  = UIImage(named: imageString)
        
        return cell
    }
    
    
    @IBAction func showFilterResults(sender: UIButton) {
        
    }
    
    @objc func cleanFilter()
    {
        
    }
    
    @objc func closeFilter ()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
