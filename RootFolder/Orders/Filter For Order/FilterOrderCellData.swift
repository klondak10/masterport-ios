//
//  FilterOrderCellData.swift
//  Masterovik
//
//  Created by Ирина on 4/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class FilterOrderCellData: UITableViewCell {

   @IBOutlet weak var filterImage: UIImageView!
    @IBOutlet weak var filterName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
