//
//  LeftOrderViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class LeftOrderViewController: UIViewController {
    
    @IBOutlet weak var editSummButton: UIButton!
    @IBOutlet weak var editDateButton: UIButton!
    @IBOutlet weak var editingOrderView: UIView!
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var leaveComplaintButton: UIButton!
    @IBOutlet weak var completeOrderButton: UIButton!
    @IBOutlet weak var editDataStart: UITextField!
    @IBOutlet weak var editDataEnd: UITextField!
    @IBOutlet weak var editMoneyValue: UITextField!
    @IBOutlet weak var summOfOrder: UITextField!
    @IBOutlet weak var openProfileBtn: UIButton!
    
    @IBAction func openProfile(_ sender: Any) {
        
        let profileOrderVC = ProfileOrderViewController(nibName: "ProfileOrderViewController", bundle: nil)
        self.navigationController?.pushViewController(profileOrderVC, animated: true)
        
    }
    
    @IBAction func editDateAction(_ sender: Any) {
        
        let editDateVc = EditDateViewController(nibName: "EditDateViewController", bundle: nil)
        self.navigationController?.pushViewController(editDateVc, animated: true)
    }
    
    @IBAction func editMoneyValue(_ sender: Any) {
        
        let editSummVC = EditSummViewController(nibName: "EditSummViewController", bundle: nil)
        self.navigationController?.pushViewController(editSummVC, animated: true)
    }
    
    @IBAction func completeOrder(_ sender: Any) {
    }
    
    @IBAction func leaveComplaint(_ sender: Any) {
        
        let complaintViewController = ComplaintViewController(nibName: "ComplaintViewController", bundle: nil)
        self.navigationController?.pushViewController(complaintViewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        completeOrderButton.layer.cornerRadius = 16
        leaveComplaintButton.layer.cornerRadius = 16
        
        self.navigationItem.title = "Название заказа"
        
    }
}
