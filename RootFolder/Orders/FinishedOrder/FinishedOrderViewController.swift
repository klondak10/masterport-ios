//
//  FinishedOrderViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class FinishedOrderViewController: UIViewController {
    
    @IBOutlet weak var editingOrderView: UIView!
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var leaveComplaintButton: UIButton!
 
    @IBOutlet weak var openProfileBtn: UIButton!
    

    
    @IBAction func leaveComplaint(_ sender: Any) {
        
        let complaintViewController = ComplaintViewController(nibName: "ComplaintViewController", bundle: nil)
        self.navigationController?.pushViewController(complaintViewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
    }
}

