//
//  RatingOrderCard.swift
//  Masterovik
//
//  Created by Ирина on 4/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class RatingOrderCard: UIView {

    @IBOutlet weak var ratingOrderView: UIView!
    
    @IBOutlet weak var nameOrderLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var valueRatingLabel: UILabel!
  

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        loadNib()
    }
    
    func loadNib() {
        
        UINib(nibName: "RatingOrderCard", bundle: nil).instantiate(withOwner: self, options: nil)
        addSubview(ratingOrderView)
        ratingOrderView.frame = self.bounds
        
//        self.layer.cornerRadius = 5
//        self.layer.shadowColor = UIColor.lightGray.cgColor
//        self.layer.shadowOffset = CGSize(width: 2, height: 2)
//        self.layer.shadowOpacity = 0.85
//        
//        ratingOrderView.layer.cornerRadius = 5
        
    }

}
