//
//  ComplaintViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ComplaintViewController: UIViewController {

    @IBOutlet weak var complaintView: UIView!
    @IBOutlet weak var statusComplaintLabel: UILabel!
    @IBOutlet weak var personNameLabel: UILabel!
    @IBOutlet weak var complaintOrderLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cityImageView: UIImageView!
    
    @IBOutlet weak var shortDescriptionLabel: UILabel!
    @IBOutlet weak var fullDescriptionLabel: UILabel!
    @IBOutlet weak var shortDescriptionTexView: UITextView!
    @IBOutlet weak var fullDescriptionTextView: UITextView!
    @IBOutlet weak var sendLetterToAdminBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Жалоба"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.doneClicked))
        
        toolBar.setItems([doneButton], animated: true)
        shortDescriptionTexView.inputAccessoryView = toolBar
        
        fullDescriptionTextView.inputAccessoryView = toolBar
        fullDescriptionTextView.layer.borderWidth = 1;
        fullDescriptionTextView.layer.cornerRadius = 5; fullDescriptionTextView.layer.borderColor =
            UIColor.lightGrayText.cgColor
   
        sendLetterToAdminBtn.layer.cornerRadius = 5
        complaintView.layer.cornerRadius = 5
        complaintView.layer.shadowColor =  UIColor.lightGrayText.cgColor
        complaintView.layer.shadowOffset = CGSize(width: 2, height: 2)
        complaintView.layer.shadowOpacity = 0.85
        
        
        fullDescriptionTextView.layer.cornerRadius = 5.0
        fullDescriptionTextView.layer.borderWidth = 1.0
        fullDescriptionTextView.layer.borderColor = UIColor.lightGrayText.cgColor
        shortDescriptionTexView.layer.borderColor = UIColor.lightGrayText.cgColor
        shortDescriptionTexView.layer.cornerRadius = 5.0
        shortDescriptionTexView.layer.borderWidth = 1.0
        sendLetterToAdminBtn.layer.cornerRadius = 16
    }
    
    @objc func doneClicked(){
        view.endEditing(true)
    }
}
