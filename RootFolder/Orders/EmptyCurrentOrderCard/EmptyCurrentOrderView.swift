//
//  EmptyCurrentOrderView.swift
//  Masterovik
//
//  Created by Ирина on 4/21/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class EmptyCurrentOrderView: UIView {

    @IBOutlet weak var emptyCurrentView: UIView!
    @IBOutlet weak var emptyCurrentImageView: UIImageView!
    @IBOutlet weak var emptyCurrentLabel: UILabel!
     @IBOutlet weak var adsButton: UIButton!
    
    @IBAction func openAds(sender: UIButton) {
      
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
//        emptyCurrentImageView.tintColor  = .green
    }
}
