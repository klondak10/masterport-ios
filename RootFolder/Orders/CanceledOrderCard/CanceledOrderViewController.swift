//
//  CanceledOrderViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CanceledOrderViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var cancelTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelTableView.delegate = self
        cancelTableView.dataSource = self
        
        let nib = UINib.init(nibName: "CancelOrderCell", bundle: nil)
        cancelTableView.register(nib, forCellReuseIdentifier: "CancelOrderCell")
    
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = cancelTableView.dequeueReusableCell(withIdentifier: "CancelOrderCell", for: indexPath) as! CancelOrderCell

        return cell
    }
}
