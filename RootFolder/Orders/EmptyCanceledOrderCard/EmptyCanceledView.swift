//
//  EmptyCanceledView.swift
//  Masterovik
//
//  Created by Ирина on 4/21/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class EmptyCanceledView: UIView {

    @IBOutlet weak var emptyCancelView: UIView!
    @IBOutlet weak var emptyCancelImageView: UIImageView!
    @IBOutlet weak var emptyCancelLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }

}
