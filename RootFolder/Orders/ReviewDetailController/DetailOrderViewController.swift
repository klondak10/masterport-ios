//
//  DetailOrderViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class DetailOrderViewController: UIViewController {
    
    @IBOutlet weak var ratingOrderCard: UIView!
    
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var reviewDateLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var goToOrderButton: UIButton!
    @IBOutlet weak var leaveComplaintButton: UIButton!
    @IBOutlet weak var firstStar: UIImageView!
    @IBOutlet weak var secondStar: UIImageView!
    @IBOutlet weak var thirdStar: UIImageView!
    @IBOutlet weak var fourthStar: UIImageView!
    @IBOutlet weak var fiveStar: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Название заказа"
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()

        
        goToOrderButton.layer.cornerRadius = 16
        leaveComplaintButton.layer.cornerRadius = 16
        
        ratingOrderCard.layer.cornerRadius = 5
        ratingOrderCard.layer.shadowColor = UIColor.lightGray.cgColor
        ratingOrderCard.layer.shadowOffset = CGSize(width: 2, height: 2)
        ratingOrderCard.layer.shadowOpacity = 0.85
    }
    
    
    @IBAction func leaveComplaint(sender: UIButton) {
        
        let complaintViewController = ComplaintViewController(nibName: "ComplaintViewController", bundle: nil)
        self.navigationController?.pushViewController(complaintViewController, animated: true)
    }
    
    
    @IBAction func goToOrder(sender: UIButton) {
        
        let orderViewController = DetailOrderViewController(nibName: "DetailOrderViewController", bundle: nil)
        self.navigationController?.pushViewController(orderViewController, animated: true)
    }
  
}
