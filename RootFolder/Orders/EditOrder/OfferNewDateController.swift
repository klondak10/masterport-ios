//
//  OfferNewDateController.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OfferNewDateController: UIViewController {

    @IBOutlet weak var startDateField: UITextField!
    
    @IBOutlet weak var endDateField: UITextField!
    
    @IBOutlet weak var offerNewdateBtn: UIButton!
    
    @IBOutlet weak var offerNewDateView: UIView!
    
    @IBOutlet weak var offerNewDateLabel: UILabel!
    
    @IBOutlet weak var leftDateField: UITextField!

    @IBAction func offerNewDate(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Изменить дату"
        
        offerNewdateBtn.layer.cornerRadius = 16
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
    }
}
