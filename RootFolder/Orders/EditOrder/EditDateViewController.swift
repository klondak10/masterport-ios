//
//  EditDateViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class EditDateViewController: UIViewController {

    @IBOutlet weak var addNewDataOfEndBtn: UIButton!
    
    @IBOutlet weak var startDateField: UITextField!
    
    @IBOutlet weak var endDateField: UITextField!
    
    @IBOutlet weak var offerNewdateBtn: UIButton!
    
    @IBOutlet weak var leftDateField: UITextField!
    
    @IBAction func addNewEndDate(_ sender: UIButton) {
        
    }
    
    
    @IBAction func offerNewDate(_ sender: UIButton) {
       
        let offerNewDateController = OfferNewDateController(nibName: "OfferNewDateController", bundle: nil)
        self.navigationController?.pushViewController(offerNewDateController, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        offerNewdateBtn.layer.cornerRadius = 16
        self.navigationItem.title = "Изменить дату"
       }
}
