//
//  EditSummViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class EditSummViewController: UIViewController {

    @IBOutlet weak var newSummLabel: UILabel!
    @IBOutlet weak var offerNewSummBtn: UIButton!
    
    @IBOutlet weak var orderSummField: UITextField!
    
    @IBAction func offerNewSumm(_ sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationItem.title = "Изменить сумму"
        offerNewSummBtn.layer.cornerRadius = 16
       
    }
}
