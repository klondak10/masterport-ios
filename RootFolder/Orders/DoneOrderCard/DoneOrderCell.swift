//
//  DoneOrderCell.swift
//  Masterovik
//
//  Created by Ирина on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class DoneOrderCell: UITableViewCell {
  
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusOrderLabel: UILabel!
    @IBOutlet weak var nameOrderLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var chatImageView: UIImageView!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var areaOrderButton: UIButton!
    
    
    @IBAction func openOrderView(sender: UIButton) {
        NotificationCenter.default.post(name: OrdersTabViewController.notificationName, object: nil, userInfo:["data": 42, "isImportant": true])
    }
    
    @IBAction func openChat(sender: UIButton) {
        NotificationCenter.default.post(name: OrdersTabViewController.notificationName, object: nil, userInfo:["data": 42, "isImportant": true])
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
