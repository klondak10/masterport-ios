//
//  DoneOrderViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class DoneOrderViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var doneTableView: UITableView!
 
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "DoneOrderCell", bundle: nil)
        doneTableView.register(nib, forCellReuseIdentifier: "DoneOrderCell")
        doneTableView.delegate = self
        doneTableView.dataSource = self
    }

    // MARK: - Table view data source
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = doneTableView.dequeueReusableCell(withIdentifier: "DoneOrderCell", for: indexPath) as! DoneOrderCell
       
        return cell
    }
}
