//
//  OrdersTabViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OrdersTabViewController: BaseViewController, UIScrollViewDelegate  {
    static let notificationName = Notification.Name("myNotificationName")
    
    var isCanceled:Bool?
    var isPerformed:Bool?
    var isCurrent:Bool?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var menuBarView: FJCustomLayerView!
    @IBOutlet weak var currentButton: UIButton!
    @IBOutlet weak var performedButton: UIButton!
    @IBOutlet weak var canceledButton: UIButton!
    
    @IBOutlet weak var underline: UIView!
    
    @IBAction func loadOrderView(sender: UIButton) {
        
        _ = underline.frame.width
        
        switch sender.tag {
        case 0:
            self.scrollToPage(page: 0, animated: true)
        case 1:
            self.scrollToPage(page: 1, animated: true)
        case 2:
            self.scrollToPage(page: 2, animated: true)
            
        default: break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.navigationBar.backgroundColor = .clear
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        navigationController?.navigationBar.isTranslucent = true
//        navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationItem.title = "Заказы"
        self.addRightButton()
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(onNotification(notification:)),
            name: OrdersTabViewController.notificationName,
            object: nil)
        
        scrollView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setScrollViewContent()
    }
    
    
    func addRightButton(){
        
        let sortImage = UIImage(named: "sort-1")?
            .scaleTo(CGSize(width: 20, height: 20))
        let filterImage = UIImage(named: "filter")?
            .scaleTo(CGSize(width: 20, height: 20))
        
        let btnSort = UIButton.init(type: .custom)
        btnSort.setImage(sortImage, for: .normal)
        btnSort.addTarget(self, action: #selector(sort), for: .touchUpInside)
        
        let btnFilter = UIButton.init(type: .custom)
        btnFilter.setImage(filterImage, for: .normal)
        btnFilter.addTarget(self, action: #selector(filter), for: .touchUpInside)
        
        let stackview = UIStackView.init(arrangedSubviews: [btnSort, btnFilter])
        stackview.distribution = .equalSpacing
        stackview.axis = .horizontal
        stackview.alignment = .center
        stackview.spacing = 8
        
        let rightBarButton = UIBarButtonItem(customView: stackview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func sortAction(sender: UIButton) {
    }
    // 4
    @objc func searchTapped(sender:UIButton) {
        
    }
    
    
    func setScrollViewContent() {
        
        let currentOrderVC = CurrentOrderViewController(nibName: "CurrentOrderViewController", bundle: nil)
        
        
        let  emptyCurrentOrderView = UINib(nibName: "EmptyCurrentOrderView", bundle: .main).instantiate(withOwner: nil, options: nil).first as! EmptyCurrentOrderView
        
        // rerformed - done tab
        let doneOrderVC = DoneOrderViewController(nibName: "DoneOrderViewController", bundle: nil)
        
        let  emptyPerformedOrderView = UINib(nibName: "EmptyPerformOrderCard", bundle: .main).instantiate(withOwner: nil, options: nil).first as! EmptyPerformOrderCard
        
        // canceled tab
        let canceledOrderVC = CanceledOrderViewController(nibName: "CanceledOrderViewController", bundle: nil)
        
        let  emptyCanceledOrderView = UINib(nibName: "EmptyCanceledView", bundle: .main).instantiate(withOwner: nil, options: nil).first as! UIView
        
        isCanceled = true
        isPerformed = true
        isCurrent = true
        
        if (self.isCurrent == false){
            scrollView.addSubview(emptyCurrentOrderView)
        }
        else{
            scrollView.addSubview(currentOrderVC.view)
        }
        
        if (self.isCanceled == false){
            scrollView.addSubview(emptyCanceledOrderView)
        }
        else{
            scrollView.addSubview(canceledOrderVC.view)
        }
        if (self.isPerformed == false){
            scrollView.addSubview(emptyPerformedOrderView)
        }
        else{
            scrollView.addSubview(doneOrderVC.view)
        }
        
        let xCenterOrigin = self.view.frame.width
        
        // current tab
        currentOrderVC.view.frame = CGRect(x:0, y: 0, width:scrollView.bounds.width, height: scrollView.bounds.height)
        emptyCurrentOrderView.frame = CGRect(x:0, y: 0, width:scrollView.bounds.width, height: scrollView.bounds.height)
        
        // done tab
        doneOrderVC.view.frame = CGRect(x: xCenterOrigin , y: 0, width:self.view.frame.width, height: self.view.frame.height)
        
        emptyPerformedOrderView.frame = CGRect(x: xCenterOrigin, y: 0, width:scrollView.bounds.width, height: self.view.frame.height)
        
        
        canceledOrderVC.view.frame = CGRect(x: xCenterOrigin * 2 , y: 0, width:scrollView.bounds.width, height: scrollView.bounds.height)
        
        emptyCanceledOrderView.frame = CGRect(x: xCenterOrigin * 2 , y: 0, width:scrollView.bounds.width, height: scrollView.bounds.height)
        
        self.scrollView.contentSize = CGSize(width: self.view.frame.width * 3, height: self.scrollView.frame.height)
        
        // hide the scrol bar.
        scrollView?.showsHorizontalScrollIndicator = false
    }
    
    
    @objc func done() { // remove @objc for Swift 3
        
    }
    
    
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: animated)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        
        if (pageIndex < 1)
        {
            currentButton.titleLabel?.textColor = UIColor.darkGreen
            performedButton.titleLabel?.textColor = UIColor.lightBlack
            canceledButton.titleLabel?.textColor = UIColor.lightBlack
            
            underline.frame = CGRect(x: currentButton.frame.origin.x, y: underline.frame.origin.y, width:underline.frame.width, height: underline.frame.height)
            
        }
        else if (pageIndex == 1){
            
            currentButton.titleLabel?.textColor = UIColor.lightBlack
            performedButton.titleLabel?.textColor = UIColor.darkGreen
            canceledButton.titleLabel?.textColor = UIColor.lightBlack
            
            underline.frame = CGRect(x: currentButton.frame.width+currentButton.frame.origin.x, y: underline.frame.origin.y, width: underline.frame.width, height: underline.frame.height)
        }
        else
        {
            currentButton.titleLabel?.textColor = UIColor.lightBlack
            performedButton.titleLabel?.textColor = UIColor.lightBlack
            canceledButton.titleLabel?.textColor = UIColor.darkGreen
            
            underline.frame = CGRect(x: menuBarView.frame.width - underline.frame.width, y: underline.frame.origin.y, width: underline.frame.width, height: underline.frame.height)
        }
    }
    
    @objc func sort()
    {
        
    }
    
    @objc func filter()
    {
        let filterVC = FilterViewController(nibName: "FilterViewController", bundle: nil)
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("end scroll")
    }
    
    @objc func onNotification(notification:Notification)
    {
        let leftOrderVC = LeftOrderViewController(nibName: "LeftOrderViewController", bundle: nil)
        self.navigationController?.pushViewController(leftOrderVC, animated: true)
    }
    
    @objc func openDetailScreenNotification(notification:Notification)
    {
        let detailOrderVC = DetailOrderViewController(nibName: "DetailOrderViewController", bundle: nil)
        self.navigationController?.pushViewController(detailOrderVC, animated: true)
    }
}






