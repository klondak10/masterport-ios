//
//  RatingViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class RatingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var myRatingView: UIView!
    @IBOutlet weak var chooseCityLabel: UILabel!
    @IBOutlet weak var chooseServiceLabel: UILabel!
    @IBOutlet weak var myRatingTableView: UITableView!
  
    @IBOutlet weak var ratingCountLabel: UILabel!
    
    @IBOutlet weak var firstStarImageView: UIImageView!
    
    @IBOutlet weak var secondStarImageView: UIImageView!
    
    @IBOutlet weak var thirdStarImageView: UIImageView!
    
    @IBOutlet weak var fourthStarImageView: UIImageView!
    
    @IBOutlet weak var fiveStarImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  myRatingView.cornerRadius = 5
        myRatingTableView.delegate = self
        myRatingTableView.dataSource = self
        
        let nib = UINib.init(nibName: "RateTableViewCell", bundle: nil)
        myRatingTableView.register(nib, forCellReuseIdentifier: "RateTableViewCell")
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = myRatingTableView.dequeueReusableCell(withIdentifier: "RateTableViewCell", for: indexPath) as! RateTableViewCell
        cell.separatorInset = .zero
        
        return cell
    }
}
