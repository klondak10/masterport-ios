//
//  RateTableViewCell.swift
//  Masterovik
//
//  Created by Ирина on 5/2/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class RateTableViewCell: UITableViewCell {
  
    @IBOutlet weak var dateRateLabel: UILabel!
     @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var ratingCountLabel: UILabel!
    @IBOutlet weak var nameRateOrderLabel: UILabel!
      @IBOutlet weak var firstImageView: UIImageView!
      @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var thirdImageView: UIImageView!
    @IBOutlet weak var fourthImageView: UIImageView!
     @IBOutlet weak var fifthImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func rateDetailAction(sender: UIButton) {
        NotificationCenter.default.post(name: MyRatingTabViewController.notificationName, object: nil, userInfo:["rate": true])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
