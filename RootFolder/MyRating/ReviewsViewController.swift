//
//  ReviewsViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ReviewsViewController: UIViewController , UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var reviewTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        reviewTableView.delegate = self
        reviewTableView.dataSource = self
        let nib = UINib.init(nibName: "ReviewsTableCell", bundle: nil)
        reviewTableView.register(nib, forCellReuseIdentifier: "ReviewsTableCell")
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = reviewTableView.dequeueReusableCell(withIdentifier: "ReviewsTableCell", for: indexPath) as! ReviewsTableCell
        cell.separatorInset = .zero
        
        return cell
    }
}
