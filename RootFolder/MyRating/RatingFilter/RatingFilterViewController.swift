//
//  RatingFilterViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class RatingFilterViewController: UIViewController ,
    UITableViewDelegate , UITableViewDataSource{
    
    let data: [String] = ["Город", "Услуга", "Сначала самые новые", "Фильтрация по дате", "Сначала высокие оценки", "Фильтрация по оценке"]
    
    let imData: [String] = ["pin-green", "list-green", "sort-green",  "calendar-green", "rating-green", "star-icon-green"]
    
    @IBOutlet var tableList : UITableView!
    @IBOutlet var showResultsButton : UIButton!
    
    override func viewDidLoad() {
    super.viewDidLoad()
    
    showResultsButton.layer.cornerRadius = 16
 
        // the UIButton is simply there to capture touch up event on the entire bar button view.
        let button = UIButton.init(type: .custom)
        button.addTarget(self, action: #selector(closeFilter), for: .touchUpInside)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 7, width: 16, height: 16))
        imageView.image = UIImage(named: "close")
        let label = UILabel(frame: CGRect(x: 35, y: 0, width: 80, height: 30))
        label.text = "Фильтр"
        label.textColor = UIColor.white
        label.font = UIFont(name:"CustomFont-franklin", size: 20.0)
        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 30))
        button.frame = buttonView.frame
        buttonView.addSubview(button)
        buttonView.addSubview(imageView)
        buttonView.addSubview(label)
        buttonView.backgroundColor = UIColor.clear
        let barButton = UIBarButtonItem.init(customView: buttonView)
        self.navigationItem.leftBarButtonItem = barButton        
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Очистить", style: .plain, target: self, action: #selector(cleanFilter))

      
    let nib = UINib(nibName: "RatingFilterCell", bundle: nil)
    tableList.register(nib, forCellReuseIdentifier: "RatingFilterCell")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell: RatingFilterCell = tableList?.dequeueReusableCell(withIdentifier: "RatingFilterCell") as! RatingFilterCell
    
    cell.ratingName?.text = self.data[indexPath.row]
    
    let imageString = self.imData[indexPath.row]
    cell.ratingImage.image  = UIImage(named: imageString)
    
    return cell
    }
    
    
    @IBAction func showFilterResults(sender: UIButton) {
    
    }
    
    @objc func cleanFilter()
    {
        
    }
    
    @objc func closeFilter ()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func sort ()
    {
        
    }
}

