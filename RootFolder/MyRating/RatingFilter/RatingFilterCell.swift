//
//  RatingFilterCell.swift
//  Masterovik
//
//  Created by Ирина on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class RatingFilterCell: UITableViewCell {

    @IBOutlet weak var ratingImage: UIImageView!
    @IBOutlet weak var ratingName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
