//
//  MyRatingTabViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class MyRatingTabViewController: UIViewController, UIScrollViewDelegate  {
 
    static let notificationName = Notification.Name("ratingNotification")
    
    var isRatingOpen:Bool?
    var isReviewsOpen:Bool?
    
    @IBOutlet weak var ratingScrollView: UIScrollView!
    @IBOutlet weak var ratingBarView: UIView!
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var reviewButton: UIButton!
    @IBOutlet weak var underline: UIView!
    
    @IBAction func loadAdsView(sender: UIButton) {
        
        _ = underline.frame.width
        
        switch sender.tag {
        case 0:
            self.scrollToPage(page: 0, animated: true)
        case 1:
            self.scrollToPage(page: 1, animated: true)
        
        default: break
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        underline.backgroundColor = UIColor.lightGreen
        self.navigationItem.title = "Мой рейтинг"
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        let filterImage  = UIImage(named: "add-green")!
    
        let filterButton = UIBarButtonItem(image: filterImage,  style: .plain, target: self, action: #selector(filter))
        
        navigationItem.rightBarButtonItems = [filterButton]

        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: MyRatingTabViewController.notificationName, object: nil)

        ratingScrollView.delegate = self
        self.addRightButton()
       }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setScrollViewContent()
      
    }
    
    
    func addRightButton(){
        
        let filterImage = UIImage(named: "filter")?
            .scaleTo(CGSize(width: 20, height: 20))
        
        let btnFilter = UIButton.init(type: .custom)
        btnFilter.setImage(filterImage, for: .normal)
        btnFilter.addTarget(self, action: #selector(filter), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem(customView: btnFilter)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    
    func setScrollViewContent() {
   
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        // reviewsVC tab
        let reviewsVC = ReviewsViewController(nibName: "ReviewsViewController", bundle: nil)
        
        let xCenterOrigin = self.view.frame.width
        
        // ratingVC tab
        ratingVC.view.frame = CGRect(x:0, y: 0, width:xCenterOrigin, height: ratingScrollView.bounds.height)
        
        // reviewsVC tab
        reviewsVC.view.frame = CGRect(x: xCenterOrigin , y: 0, width:xCenterOrigin, height: ratingScrollView.bounds.height)
        
        ratingScrollView.addSubview(ratingVC.view)
        ratingScrollView.addSubview(reviewsVC.view)
        
        ratingScrollView.contentSize = CGSize(width: self.view.frame.width * 2, height: ratingScrollView.frame.height)
        
        // hide the scrol bar.
        ratingScrollView?.showsHorizontalScrollIndicator = false
    }
    
    @objc func done() { // remove @objc for Swift 3
        
    }
    
    
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = ratingScrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        ratingScrollView.scrollRectToVisible(frame, animated: animated)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
       
        if (pageIndex < 1)
        {
            ratingButton.titleLabel?.textColor = UIColor.darkGreen
            reviewButton.titleLabel?.textColor = UIColor.lightGrayText
            
            underline.frame = CGRect(x: ratingButton.center.x-underline.frame.width/2, y: underline.frame.origin.y, width:underline.frame.width, height: underline.frame.height)
            
        }
        else {
            
            ratingButton.titleLabel?.textColor = UIColor.lightGrayText
            reviewButton.titleLabel?.textColor = UIColor.darkGreen
            
            underline.frame = CGRect(x: reviewButton.center.x-underline.frame.width/2, y: underline.frame.origin.y, width: underline.frame.width, height: underline.frame.height)
        }
    }
    
    @objc func filter()
    {
        let filterVC = RatingFilterViewController(nibName: "RatingFilterViewController", bundle: nil)
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    func sort()
    {
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("end scroll")
    }
    
    @objc func onNotification(notification:Notification)
    {
        let detailRatingViewController = DetailRatingViewController(nibName: "DetailRatingViewController", bundle: nil)
        self.navigationController?.pushViewController(detailRatingViewController, animated: true)
    }
}








