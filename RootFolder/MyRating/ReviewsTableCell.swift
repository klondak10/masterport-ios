//
//  ReviewsTableCell.swift
//  Masterovik
//
//  Created by Ирина on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ReviewsTableCell: UITableViewCell {

    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var dateReviewsLabel: UILabel!
    @IBOutlet weak var ratingCountLabel: UILabel!
    @IBOutlet weak var reviewButton: UIButton!
    @IBOutlet weak var nameReviewOrderLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func reviewDetailAction(sender: UIButton) {
        NotificationCenter.default.post(name: MyRatingTabViewController.notificationName, object: nil, userInfo:["review": true])
    }

    
}
