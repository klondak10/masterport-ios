//
//  FJCustomLayerView.swift
//  Joy
//
//  Created by Dmitriy Yurchenko on 13.09.2018.
//

import Foundation
import UIKit

@IBDesignable class FJCustomLayerView: UIView {
    
    
    //MARK: - Properties
    
//    @IBInspectable var borderWidth: CGFloat = 0 {
//        didSet {
//            configure()
//        }
//    }
//    @IBInspectable var borderColor: UIColor = .white
//        {
//        didSet {
//            configure()
//        }
//    }
    @IBInspectable var corner: CGFloat = 1
        {
        didSet {
            configure()
        }
    }
    @IBInspectable var isFullHeightCorner: Bool = false
        {
        didSet {
            configure()
        }
    }
    @IBInspectable var isShadowEnable: Bool = false
        {
        didSet {
            configure()
            reloadLayout()
        }
    }
    @IBInspectable var shadowColor: UIColor = UIColor.darkGray.withAlphaComponent(0.35)
        {
        didSet {
            configure()
            reloadLayout()
        }
    }
    @IBInspectable var shadowRadius: CGFloat = 20
        {
        didSet {
            configure()
            reloadLayout()
        }
    }
    @IBInspectable var shadowOpacity: Float = 1
        {
        didSet {
            configure()
            reloadLayout()
        }
    }
    
    
    //MARK: - Lifecycle
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = isFullHeightCorner ? bounds.height / 2 : cornerRadius
        if isShadowEnable { layer.shadowPath = UIBezierPath(rect: bounds).cgPath }
    }
    
    
    //MARK: - Methods
    
    private func reloadLayout() {
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    private func configure() {
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        if isShadowEnable {
            layer.masksToBounds = false
            layer.shadowColor = shadowColor.cgColor
            layer.shadowOpacity = shadowOpacity
            layer.shadowRadius = shadowRadius
            layer.shadowOffset = CGSize.zero
        }
    }
}
