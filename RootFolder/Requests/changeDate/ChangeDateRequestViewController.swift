//
//  ChangeDateRequestViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ChangeDateRequestViewController: UIViewController {

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var newPeriodBtn: UIButton!
    
    @IBOutlet weak var donePeriodField: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var offeredCustomerField: UILabel!
    
    @IBOutlet weak var offeredByYouField: UILabel!
    @IBOutlet weak var endDateField: UILabel!
    
    @IBAction func newPeriodAction(_ sender: UIButton) {
     
        
    }
    
    @IBAction func save(_ sender: UIButton) {
    
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        saveBtn.layer.cornerRadius = 16
        self.navigationItem.title = "Изменить сроки"
    }

}
