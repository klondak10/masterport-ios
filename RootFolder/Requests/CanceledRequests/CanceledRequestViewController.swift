//
//  CanceledRequestViewController.swift
//  Masterovik
//
//  Created by Ирина on 5/2/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CanceledRequestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var cancelTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelTableView.delegate = self
        cancelTableView.dataSource = self
        
        let nib = UINib.init(nibName: "CanceledRequestTableViewCell", bundle: nil)
        cancelTableView.register(nib, forCellReuseIdentifier: "CanceledRequestTableViewCell")
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 163
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = cancelTableView.dequeueReusableCell(withIdentifier: "CanceledRequestTableViewCell", for: indexPath) as! CanceledRequestTableViewCell
        
        return cell
    }
}
