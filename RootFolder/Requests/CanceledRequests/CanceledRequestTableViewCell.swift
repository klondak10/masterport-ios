//
//  CanceledRequestTableViewCell.swift
//  Masterovik
//
//  Created by Ирина on 5/2/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CanceledRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var cancelRequestBtn: UIButton!
   
    @IBAction func cancelRequest(_ sender: UIButton) {
        NotificationCenter.default.post(name: RequestsTabViewController.notificationName, object: nil, userInfo:["RequestNotification": true])
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
