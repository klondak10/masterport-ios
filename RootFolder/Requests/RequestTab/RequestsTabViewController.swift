//
//  RequestsTabViewController.swift
//  Masterovik
//
//  Created by Ирина on 4/23/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class RequestsTabViewController: UIViewController , UIScrollViewDelegate  {
    static let notificationName = Notification.Name("RequestNotification")
    
    @IBOutlet weak var requestScrollView: UIScrollView!
    @IBOutlet weak var requestBarView: UIView!
    @IBOutlet weak var requestInWorkButton: UIButton!
    @IBOutlet weak var requestCanceledButton: UIButton!
    @IBOutlet weak var underline: UIView!
    
    @IBAction func loadRequestView(sender: UIButton) {
        _ = underline.frame.width
        
        switch sender.tag {
        case 0:
            
            self.scrollToPage(page: 0, animated: true)
            
        case 1:
            self.scrollToPage(page: 1, animated: true)
        default: break
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Заявки"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        NotificationCenter.default.addObserver(self, selector: #selector(openRequestDetail(notification:)), name: RequestsTabViewController.notificationName, object: nil)
        
        requestScrollView.delegate = self
        self.addRightButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setScrollViewContent()
        UserDefaults.standard.set(true, forKey: "Requests") //Bool
    }
    
    func addRightButton(){
        
        let filterImage = UIImage(named: "filter")?
            .scaleTo(CGSize(width: 20, height: 20))
        
        let btnFilter = UIButton.init(type: .custom)
        btnFilter.setImage(filterImage, for: .normal)
        btnFilter.addTarget(self, action: #selector(filter), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem(customView: btnFilter)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func done() { // remove @objc for Swift 3
        
    }
    
    
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = requestScrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        requestScrollView.scrollRectToVisible(frame, animated: animated)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        
        if (pageIndex < 1)
        {
            requestInWorkButton.titleLabel?.textColor = UIColor.darkGreen
            requestCanceledButton.titleLabel?.textColor = UIColor.lightGrayText
            
            underline.frame = CGRect(x: requestInWorkButton.center.x-underline.frame.width/2, y: underline.frame.origin.y, width:underline.frame.width, height: underline.frame.height)
            
            self.navigationItem.rightBarButtonItem?.customView?.alpha = 1.0
        }
        else {
            
            requestInWorkButton.titleLabel?.textColor = UIColor.lightGrayText
            requestCanceledButton.titleLabel?.textColor = UIColor.darkGreen
            
            underline.frame = CGRect(x: requestCanceledButton.center.x-underline.frame.width/2, y: underline.frame.origin.y, width: underline.frame.width, height: underline.frame.height)
            self.navigationItem.rightBarButtonItem?.customView?.alpha = 0.0
        }
    }
    
    
    @objc func filter()
    {
        let filterVC = FilterViewController(nibName: "FilterViewController", bundle: nil)
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("end scroll")
    }
    
    
    @objc func openRequestDetail(notification:Notification)
    {
        let detailRequestVC = DetailRequestViewController(nibName: "DetailRequestViewController", bundle: nil)
        self.navigationController?.pushViewController(detailRequestVC, animated: true)
    }
    
    
    func setScrollViewContent() {
        
        let requestInWorkController = RequestInWorkController(nibName:"RequestInWorkController", bundle:nil)
        let requestCanceledController = CanceledRequestViewController(nibName:"CanceledRequestViewController", bundle:nil)
        
        let xCenterOrigin = self.view.frame.width
        
        // requestCanceledController tab
        requestInWorkController.view.frame = CGRect(x:0, y: 0, width:requestScrollView.bounds.width, height: requestScrollView.bounds.height)
        
        // requestInWorkController tab
        requestCanceledController.view.frame = CGRect(x: xCenterOrigin , y: 0, width:requestScrollView.bounds.width, height: requestScrollView.bounds.height)
        
        requestScrollView.addSubview(requestInWorkController.view)
        requestScrollView.addSubview(requestCanceledController.view)
        
        requestScrollView.contentSize = CGSize(width: self.view.frame.width * 2, height: requestScrollView.frame.height)
    }
}







