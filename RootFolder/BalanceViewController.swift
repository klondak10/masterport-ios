//
//  BalanceViewController.swift
//  Masterovik
//
//  Created by Ирина on 5/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BalanceViewController: UIViewController {

    @IBOutlet weak var chooseMasterCardImage: UIImageView!
    @IBOutlet weak var chooseVisaImageView: UIImageView!
    @IBOutlet weak var masterCardBtn: UIButton!
    @IBOutlet weak var visaBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    
    @IBOutlet weak var paymentTypeLabel: UILabel!
    
    
    @IBOutlet weak var typePaymentBtn: UIButton!
    
    @IBAction func chooseTypePaymant(_ sender: UIButton) {
    }
    
    @IBOutlet weak var chooseSummOfPayment: UITextView!
    
    
    @IBAction func continueAction(_ sender: UIButton) {
        
    }
    
    @IBAction func addCard(_ sender: UIButton) {
    }
    @IBAction func chooseMasterCard(_ sender: UIButton) {
        
        if masterCardBtn.isTouchInside == true {
            
            chooseMasterCardImage.image = UIImage(named:"check-gray")!
        }else{
            chooseMasterCardImage.image = UIImage(named:"check-green")!
        }
    }
    
    @IBAction func chooseVisa(_ sender: UIButton) {
        
        if visaBtn.isTouchInside == true {
            
            chooseVisaImageView.image = UIImage(named:"check-gray")!
        }else{
            chooseVisaImageView.image = UIImage(named:"check-green")!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        chooseSummOfPayment.layer.borderWidth = 1.0; chooseSummOfPayment.layer.cornerRadius = 5
        chooseSummOfPayment.layer.borderColor  = UIColor.lightGray.cgColor
        self.navigationItem.title = "Пополнить баланс"
        continueBtn.layer.cornerRadius = 16.7
        
    }

}
