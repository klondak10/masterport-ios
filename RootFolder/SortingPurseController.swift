//
//  SortingPurseController.swift
//  Masterovik
//
//  Created by Ирина on 5/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class SortingPurseController: UIViewController {

    @IBOutlet weak var newestBtn: UIButton!
    @IBOutlet weak var showResultsSortButton: UIButton!
    @IBOutlet weak var writeOffBtn: UIButton!
    @IBOutlet weak var olderBtn: UIButton!
    @IBOutlet weak var refillBtn: UIButton!
    @IBOutlet weak var byCardBtn: UIButton!
    @IBOutlet weak var byDateBtn: UIButton!
    
    @IBOutlet weak var newestImageview: UIImageView!
    @IBOutlet weak var writeOffImageview: UIImageView!
    @IBOutlet weak var olderImageview: UIImageView!
    @IBOutlet weak var refillImageview: UIImageView!
    
    @IBAction func newestAction(_ sender: UIButton) {
        
        if newestBtn.isTouchInside == true {

            newestImageview.image = UIImage(named:"check-gray")!
        }else{
             newestImageview.image = UIImage(named:"check-green")!
        }
    }
    
    @IBAction func refillAction(_ sender: UIButton) {
        if refillBtn.isTouchInside == true {
            
            refillImageview.image = UIImage(named:"check-gray")!
        }else{
            refillImageview.image = UIImage(named:"check-green")!
        }
    }
    
    @IBAction func writeOffAction(_ sender: UIButton) {
        if writeOffBtn.isTouchInside == true {
            
            writeOffImageview.image = UIImage(named:"check-gray")!
        }else{
            writeOffImageview.image = UIImage(named:"check-green")!
        }
    }
    @IBAction func olderAction(_ sender: UIButton) {
        if olderBtn.isTouchInside == true {
            
            olderImageview.image = UIImage(named:"check-gray")!
        }else{
            olderImageview.image = UIImage(named:"check-green")!
        }
    }
    @IBAction func byDateAction(_ sender: UIButton) {
        
    }
    @IBAction func byCardAction(_ sender: UIButton) {
       
    }
   
    @IBAction func showResultSorting(_ sender: UIButton) {
        if newestBtn.isTouchInside == true {
            
            newestImageview.image = UIImage(named:"check-gray")!
        }else{
            newestImageview.image = UIImage(named:"check-green")!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showResultsSortButton.cornerRadius = 16.7
        
        // the UIButton is simply there to capture touch up event on the entire bar button view.
        let button = UIButton.init(type: .custom)
        button.addTarget(self, action: #selector(closeSorting), for: .touchUpInside)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 7, width: 16, height: 16))
        imageView.image = UIImage(named: "close")
        let label = UILabel(frame: CGRect(x: 35, y: 0, width: 200, height: 30))
        label.text = "Сортировка"
        label.textColor = UIColor.white
        label.font = UIFont(name:"CustomFont-franklin", size: 20.0)
        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 30))
        button.frame = buttonView.frame
        buttonView.addSubview(button)
        buttonView.addSubview(imageView)
        buttonView.addSubview(label)
        buttonView.backgroundColor = UIColor.clear
        let barButton = UIBarButtonItem.init(customView: buttonView)
        self.navigationItem.leftBarButtonItem = barButton

    }
    
     @objc func closeSorting()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }

}
