//
//  UITextField+Validator.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UITextField {
    
    func validatedText(of type: ValidatorEnum) throws -> String {
        let validator = ValidatorFactory.validate(for: type)
        return try validator.validated(self.text)
    }
    
    func notValidValueIn(withPlaceholder: String) {
        self.shakeAnimation()
        self.text = ""
        self.placeholder = withPlaceholder
        self.layer.borderColor = UIColor.warningRed.cgColor
    }
}
