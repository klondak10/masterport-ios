//
//  UIViewController+UserExit.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/21/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func userExit() {
        showWireframe(yesHandler: { [unowned self] in
            TokenManager.removeTokens()
            UserCoreDataManager.shared.deleteUser()
            UserDefaults.standard.removeObject(forKey: userIsRegistered)
            UserDefaults.standard.removeObject(forKey: userWasYetExecutor)
            self.sideMenuController?.hideMenu()
            self.pushVC(SignInViewController.instanceFromStoryboard)
            }, noHandler: nil, title: "Внимание", description: "Вы уверены, что хотите выйти?")
    }
}

