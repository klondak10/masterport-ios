//
//  UIBarButton+CustomBarButtons.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    convenience init(image: UIImage?, target: Any?, action: Selector?, leftInset: CGFloat = 0, rightInset: CGFloat = 0) {
        let containerView = UIView()
        let button = UIButton(frame: CGRect(origin: .zero, size: navBarButtonImageSize))
        button.autoSetDimensions(to: navBarButtonImageSize)
        if image != nil {
            button.imageView?.contentMode = .scaleAspectFit
            button.imageView?.tintColor = .white
            button.setImage(image, for: .normal)
        }
        if action != nil {
            button.addTarget(target, action: action!, for: .touchUpInside)
        }
        containerView.addSubview(button)
        button.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0.0, left: leftInset, bottom: 0.0, right: rightInset))
        self.init(customView: containerView)
    }
    
    static var empty: UIBarButtonItem {
        return UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
