//
//  UIImage+ImageFromLayer.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIImage {
    convenience init(layer: CALayer) {
        var image: UIImage = UIImage()
        UIGraphicsBeginImageContext(layer.frame.size)
        if let context = UIGraphicsGetCurrentContext()
        {
            layer.render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        }
        UIGraphicsEndImageContext()
        if image.cgImage != nil
        {
            self.init(cgImage: image.cgImage!)
        } else {
            self.init()
        }
        
    }
    
    func scaleTo(_ newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
