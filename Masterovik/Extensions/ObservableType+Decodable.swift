//
//  ObservableType+Decodable.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import Moya

public extension ObservableType where E == Moya.Response {
    
    func map<T: Decodable>(_ type: T.Type)
        -> Observable<T> {
            return flatMap { (response : Response)
                -> Observable<T> in
                return Observable.just(try response.map(type))
            }
    }
    
    func map<T: Decodable>(_ type: [T.Type])
        -> Observable<[T]> {
            return flatMap { (response : Response)
                -> Observable<[T]> in
                return Observable.just(try response.map(type))
            }
    }
}
