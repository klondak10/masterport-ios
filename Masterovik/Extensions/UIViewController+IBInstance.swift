//
//  UIViewController+IBInstance.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/15/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIViewController {
    
    static var instanceFromStoryboard: UIViewController {
        let storyboardName = String(String(describing: self).dropLast("ViewController".count))
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        if let viewController = storyboard.instantiateInitialViewController() {
            return viewController
        } else {
            print("An error occured. Storyboard with name " + String(describing: self) + " can't instantiate initial view controller")
            return UIViewController()
        }
    }
    
    static var instanceFromNib: UIViewController {
        return UIViewController(nibName: String(describing: self), bundle: nil)
    }
}
