//
//  Moya+Decodable.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import Moya

public extension Moya.Response {
    func map<T: Decodable>(_ type: T.Type)
        throws -> T {
            let item = try JSONDecoder().decode(T.self, from: self.data)
            return item
    }
    
    func map<T: Decodable>(_ type: [T.Type])
        throws -> [T] {
             let array = try JSONDecoder().decode(Array<T>.self, from: self.data)
            return array
    }
}
