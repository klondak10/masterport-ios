//
//  showLoadIndicatorOnView.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/9/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//
import UIKit

extension UIView {
    
    static func showLoadIndicatiorOn(view: UIView) {
        
        let indicator = UIActivityIndicatorView(frame: view.bounds)
        let viewBack = UIView(frame: view.bounds)
        viewBack.backgroundColor = UIColor(white: 0.2, alpha: 0.3)
        indicator.insertSubview(viewBack, at: 0)
        indicator.style = .gray
        indicator.startAnimating()
        UIView.animate(withDuration: 0.2) {
            view.addSubview(indicator)
        }
        view.layoutIfNeeded()
    }
    
    static func removeLoadIndicatorFrom(view: UIView) {
        let indicator = view.subviews.first { $0 is UIActivityIndicatorView}
        indicator?.removeFromSuperview()
        view.layoutIfNeeded()
    }
}
