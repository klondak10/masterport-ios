//
//  UIColor+CustomColors.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let lightGreen = UIColor(hex: 0x15CB84)
    static let darkGreen = UIColor(hex: 0x067952)
    static let lime = UIColor(hex: 0xE1F1E6)
    static let warningRed = UIColor(hex: 0xFF5437)
    static let lightBlack = UIColor(hex: 0x505050)
    static let lightGrayText = UIColor(white: CGFloat(0xA0)/255.0, alpha: 1.0)
    static let darkGrayText = UIColor(white: CGFloat(0x50)/255.0, alpha: 1.0)
    static let textFieldBorderColor = UIColor(white: 0.75, alpha: 1.0)
    
    convenience init(hex: Int) {
        let red = CGFloat((hex >> 16) & 0xFF) / 255.0
        let green = CGFloat((hex >> 8) & 0xFF) / 255.0
        let blue = CGFloat(hex & 0xFF) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
}
