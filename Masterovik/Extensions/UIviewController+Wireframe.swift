//
//  UIviewController+Wireframe.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIViewController {
    
    typealias AlertHandler = (() -> Void)?
    
    func showWireframe(yesHandler: AlertHandler, noHandler: AlertHandler,
                       title: String, description: String,
                       image: UIImage? = nil, okButtonHandler: AlertHandler = nil) {
        
        let wireframe = UIStoryboard(name: "WireframeStoryboard", bundle: nil).instantiateViewController(withIdentifier: "WireframeID") as! Wireframe
        wireframe.providesPresentationContextTransitionStyle = true
        wireframe.definesPresentationContext = true
        wireframe.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        wireframe.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        wireframe.loadViewIfNeeded()
        wireframe.yesButtonHandler = yesHandler
        wireframe.noButtonHandler = noHandler
        wireframe.okButtonHandler = okButtonHandler
        if image != nil {
            wireframe.imageView.image = image
        }
        wireframe.titleLabel.text = title
        wireframe.descriptionLabel.text = description
        self.present(wireframe, animated: true, completion: nil)
    }
}
