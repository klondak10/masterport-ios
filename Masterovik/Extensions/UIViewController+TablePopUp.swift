//
//  UIViewController+TablePopUp.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/14/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showPopupTable(items: [String]?, multipleChoise: Bool = true, selectedCellsIndex: [Int]? = nil, addHandler: @escaping ([String]) -> ()) {
        
        guard let items = items, items.count != 0 else { return }
        let tablePopUp = UIStoryboard(name: "WireframeStoryboard", bundle: nil).instantiateViewController(withIdentifier: "TablePopUpID") as! TablePopUpVC
        tablePopUp.providesPresentationContextTransitionStyle = true
        tablePopUp.definesPresentationContext = true
        tablePopUp.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        tablePopUp.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        tablePopUp.loadViewIfNeeded()
        tablePopUp.action = addHandler
        tablePopUp.tableView.allowsMultipleSelection = multipleChoise
        tablePopUp.setItems(items)
        if let selected = selectedCellsIndex {
            tablePopUp.setSelectedItems(selected)
        }
        self.present(tablePopUp, animated: true, completion: nil)
    }
}

