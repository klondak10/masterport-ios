//
//  TokenManager.swift
//  Masterovik
//
//  Created by Roman Haiduk on 6/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct TokenManager {
    
    private static let atokenPath = "accessToken"
    private static let rtokenPath = "refreshToken"
    
    static func getAccessToken() -> String? {
        return UserDefaults.standard.string(forKey: self.atokenPath)
    }
    
    static func getRefreshToken() -> String? {
        return UserDefaults.standard.string(forKey: self.rtokenPath)
    }
    
    static func changeAccess(token: String) {
        UserDefaults.standard.set(token, forKey: self.atokenPath)
        UserDefaults.standard.synchronize()
    }
    
    static func changeRefreshToken(token: String) {
        UserDefaults.standard.set(token, forKey: self.rtokenPath)
        UserDefaults.standard.synchronize()
    }
    
    static func removeTokens() {
        UserDefaults.standard.set(nil, forKey: self.atokenPath)
        UserDefaults.standard.set(nil, forKey: self.rtokenPath)
    }
}
