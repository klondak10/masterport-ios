//
//  RefreshTokenAPI.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/5/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya
import SwiftyJSON
import RxSwift


extension myRxProvider {
    
    enum ResfreshTokenError:String, Error {
        case castFromJsonError = "Ошибка преобразования токена с Json"
        case errorResponse = "Ошибка получения данных(token)"
    }
    
    struct RefreshTokenRequest: TargetType{
        
        var environmentBaseURL: String {
            switch environment {
            case .test :
                return "http://masterovik.net/api/v1/"
            case .production:
                return "huiznaet.com" /// Вставить значение и изменить на вызове
            }
        }
        
        var baseURL: URL {
            guard let url = URL(string: environmentBaseURL)
                else { fatalError("URL doesn't exist. (BaseURL: \(environmentBaseURL).") }
            return url
        }
        
        var environment: Environments {
            return .test
        }
        
        var headers: [String : String]? {
            var header = [String : String]()
            header["Accept"] = "application/json"
            if let token = TokenManager.getAccessToken() {
                header["Authorization"] = "Bearer " + token
            }
            return header
        }
        
        
        var path: String {
            return "user/refresh-token"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var sampleData: Data {
            return Data()
        }
        
        var task: Task {
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        }
        
        var params: [String: Any] {
            if let refreshToken = TokenManager.getAccessToken() {
                return ["refresh_token" : refreshToken]
            } else {
                print(" REFRESH TOKEN NOT FOUND IN USERDEFAULTS")
                return [:]
            }
        }
        
        func changeToken() -> Single<Bool> {
            return Single<Bool>.create { observer in
                let provider = MoyaProvider<RefreshTokenRequest>.init()
                print("Start change token")
                
                let task = provider.request(self, completion: { (response) in
                    switch response {
                    case .success(let result):
                        if let json = try? JSONSerialization.jsonObject(with: result.data, options: .mutableContainers) as? [String:String] {
                            
                            if let token = json["access_token"] {
                                TokenManager.changeAccess(token: token)
                                observer(.success(true))
                            } else if json["error_message"] != nil {
                                observer(.error(ResfreshTokenError.castFromJsonError))
                            }
                        } else {
                            let error: ResfreshTokenError = .castFromJsonError
                            print(error)
                            observer(.error(error))
                        }
                    case .failure(_):
                        let error: ResfreshTokenError = .errorResponse
                        print(error)
                        observer(.error(error))
                    }
                })
                return Disposables.create {
                    task.cancel()
                }
            }
        }
    }
}

