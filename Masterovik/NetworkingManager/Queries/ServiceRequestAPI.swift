//
//  ServiceRequestAPI.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya


enum ServiceRequestAPI {
    
    case getAllServices
    
    case getUserServices
    
    case getServiceFromId(Int)
    
    case createService(name: String, description: String, reason: String)
    
    case addServices(servicesID: [Int])
    
    case removeServices(servicesID: [Int])
}

extension ServiceRequestAPI: Moya.TargetType {
    
    var environmentBaseURL: String {
        
        switch environment {
        case .test :
            return "http://masterovik.net/api/v1/"
        case .production:
            return "huiznaet.com" /// Вставить значение и изменить на вызове
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL)
            else { fatalError("URL doesn't exist. (BaseURL: \(environmentBaseURL).") }
        return url
    }
    
    var environment: Environments {
        return .test
    }
    
    var headers: [String : String]? {
        switch self {
        case .getAllServices, .getServiceFromId:
            return ["Authorization": "Bearer " + TokenManager.getAccessToken()!]
        default:
            var header = [String : String]()
            header["Accept"] = "application/json"
            if let token = TokenManager.getAccessToken() {
                header["Authorization"] = "Bearer " + token
            }
            return header
        }
    }
    
    var path: String {
        switch self {
        case .getAllServices:
            return "service/get-services"
        case .getUserServices:
            return "service/get-user-services"
        case .getServiceFromId(let id):
            return "service/\(id)"
        case .createService:
            return "service-offered/create"
        case .addServices:
            return "user/add-service"
        case .removeServices:
            return "user/remove-service"
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getAllServices, .getServiceFromId:
            return .requestPlain
        default:
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        }
    }
    
    var params: [String : Any] {
        switch self {
        case .getUserServices:
            return ["access_token": TokenManager.getAccessToken()!]
        case .createService(let name, let description, let reason):
            return [
                "name": name,
                "description": description,
                "reason": reason
            ]
        case .removeServices(let servicesID),.addServices(let servicesID):
            return [
                "access_token": TokenManager.getAccessToken()!,
                "services": servicesID
            ]
        default:
            return [:]
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getAllServices, .getServiceFromId:
            return .get
        default:
            return .post
        }
    }
}
