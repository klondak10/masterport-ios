//
//  WalletRequestAPI.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/15/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

//
//  ServiceRequestAPI.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya


enum WalletRequestAPI {
    
    case addNewCard(number: String)
    
    case getCardList
    
    case removeCard(cardID: Int)
    
    case editCard(cardID: Int, number: String)
    
    case addMoney(cardID: Int, summary: Double)
    
    case useMoney(cardID: Int, summary: Double)
    
    case paymentHistory(ascend: Bool, forOperation: String?, forCard: Int?)
    
    case getPartners
}

extension WalletRequestAPI: Moya.TargetType {
    
    var method: Method {
        switch self {

        default:
            return .post
        }
    }
    
    var environmentBaseURL: String {
        
        switch environment {
        case .test :
            return "http://masterovik.net/api/v1/"
        case .production:
            return "huiznaet.com" /// Вставить значение и изменить на вызове
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL)
            else { fatalError("URL doesn't exist. (BaseURL: \(environmentBaseURL).") }
        return url
    }
    
    var environment: Environments {
        return .test
    }
    
    var headers: [String : String]? {
        switch self {
        case .getPartners, .getCardList, .paymentHistory:
            return ["Authorization": "Bearer " + TokenManager.getAccessToken()!]
        default:
            var header = [String : String]()
            header["Accept"] = "application/json"
            if let token = TokenManager.getAccessToken() {
                header["Authorization"] = "Bearer " + token
            }
            return header
        }
    }
    
    var path: String {
        switch self {
        case .addMoney:
            return "credit-cards/add-money"
        case .addNewCard:
            return "credit-cards/add-card"
        case .editCard:
            return "credit-cards/edit-card"
        case .getCardList:
            return "credit-cards/view-cards"
        case .getPartners:
            return "credit-cards/get-partners"
        case .paymentHistory:
            return "credit-cards/get-payment-history"
        case .removeCard:
            return "credit-cards/delete-card"
        case .useMoney:
            return "credit-cards/subtract-money"
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestParameters(parameters: params, encoding: JSONEncoding.default)
    }
    
    var params: [String : Any] {
        switch self {
        case .getPartners, .getCardList:
            return ["access_token": TokenManager.getAccessToken()!]
        case .addMoney(let cardID, let summary), .useMoney(let cardID, let summary):
            return [
                "access_token": TokenManager.getAccessToken()!,
                "card_id": cardID,
                "sum": summary
            ]
        case .editCard(let cardID, let number):
            return [
                "card_id": cardID,
                "card": number
            ]
            
        case .addNewCard(let number):
            return [
                "access_token": TokenManager.getAccessToken()!,
                "card": number
                ]
        case .removeCard(let cardID):
            return [
                "access_token": TokenManager.getAccessToken()!,
                "card_id": cardID
            ]
            
        case .paymentHistory(let ascend, let forOperation, let forCard):
            var dict: [String: Any] = [:]
            dict["access_token"] = TokenManager.getAccessToken()!
            dict["new"] = ascend
            dict["operation"] = forOperation
            dict["card_id"] = forCard
            return dict
        }
    }
}
