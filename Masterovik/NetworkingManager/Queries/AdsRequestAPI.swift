//
//  AdsRequestAPI.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/21/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya


enum AdsRequestAPI {
    
    case create(
        city_id: Int,
        service_id: [Int],
        term: Int,
        price: Int,
        place_for_work: String?,
        description: String,
        description_full: String?
    )
    case all(
        city_id: Int?,
        services_id: [Int]?,
        priceFromTo: FromToFilterModel?,
        dateFrom: Double?,
        dateTo: Double?,
        durationFromTo: FromToFilterModel?
    )
    case my
}

extension AdsRequestAPI: Moya.TargetType {
    
    var method: Method {
        switch self {
            
        default:
            return .post
        }
    }
    
    var environmentBaseURL: String {
        
        switch environment {
        case .test :
            return "http://masterovik.net/api/v1/"
        case .production:
            return "huiznaet.com" /// Вставить значение и изменить на вызове
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL)
            else { fatalError("URL doesn't exist. (BaseURL: \(environmentBaseURL).") }
        return url
    }
    
    var environment: Environments {
        return .test
    }
    
    var headers: [String : String]? {
        switch self {
        default:
            var header = [String : String]()
           // header["Accept"] = "application/json"
            if let token = TokenManager.getAccessToken() {
                header["Authorization"] = "Bearer " + token
            }
            return header
        }
    }
    
    var path: String {
        switch self {
        case .my:
            return "announcement/get-my-announcements"
        case .all:
            return "announcement/get-catalog-announcements"
        case .create:
            return "announcement/create-announcement"
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestParameters(parameters: params, encoding: JSONEncoding.default)
    }
    
    var params: [String : Any] {
        switch self {
        case .my:
             return ["access_token": TokenManager.getAccessToken()!]
        case .create(let city_id, let service_id, let term, let price,
                     let place_for_work, let description, let description_full):
            return [
                "access_token": TokenManager.getAccessToken()!,
                "city_id": city_id,
                "service_id": service_id,
                "term": term,
                "price": price,
                "place_for_work": place_for_work as Any,
                "description": description,
                "description_full": description_full as Any
            ]
            case .all(let city_id, let services_id, let priceFromTo,
                      let dateFrom, let dateTo, let durationFromTo):
                var dictionary: [String: Any] = [
                    "city_id": city_id as Any,
                    "services_id": services_id as Any,
                ]
                dictionary["price"] = [priceFromTo?.from, priceFromTo?.to]
                dictionary["date"] = [dateFrom, dateTo]
                dictionary["term"] = [durationFromTo!.from, durationFromTo!.to]
                
            return dictionary
        }
    }
}
