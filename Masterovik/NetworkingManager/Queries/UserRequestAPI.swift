//
//  UserRequestAPI.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya

enum Environments {
    case test
    case production
}

enum UserRequestAPI {
    
    case checkPhoneExist(phone: String)
    case login(phone:String, sms: String)
    case changeRole
    case getUserData
    case getAllUsers
    case createUser(firstName: String,
        lastName: String,
        email: String?,
        cityID: Int,
        companyName: String?,
        companyDescription: String?,
        phone: String,
        roleID: Int,
        invitedBy: Int?
    )
    case cityList
    case cityFromId(Int)
    case changeEmail(String)
    case phoneChangeFirstStep(phone: String)
    case phoneChangeLastStep(phone: String, sms: String)
    case changePersonalData(firstName: String, lastName: String,
                            cityId: Int, companyName: String, companyDescription: String)
}

extension UserRequestAPI: Moya.TargetType {
    
    var environmentBaseURL: String {
        
        switch environment {
        case .test :
            return "http://masterovik.net/api/v1/"
        case .production:
            return "huiznaet.com" /// Вставить значение и изменить на вызове
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL)
            else { fatalError("URL doesn't exist. (BaseURL: \(environmentBaseURL).") }
        return url
    }
    
    var environment: Environments {
        return .test
    }
    
    var headers: [String : String]? {
        switch self {
        case .cityList, .cityFromId:
            return nil
        default:
            var header = [String : String]()
            header["Accept"] = "application/json"
            if let token = TokenManager.getAccessToken() {
                header["Authorization"] = "Bearer " + token
            }
            return header
        }
    }
    
    var path: String {
        switch self {
        case .checkPhoneExist, .login:
            return "user/login"
        case .getAllUsers:
            return "user/"
        case .changeRole:
            return "user/change-role"
        case .getUserData:
            return "user/get-user-data"
        case .createUser:
            return "user/create-user"
        case .cityList:
            return "city/"
        case .cityFromId(let id):
            return "city/\(id)"
        case .changeEmail:
            return "user/change-user-email"
        case .phoneChangeFirstStep, .phoneChangeLastStep:
            return "user/change-user-phone"
        case .changePersonalData:
            return "user/edit-user-data"
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .cityList, .cityFromId:
            return .requestPlain
        default:
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        }
    }
    
    var params: [String : Any] {
        switch self {
        case .changeRole, .getUserData:
            return ["access_token": TokenManager.getAccessToken()!]
        case .checkPhoneExist(let phone):
            return ["phone": phone]
        case .login(let phone, let sms):
            return ["phone": phone, "sms": sms]
        case .createUser(let firstName, let lastName, let email, let cityID,
                         let companyName, let companyDescription, let phone, let roleID, let invitedBy):
            return [
                "first_name": firstName,
                "last_name": lastName,
                "email": email as Any,
                "city_id": cityID,
                "company_name": companyName as Any,
                "company_description": companyDescription as Any,
                "phone": phone,
                "role_id": roleID,
                "invited_by": invitedBy as Any
            ]
        case .changeEmail(let email):
            return [
                "access_token": TokenManager.getAccessToken()!,
                "email": email
            ]
        case .phoneChangeFirstStep(let phone):
            return [
                "access_token": TokenManager.getAccessToken()!,
                "phone": phone
            ]
        case .phoneChangeLastStep(let phone, let sms):
            return [
                "access_token": TokenManager.getAccessToken()!,
                "phone": phone,
                "sms": sms
            ]
            case .changePersonalData(let firstName, let lastName, let cityId,
                                     let companyName, let companyDescription):
            return [
                "access_token": TokenManager.getAccessToken()!,
                "cityId": cityId,
                "first_name": firstName,
                "last_name": lastName,
                "company_name": companyName,
                "company_description": companyDescription
            ]
        default:
            return [:]
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getAllUsers, .cityFromId, .cityList:
            return .get
        default:
            return .post
        }
    }
}
