//
//  MyRxProvider.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/5/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya
import RxSwift

enum TokenError: String, Error {
    case TokenExpired
}


public class myRxProvider<Target>  where Target: TargetType {
    
    private let provider: MoyaProvider<Target>
    private let disposeBag = DisposeBag()
    
    init(endpointClosure: @escaping MoyaProvider<Target>.EndpointClosure = MoyaProvider.defaultEndpointMapping,
         requestClosure: @escaping MoyaProvider<Target>.RequestClosure = MoyaProvider<Target>.defaultRequestMapping,
         stubClosure: @escaping MoyaProvider<Target>.StubClosure = MoyaProvider.neverStub,
         manager: Manager = MoyaProvider<Target>.defaultAlamofireManager(),
         plugins: [PluginType] = [],
         trackInflights: Bool = false) {

        self.provider = MoyaProvider(endpointClosure: endpointClosure,
                                     requestClosure: requestClosure,
                                     stubClosure: stubClosure,
                                     manager: manager,
                                    plugins: [NetworkLoggerPlugin(verbose: true)],// plugins: plugins,
                                     trackInflights: trackInflights)
    }
    
    func request(_ token: Target, isSecondTryAfterAuth: Bool = false) -> Single<Response> {
        let request = provider.rx.request(token)
        return request
            .flatMap { response in
                if response.statusCode == 426 {
                    return RefreshTokenRequest().changeToken().do()
                        
                        .flatMap { _ in return self.request(token) }
                } else {
                    return Single.just(response)
                }
        }
    }
}
