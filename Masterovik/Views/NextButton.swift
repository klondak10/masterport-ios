//
//  NextButton.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class NextButton: UIButton {
    
    @IBInspectable var isGreenButton = false
    
    override var isUserInteractionEnabled: Bool {
        didSet {
            if !isUserInteractionEnabled {
                self.backgroundColor = UIColor.init(white: 0.94, alpha: 1)
                 self.setTitleColor(UIColor(white: 0.6, alpha: 1), for: .normal)
            }  else {
                if !isGreenButton{
                    self.backgroundColor = UIColor.init(white: 1, alpha: 1)
                    self.setTitleColor(UIColor(white: 0.0, alpha: 1), for: .normal)
                } else {
                    self.backgroundColor = #colorLiteral(red: 0, green: 0.5391061902, blue: 0.3955700099, alpha: 1)
                    self.setTitleColor(UIColor.white, for: .normal)
                }
            }
        }
    }
    
    override var bounds: CGRect {
        didSet {
            self.cornerRadius = min(self.bounds.width, self.bounds.height) / 2.0
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 25.0, bottom: 0.0, right: 25.0)
        self.titleLabel?.textAlignment = .center
        titleLabel?.font = FontHelper.font(type: .franklin, size: .medium)
        setContentHuggingPriority(UILayoutPriority(rawValue: 200.0), for: .horizontal)
        setContentCompressionResistancePriority(UILayoutPriority(rawValue: 800.0), for: .horizontal)
        let screenWidth = UIScreen.main.bounds.width
        autoSetDimension(.width, toSize: screenWidth * 0.5, relation: .greaterThanOrEqual)
        autoSetDimension(.height, toSize: screenWidth * 0.1)
        let bounds = self.bounds
        self.bounds = bounds
    }
}
