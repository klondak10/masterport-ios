//
//  BaseTableViewCell.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/8/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    override required init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        setupView()
    }
    
    class func registerClass(for tableView: UITableView) {
        tableView.register(self, forCellReuseIdentifier: reuseIdentifier)
    }
    
    class func registerNib(for tableView: UITableView) {
        tableView.register(UINib(nibName: String(describing: self), bundle: nil), forCellReuseIdentifier: reuseIdentifier)
    }
    
    class func dequeue(tableView:UITableView) -> Self {
        func helper<T>(tableView:UITableView) -> T where T : BaseTableViewCell {
            return tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as! T
        }
        return helper(tableView:tableView)
    }
    
    func setupView() {}
    
}
