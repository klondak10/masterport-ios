//
//  AutoHeightTableView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/15/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class AutoHeightTableView: UITableView {
    
    var maxHeight: CGFloat?
    var minHeight: CGFloat?
    
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        let width = super.intrinsicContentSize.width
        var height = contentSize.height
        if maxHeight != nil {
            height = min(height, maxHeight!)
        }
        if minHeight != nil {
            height = max(height, minHeight!)
        }
        return CGSize(width: width, height: contentSize.height)
    }
}
