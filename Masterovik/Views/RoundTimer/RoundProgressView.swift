//
//  RoundProgressView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/15/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class RoundProgressView: UIView {
    
    var progress: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    var progressColor: UIColor = .black
    var emptyColor: UIColor = .white
    var lineWidth: CGFloat = 5.0
    var radius: CGFloat = 10.0 // Ничего не значит, устанавливается в drawSegment
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawSegment(startAngle: 0, endAngle: CGFloat.pi * 2, color: emptyColor)
        drawSegment(startAngle: CGFloat.pi, endAngle: (progress + 0.5) * CGFloat.pi * 2, color: progressColor)
    }
    
    func drawSegment(startAngle: CGFloat, endAngle: CGFloat, color: UIColor) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        radius = min(bounds.width, bounds.height) / 3 - lineWidth
        let path = CGMutablePath()
        path.addArc(center: center, radius: radius + lineWidth, startAngle: startAngle, endAngle: endAngle, clockwise: false)
        path.addArc(center: center, radius: radius, startAngle: endAngle, endAngle: startAngle, clockwise: true)
        path.closeSubpath()
        context.addPath(path)
        context.setFillColor(color.cgColor)
        context.drawPath(using: .fill)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
