//
//  LabeledFieldView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import PureLayout

class LabeledFieldView: UIView{
    
    var maxCharacters = 0
    
    let label = UILabel()
    let textField = UITextField()
    var validateType: ValidatorEnum?
    var offset: CGFloat = 5.0
    
    private var pasteIsDisable = false
    
    var isValidText = PublishSubject<Bool>()
    
    @IBInspectable var labelText: String {
        didSet {
            label.text = labelText
            if isRequired {
                setAsterisk()
            } else {
                label.attributedText = nil
                label.text = labelText
            }
        }
    }
    
    @IBInspectable var isPhoneNumberTF: Bool = false {
        didSet {
            if isPhoneNumberTF {
                self.textField.delegate = self
            }
        }
    }
    
    @IBInspectable var isRequired: Bool = false {
        didSet {
            if isRequired {
                setAsterisk()
            } else {
              //  label.attributedText = nil
            }
        }
    }
    
    @IBInspectable var isDisablePaste: Bool = false {
        didSet {
            self.pasteIsDisable = isDisablePaste
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        labelText = ""
        super.init(coder: aDecoder)
        backgroundColor = .clear
        let screenWidth = UIScreen.main.bounds.width
        let sizeAdjustment = trunc((screenWidth - 320.0) / 40.0)
        offset += sizeAdjustment
        setupSubviews()
    }
    
    func setupSubviews() {
        self.addSubview(label)
        self.addSubview(textField)
        
        label.font = FontHelper.font(type: .franklin, size: .small)
        label.textColor = .lightGrayText
        textField.borderStyle = .roundedRect
        textField.font = FontHelper.font(type: .roboto, size: .small)
        label.autoPinEdge(toSuperviewEdge: .top)
        label.autoPinEdge(toSuperviewEdge: .left)
        label.autoSetDimension(.height, toSize: 20.0)
        textField.autoPinEdge(.top, to: .bottom, of: label, withOffset: offset)
        textField.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        textField.autoMatch(.width, to: .height, of: textField, withMultiplier: 8.0)
    }
    
    func setAsterisk() {
        guard label.text != nil else { return }
        let asteriskText = label.text! + " *"
        
        let attrText = NSMutableAttributedString(string: asteriskText)
        let range = (asteriskText as NSString).range(of: " *")
        attrText.setAttributes([.foregroundColor: UIColor.red], range: range)
        label.attributedText = attrText
    }
    
    func validateText() throws -> String {
        
        if let validator = self.validateType {
            let text = try textField.validatedText(of: validator)
            return text
        } else {
            return textField.text ?? ""
        }
    }
}

extension LabeledFieldView: UITextFieldDelegate {
    
    private func checkValidation(_ validator: Validatable,_ text: String) {
        do {
            _ = try validator.validated(text)
        } catch {
            self.isValidText.onNext(false)
            self.label.textColor = .warningRed
            return
        }
        self.isValidText.onNext(true)
        self.label.textColor = .lightGrayText
    }
    
    private func createPhoneNumberMask(_ text: String) -> String {
        let unsafeChars = CharacterSet.alphanumerics.inverted  // Remove the .inverted to get the opposite result.
        
        var cleanChars  = text.components(separatedBy: unsafeChars).joined(separator: "")
        
        if cleanChars.count == 0 {
            return ""
        }
        
        var offset = 0
        for i in 0...cleanChars.count {
            if i == 0 {
                cleanChars.insert("+", at: cleanChars.index(cleanChars.startIndex, offsetBy: 0))
                cleanChars.insert(" ", at: cleanChars.index(cleanChars.startIndex, offsetBy: 2))
                cleanChars.insert("(", at: cleanChars.index(cleanChars.startIndex, offsetBy: 3))
                offset += 2
            }
            if i == 4 {
                cleanChars.insert(")", at: cleanChars.index(cleanChars.startIndex, offsetBy: 7))
            }
            if i == 7 {
                cleanChars.insert("-", at: cleanChars.index(cleanChars.startIndex, offsetBy: 11))
            }
        }
        return cleanChars
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if isPhoneNumberTF && textField.text == "" {
            textField.text = "+"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text, range.length == 0 {
            var string = string
            
            if isPhoneNumberTF {
                textField.text = createPhoneNumberMask(text + string)
                if textField.text!.count > 17 {
                    textField.text?.removeLast()
                    string = ""
                }
            }
            
            if let validateFor = self.validateType {
                let validator = ValidatorFactory.validate(for: validateFor)
                checkValidation(validator, text + string)
            }
            if isPhoneNumberTF {
                return false
            }
        } else if range.length == 1 {
            if let validateFor = self.validateType {
                var text = textField.text ?? ""
                text.removeLast()
                let validator = ValidatorFactory.validate(for: validateFor)
                checkValidation(validator, text)
            }
        }
        return true
    }
}
