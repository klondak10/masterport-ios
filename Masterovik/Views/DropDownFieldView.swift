//
//  DropDownFieldView.swift
//  Masterovik
//
//  Created by Roman Haiduk on 6/14/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import PureLayout

class DropDownFieldView: UIView {
    
    private var maxCharacters = 0
    
    private let label = UILabel()
    let textField = DropDown()
    private var offset: CGFloat = 5.0
    
    private var cityNameArray: [CityModel] = [] {
        didSet {
            textField.optionArray = cityNameArray.map { $0.cityName }
        }
    }
    
    var setCityList: [CityModel] {
        get {
            return cityNameArray
        }
        set {
            cityNameArray = newValue
        }
    }
    
    @IBInspectable var labelText: String {
        didSet {
            label.text = labelText
            if isRequired {
                setAsterisk()
            } else {
                label.attributedText = nil
                label.text = labelText
            }
        }
    }
    
    @IBInspectable var isRequired: Bool = false {
        didSet {
            if isRequired {
                setAsterisk()
            } else {
                label.attributedText = nil
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        labelText = ""
        super.init(coder: aDecoder)
        backgroundColor = .clear
        let screenWidth = UIScreen.main.bounds.width
        let sizeAdjustment = trunc((screenWidth - 320.0) / 40.0)
        offset += sizeAdjustment
        setupSubviews()
        
        textField.selectedRowColor = .lightGray
       // textField.delegate = self

        textField.hideOptionsWhenSelect = true
        
        textField.checkMarkEnabled = false
    }
    
    func setupSubviews() {
        self.addSubview(label)
        self.addSubview(textField)
 
        label.font = FontHelper.font(type: .franklin, size: .small)
        label.textColor = .lightGrayText
        textField.borderStyle = .roundedRect
        textField.font = FontHelper.font(type: .roboto, size: .small)
        label.autoPinEdge(toSuperviewEdge: .top)
        label.autoPinEdge(toSuperviewEdge: .left)
        label.autoSetDimension(.height, toSize: 20.0)
        textField.autoPinEdge(.top, to: .bottom, of: label, withOffset: offset)
        textField.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        textField.autoMatch(.width, to: .height, of: textField, withMultiplier: 8.0)
        
        textField.arrowSize = 0
    }
    
    func setAsterisk() {
        guard label.text != nil else { return }
        let asteriskText = label.text! + " *"
        
        let attrText = NSMutableAttributedString(string: asteriskText)
        let range = (asteriskText as NSString).range(of: " *")
        attrText.setAttributes([.foregroundColor: UIColor.red], range: range)
        label.attributedText = attrText
    }
}
