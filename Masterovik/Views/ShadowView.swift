//
//  ShadowView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/7/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ShadowView: UIView {
    
    var borderView = UIView()

    var shadowColor: UIColor {
        set {
            layer.shadowColor = newValue.cgColor
        }
        get {
            if let cgColor = layer.shadowColor {
                return UIColor(cgColor: cgColor)
            }
            return .clear
        }
    }
    
    var shadowRadius: CGFloat {
        set {
            layer.shadowRadius = newValue
        }
        get {
            return layer.shadowRadius
        }
    }
    
    var shadowOffset: CGSize {
        set {
            layer.shadowOffset = newValue
        }
        get {
            return layer.shadowOffset
        }
    }
    
    var shadowCorners: CGFloat = 5.0 {
        didSet {
            updateShadow()
        }
    }
    
    func updateShadow() {
        let outerPath = UIBezierPath(roundedRect: self.bounds.offsetBy(dx: 1.0, dy: 1.0), cornerRadius: shadowCorners)
        let innerPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: shadowCorners)
        outerPath.append(innerPath.reversing())
        layer.shadowPath = outerPath.cgPath
        
        borderView.cornerRadius = shadowCorners
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        backgroundColor = .clear
        layer.shadowOpacity = 0.3
        shadowColor = .black
        shadowRadius = 2.0
        shadowOffset = .zero
        clipsToBounds = false
        addSubview(borderView)
        borderView.setRoundedRect()
        borderView.autoPinEdgesToSuperviewEdges()
        borderView.backgroundColor = .white
        self.sendSubviewToBack(borderView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateShadow()
    }
}
