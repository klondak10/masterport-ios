//
//  CodeHistoryTableViewCell.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/8/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class HistoryTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var iconView: UIImageView!
    
    @IBOutlet weak var operationTypeLabel: UILabel! {
        didSet {
            operationTypeLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var cardNameLabel: UILabel! {
        didSet {
            cardNameLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var costLabel: UILabel! {
        didSet {
            costLabel.font = FontHelper.font(type: .roboto, size: .medium)
        }
    }
    
    @IBOutlet weak var balanceLabel: UILabel! {
        didSet {
            balanceLabel.font = FontHelper.font(type: .roboto, size: .small)
        }
    }
    
    
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    public func configure(for history: HistoryWalletModel) {
        setCostLabel(money: history.sum)
        setImage(money: history.sum)
        setBalanceLabel(balance: history.balance)
        setCardNameLAbel(cardID: history.cardId)
        setDataLabel(date: history.createdAt)
        setOperationTypeLabel(operation: history.operation)
    }
    
    private func setCostLabel(money: Double) {
        
        let formatter = NumberFormatter.init()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.numberStyle = .currency
        guard let string = formatter.string(for: money) else {
            self.costLabel.text = "--- --- --"
            return
        }
        
        let range = NSRange(location: 0, length: string.count)
        let attributesString = NSMutableAttributedString(string: string)
       attributesString.addAttributes([
            NSAttributedString.Key.font: FontHelper.font(type: .roboto, size: .medium)],
                                      range: range)
        if money >= 0 {
            attributesString.addAttribute(.foregroundColor, value: UIColor.darkGreen, range: range)
        } else {
            attributesString.addAttribute(.foregroundColor, value: UIColor.warningRed, range: range)
        }
        
        self.costLabel.attributedText = attributesString
    }
    
    private func setImage(money: Double) {
        if money >= 0 {
            self.iconView?.image = #imageLiteral(resourceName: "card-deposit-icon-green")
        } else {
            self.iconView?.image = #imageLiteral(resourceName: "card-withdraw-icon-red")
        }
    }
    
    private func setBalanceLabel(balance: Double) {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.numberStyle = .currency
        guard let string = formatter.string(for: balance) else {
            self.costLabel.text = "--- --- --"
            return
        }
        self.balanceLabel.text = "Баланс: \(string)"
    }
    
    private func setDataLabel(date: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        formatter.locale = Locale(identifier: "ru_RU")
        
        guard let from = formatter.date(from: date) else {
            dateLabel.text = "--.----.----"
            return
        }
        
        let toFromatter = DateFormatter()
        toFromatter.dateFormat = "dd.MM.yy"
        toFromatter.locale = Locale(identifier: "ru_RU")
        let to = toFromatter.string(from: from)
        
        self.dateLabel.text = to
    }
    
    private func setOperationTypeLabel(operation: String) {
        self.operationTypeLabel.text = operation
    }
    
    private func setCardNameLAbel(cardID: Int?) {
        guard let id = cardID , id != 0 else { cardNameLabel.isHidden = true; return }
        cardNameLabel.isHidden = false
        cardNameLabel.text = "\(id)"
    }
}
