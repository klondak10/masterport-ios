//
//  Constants.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import SideMenuSwift

let navBarButtonImageSize = CGSize(width: 25.0, height: 25.0)
let buttonImageLargeSize = CGSize(width: 60.0, height: 60.0)

let userWasYetExecutor = "userWasYetExecutor"
let userIsRegistered = "userIsRegisteredYet"
var currentRoleIsExecutor: Bool  {
    if UserDefaults.standard.bool(forKey: userIsRegistered) {
        if UserCoreDataManager.shared.getUser()?.roleId == 1 {
            return true
        } else { return false }
    } else {
        return true
    }
}

