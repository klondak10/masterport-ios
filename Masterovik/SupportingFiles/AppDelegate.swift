//
//  AppDelegate.swift
//  Masterovik
//
//  Created by Dmitriy Yurchenko on 4/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SideMenuSwift
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var sideMenu: SideMenuController?

   
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        startSetupSideMenu()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        return true
    }
    
    func startSetupSideMenu() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        window!.makeKeyAndVisible()
        let splashScreenViewController = SplashScreenViewController.instanceFromStoryboard
        let navController = UINavigationController(rootViewController: splashScreenViewController)
        navController.navigationBar.tintColor = .white
        let font = FontHelper.font(type: .franklin, size: .title)
        navController.navigationBar.titleTextAttributes = [.font: font, .foregroundColor: UIColor.white]
        
        setupSideBar(navController)
    }
    
    func setupNavbar() {
        let sideController = window?.rootViewController as! SideMenuController
        let navController = sideController.contentViewController as! UINavigationController
        let navBar = navController.navigationBar
        navBar.setBackgroundImage(UIImage(), for: .default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        navBar.isOpaque = false
        navBar.backgroundColor = .clear
    }
    
    func setupSideBar(_ navController: UINavigationController) {
        let sideMenuController = setLeftMenuForRole(navController)
        SideMenuController.preferences.basic.menuWidth = UIScreen.main.bounds.width * 0.75
        SideMenuController.preferences.basic.statusBarBehavior = .slide
        window?.rootViewController = sideMenuController
        if UserDefaults.standard.bool(forKey: userIsRegistered) {
             SideMenuController.preferences.basic.enablePanGesture = true
        } else {
             SideMenuController.preferences.basic.enablePanGesture = false
        }
        setupNavbar()
    }
    
    private func setLeftMenuForRole(_ navController: UINavigationController) -> SideMenuController{
        
        if currentRoleIsExecutor {
            return  SideMenuController(contentViewController: navController, menuViewController: LeftMenuExecutorViewController.instanceFromStoryboard)
        } else {
            return  SideMenuController(contentViewController: navController, menuViewController: LeftMenuCustomerViewController.instanceFromStoryboard)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }

    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "UserModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

