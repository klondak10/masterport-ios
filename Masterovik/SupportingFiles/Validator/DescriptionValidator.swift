//
//  CommentValidator.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

class CommentValidator : Validatable {
    func validated(_ value: String?) throws -> String {
        do {
            if let value = value {
                if try NSRegularExpression(pattern: "^.{0,95}$",
                                           options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw ValidatorError("Введено более 500 символов")
                }
            } else { throw ValidatorError("Введено более 500 символов") }
        } catch {
            throw ValidatorError("Введено более 500 символов")
        }
        return value!
    }
}
