//
//  ValidatorEnum.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

enum ValidatorEnum : String {
    case email
    case phoneNumber
    case userName
    case password
    case balance
    case description
    case nameAddress
    case sms
    case digit
}

enum ValidatorFactory {
    static func validate(for: ValidatorEnum) -> Validatable {
        switch `for` {
        case .email:
            return EmailValidator()
        case .phoneNumber:
            return PhoneNumberValidator() 
        case .password:
            return PasswordValidator()
        case .balance:
            return BalanceValidator()
        case .userName:
            return UsernameValidator()
        case .nameAddress:
            return NameAddressValidator()
        case .description:
            return CommentValidator()
        case .sms:
            return SmsCodeValidator()
        case .digit:
            return DigitValidator()
        }
    }
}
