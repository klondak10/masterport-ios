//
//  PasswordValidator.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

class PasswordValidator : Validatable {
    func validated(_ value: String?) throws -> String {
        do {
            if let value = value {
                if try NSRegularExpression(pattern: "^[A-ZА-Яа-яa-z0-9_-Ёё]{6,50}$",
                                           options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw ValidatorError("Некорректный пароль")
                }
            } else { throw ValidatorError("Некорректный пароль") }
        } catch {
            throw ValidatorError("Некорректный пароль")
        }
        return value!
    }
}
