//
//  UsernameValidator.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

class UsernameValidator : Validatable {
    func validated(_ value: String?) throws -> String {
        do {
            if let value = value {
                if try NSRegularExpression(pattern: "^[А-ЯЁа-яё|A-Za-z]{1,25}$",
                                           options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw ValidatorError("Некорректное имя")
                }
            } else { throw ValidatorError("Некорректное имя") }
        } catch {
            throw ValidatorError("Некорректное имя")
        }
        return value!
    }
}
