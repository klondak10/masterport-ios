//
//  INNValidator.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

class BalanceValidator : Validatable {
    func validated(_ value: String?) throws -> String {
        do {
            if let value = value {
                if try NSRegularExpression(pattern: "^[0-9]+(\\,[0-9]{1,2})?$",
                                           options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw ValidatorError("Будет возвращен 0")
                }
            } else { throw  ValidatorError("Будет возвращен 0") }
        } catch {
            throw ValidatorError("Будет возвращен 0")
        }
        return value!
    }
}
