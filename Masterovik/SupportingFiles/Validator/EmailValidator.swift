//
//  EmailValidator.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

class EmailValidator : Validatable {
    func validated(_ value: String?) throws -> String {
        do {
            if let value = value {
                if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$",
                                           options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                     throw ValidatorError("Неправильный Email")
                }
            } else { throw ValidatorError("Пустой Email") }
        } catch {
            throw ValidatorError("Неправильный Email")
        }
        return value!
    }
}
