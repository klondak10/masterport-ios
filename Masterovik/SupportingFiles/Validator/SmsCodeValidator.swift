//
//  SmsCodeValidator.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

class SmsCodeValidator : Validatable {
    func validated(_ value: String?) throws -> String {
        do {
            if let value = value {
                if try NSRegularExpression(pattern: "^[\\d]{4}$",
                                           options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw ValidatorError("Код содержит 4 цифры")
                }
            } else { throw ValidatorError("Поле не может быть пустым") }
        } catch {
            throw ValidatorError("Код содержит 4 цифры")
        }
        return value!
    }
}
