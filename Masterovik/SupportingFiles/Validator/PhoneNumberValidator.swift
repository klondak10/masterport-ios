//
//  PhoneNumberValidator.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

class PhoneNumberValidator : Validatable {
    func validated(_ value: String?) throws -> String {
        do {
            if let value = value {
                if try NSRegularExpression(pattern: "^\\+\\d\\s\\(\\d{3}\\)\\d{3}-\\d{4,5}$",
                                           options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw ValidatorError("Некорректный номер")
                }
            } else { throw ValidatorError("Некорректный номер") }
        } catch {
            throw ValidatorError("Некорректный номер")
        }
        return value!
    }
}
