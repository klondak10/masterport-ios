//
//  Validatable.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

protocol Validatable {
    func validated(_ value: String?) throws -> String
}
