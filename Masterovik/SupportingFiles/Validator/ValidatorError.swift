//
//  ValidatorError.swift
//  Stroylandiya
//
//  Created by Roman Haiduk on 7/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct ValidatorError : Error {
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}
