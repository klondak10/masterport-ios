//
//  DigitValidator.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

class DigitValidator : Validatable {
    func validated(_ value: String?) throws -> String {
        do {
            if let value = value {
                let allowedCharacters = CharacterSet.decimalDigits
                let characterSet = CharacterSet(charactersIn: value)
                if !allowedCharacters.isSuperset(of: characterSet) || value == "" {
                    throw ValidatorError("Введено более 500 символов")
                }
            } else { throw ValidatorError("Введено более 500 символов") }
        } catch {
            throw ValidatorError("Введено более 500 символов")
        }
        return value!
    }
}
