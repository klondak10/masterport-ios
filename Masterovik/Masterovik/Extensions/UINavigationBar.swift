//
//  UINavigationBar.swift
//  Masterovik
//
//  Created by Ирина on 4/26/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: 95.7)
    }
    
}
