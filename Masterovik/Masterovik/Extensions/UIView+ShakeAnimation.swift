//
//  UIView+ShakeAnimation.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 11.06.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIView {
    func shakeAnimation(for duration: TimeInterval = 0.5, withTranslation translation: CGFloat = 10) {
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.3) {
            self.transform = CGAffineTransform(translationX: translation, y: 0)
        }
        
        propertyAnimator.addAnimations ({
            self.transform = CGAffineTransform(translationX: 0, y: 0)
        }, delayFactor: 0.2)
        
        propertyAnimator.startAnimation()
    }
    
    func validationIsFalse() {
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = 1.0
        self.cornerRadius = 5.0
        self.shakeAnimation()
    }
}
