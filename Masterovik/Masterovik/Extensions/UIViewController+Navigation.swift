//
//  UIViewController+SimplePush.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIViewController {
    
    var navCon: UINavigationController? {
        return sideMenuController?.contentViewController as? UINavigationController
    }
    
    func pushVC(_ viewController: UIViewController) {
        guard let navController = navCon else {return}
        if let topViewController = navController.topViewController {
            topViewController.navigationItem.backBarButtonItem = .empty
        }
        navController.pushViewController(viewController, animated: true)
    }
}
