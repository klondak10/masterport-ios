//
//  UIView+NibInstance.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/8/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIView {
    static var nibName: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: nibName, bundle: .main)
    }
    
    static var instanceFromNib: UIView {
        if let view = nib.instantiate(withOwner: self, options: nil).first as? UIView {
            let container = self.init()
            container.addSubview(view)
            view.autoPinEdgesToSuperviewEdges()
            return container
        }
        return self.init()
    }
    

}
