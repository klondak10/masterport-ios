//
//  UIView+RoundCorners.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/15/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            if newValue > 0.1 {
                clipsToBounds = true
            }
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get
        {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        set {
            layer.borderColor = newValue.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            } else {
                return .clear
            }
        }
    }
    
//    @IBInspectable var shadowColor: UIColor {
//        set {
//
//        }
//    }
    
    func setRoundedRect() {
        cornerRadius = 5.0
        borderWidth = 0.6
        borderColor = .textFieldBorderColor
    }
}
