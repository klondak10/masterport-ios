//
//  MenuItemView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class MenuItemView: ImageTitleButtonView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        label.font = FontHelper.font(type: .myriad, size: .medium)
        label.textColor = .black
        autoMatch(.width, to: .height, of: self, withMultiplier: 10.0)
    }
}
