//
//  SwitchItemView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class SwitchItemView: UIView {

    let label = UILabel()
    let switchView = UISwitch()
    
    @IBInspectable var labelText: String? {
        set {
            label.text = newValue
        }
        get {
            return label.text
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(label)
        addSubview(switchView)
        label.font = FontHelper.font(type: .franklin, size: .medium)
        label.textColor = .black
        switchView.tintColor = .white
        switchView.onTintColor = .white
        switchView.thumbTintColor = .gray
        switchView.layer.borderColor = UIColor.black.cgColor
        switchView.layer.borderWidth = 0.5
        switchView.cornerRadius = 15.0
        
        label.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .right)
        switchView.autoAlignAxis(toSuperviewAxis: .horizontal)
        switchView.autoPinEdge(toSuperviewEdge: .top, withInset: 0, relation: .greaterThanOrEqual)
        switchView.autoPinEdge(toSuperviewEdge: .right)
        switchView.autoPinEdge(.left, to: .right, of: label, withOffset: 10.0)
    }
}
