//
//  HistoryTableViewCell.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CodeHistoryTableViewCell: BaseTableViewCell {
    
    let containerView = UIView()
    let iconImageView = UIImageView()
    let operationTypeLabel = UILabel()
    let cardNamelabel = UILabel()
    let costLabel = UILabel()
    let dateLabel = UILabel()
    let balanceLabel = UILabel()
    
    class func dequeue(from tableView: UITableView) -> CodeHistoryTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier) as? CodeHistoryTableViewCell ?? CodeHistoryTableViewCell()
        cell.commonInit()
        return cell
    }
    
    func commonInit() {
        contentView.addSubview(containerView)
        containerView.setRoundedRect()
        containerView.addSubview(iconImageView)
        containerView.addSubview(operationTypeLabel)
        containerView.addSubview(cardNamelabel)
        containerView.addSubview(costLabel)
        containerView.addSubview(dateLabel)
        containerView.addSubview(balanceLabel)
        iconImageView.contentMode = .scaleAspectFit
        operationTypeLabel.numberOfLines = 0
        operationTypeLabel.lineBreakMode = .byWordWrapping
        operationTypeLabel.textColor = .black
        operationTypeLabel.font = FontHelper.font(type: .franklin, size: .small)
        cardNamelabel.textColor = .black
        cardNamelabel.font = FontHelper.font(type: .franklin, size: .medium)
        costLabel.font = FontHelper.font(type: .roboto, size: .medium)
        dateLabel.textColor = .lightGrayText
        dateLabel.font = FontHelper.font(type: .franklin, size: .small)
        balanceLabel.textColor = .black
        balanceLabel.font = FontHelper.font(type: .roboto, size: .small)
        containerView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 15.0, left: 0.0, bottom: 0.0, right: 0.0))
        iconImageView.autoPinEdge(toSuperviewEdge: .top, withInset: 15.0)
        iconImageView.autoPinEdge(toSuperviewEdge: .left, withInset: 10.0)
        iconImageView.autoSetDimensions(to: CGSize(width: 20.0, height: 20.0))
        operationTypeLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 15.0)
        operationTypeLabel.autoPinEdge(.left, to: .right, of: iconImageView, withOffset: 15.0)
        operationTypeLabel.autoSetDimension(.width, toSize: 120.0)
        cardNamelabel.autoPinEdge(toSuperviewEdge: .left, withInset: 10.0)
        cardNamelabel.autoPinEdge(.top, to: .bottom, of: iconImageView, withOffset: 15.0)
        costLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 15.0)
        costLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 10.0)
        costLabel.autoPinEdge(.top, to: .bottom, of: cardNamelabel)
        dateLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 15.0)
        dateLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 15.0)
        balanceLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 15.0)
        balanceLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 15.0)
    }
}
