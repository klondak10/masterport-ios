//
//  AddButtonView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class AddButtonView: UIView {
    
    @IBInspectable var labelText: String? {
        set {
            label.text = newValue
        }
        get {
            return label.text
        }
    }
    
    let label = UILabel()
    let imageView = UIImageView()
    let button = UIButton()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        cornerRadius = 5.0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.darkGreen.cgColor
        backgroundColor = .clear
        
        addSubview(label)
        addSubview(imageView)
        addSubview(button)
        label.textAlignment = .center
        label.font = FontHelper.font(type: .franklin, size: .medium)
        label.textColor = .black
        label.isUserInteractionEnabled = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "add-icon-green")
        imageView.isUserInteractionEnabled = false
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(pressButton(_:)), for: .touchDown)
        button.addTarget(self, action: #selector(freeButton(_:)), for: .touchUpInside)
        button.addTarget(self, action: #selector(freeButton(_:)), for: .touchUpOutside)
        
        label.autoAlignAxis(toSuperviewAxis: .vertical)
        label.autoPinEdge(toSuperviewEdge: .top)
        label.autoPinEdge(toSuperviewEdge: .bottom)
        imageView.autoPinEdge(.right, to: .left, of: label, withOffset: -10.0)
        imageView.autoAlignAxis(toSuperviewAxis: .horizontal)
        imageView.autoSetDimensions(to: CGSize(width: 20.0, height: 20.0))
        imageView.autoMatch(.width, to: .height, of: imageView)
        button.autoPinEdgesToSuperviewEdges()
    }
    
    @objc func pressButton(_ sender: UIButton) {
        let color = self.label.textColor
        self.label.textColor = color?.withAlphaComponent(0.3)
        imageView.alpha = 0.3
    }
    
    @objc func freeButton(_ sender: UIButton) {
        let color = self.label.textColor
        self.label.textColor = color?.withAlphaComponent(1.0)
        imageView.alpha = 1.0
    }
}
