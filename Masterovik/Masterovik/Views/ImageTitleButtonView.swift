//
//  ImageTitleButtonView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/23/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

@IBDesignable class ImageTitleButtonView: UIView {
    
    @IBInspectable var image: UIImage? {
        set {
            imageView.image = image
        }
        get {
            return imageView.image
        }
    }

    @IBInspectable var text: String? {
        set {
            label.text = newValue
        }
        get {
            return label.text
        }
    }
    
    let button = UIButton(type: .system)
    let imageView = UIImageView()
    let label = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addSubview(button)
        addSubview(imageView)
        addSubview(label)
        
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = false
        label.isUserInteractionEnabled = false
        button.backgroundColor = .white
        self.sendSubviewToBack(button)
        button.addTarget(self, action: #selector(pressButton(_:)), for: .touchDown)
        button.addTarget(self, action: #selector(freeButton(_:)), for: .touchUpInside)
        button.addTarget(self, action: #selector(freeButton(_:)), for: .touchUpOutside)
        
        button.autoPinEdgesToSuperviewEdges()
        imageView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .right)
        imageView.autoMatch(.width, to: .height, of: imageView)
        label.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .left)
        label.autoPinEdge(.left, to: .right, of: imageView, withOffset: 10.0)
    }
    
    @objc func pressButton(_ sender: UIButton) {
        let color = self.label.textColor
        self.label.textColor = color?.withAlphaComponent(0.3)
        imageView.alpha = 0.3
    }
    
    @objc func freeButton(_ sender: UIButton) {
        let color = self.label.textColor
        self.label.textColor = color?.withAlphaComponent(1.0)
        imageView.alpha = 1.0
    }
}
