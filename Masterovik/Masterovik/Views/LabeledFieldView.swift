//
//  LabeledFieldView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import PureLayout

class LabeledFieldView: UIView, Chekable {
    
    var maxCharacters = 0
    
    let label = UILabel()
    let textField = UITextField()
    var offset: CGFloat = 5.0
    
    @IBInspectable var labelText: String {
        didSet {
            label.text = labelText
            if isRequired {
                setAsterisk()
            } else {
                label.attributedText = nil
                label.text = labelText
            }
        }
    }
    
    @IBInspectable var isRequired: Bool = false {
        didSet {
            if isRequired {
                setAsterisk()
            } else {
                label.attributedText = nil
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        labelText = ""
        super.init(coder: aDecoder)
        backgroundColor = .clear
        let screenWidth = UIScreen.main.bounds.width
        let sizeAdjustment = trunc((screenWidth - 320.0) / 40.0)
        offset += sizeAdjustment
        setupSubviews()
    }
    
    func setupSubviews() {
        self.addSubview(label)
        self.addSubview(textField)
//        let fontIncrease = trunc((UIScreen.main.bounds.width - 320.0) / 20.0)
//        label.font = UIFont(name: franklinFontName, size: smallTextFontSize + fontIncrease)
        label.font = FontHelper.font(type: .franklin, size: .small)
        label.textColor = .lightGrayText
        textField.borderStyle = .roundedRect
//        textField.font = UIFont(name: robotoFontName, size: smallTextFontSize + fontIncrease)
        textField.font = FontHelper.font(type: .roboto, size: .small)
        label.autoPinEdge(toSuperviewEdge: .top)
        label.autoPinEdge(toSuperviewEdge: .left)
        label.autoSetDimension(.height, toSize: 20.0)
        textField.autoPinEdge(.top, to: .bottom, of: label, withOffset: offset)
        textField.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        textField.autoMatch(.width, to: .height, of: textField, withMultiplier: 8.0)
    }
    
    func setAsterisk() {
        guard label.text != nil else { return }
        let asteriskText = label.text! + " *"
        
        let attrText = NSMutableAttributedString(string: asteriskText)
        let range = (asteriskText as NSString).range(of: " *")
        attrText.setAttributes([.foregroundColor: UIColor.red], range: range)
        label.attributedText = attrText
    }
    
    func checkIfEmpty() -> Bool {
        guard isRequired else { return false }
        guard let text = textField.text else { return true }
        if text.isEmpty {
            textField.validationIsFalse()
            return true
        } else {
            return false
        }
    }
    
    func getTextField() -> UITextField {
        return textField
    }
}
