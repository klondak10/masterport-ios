//
//  NextButton.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class NextButton: UIButton {
    
    override var bounds: CGRect {
        didSet {
            self.cornerRadius = min(self.bounds.width, self.bounds.height) / 2.0
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 25.0, bottom: 0.0, right: 25.0)
        self.titleLabel?.textAlignment = .center
        titleLabel?.font = FontHelper.font(type: .franklin, size: .medium)
        setContentHuggingPriority(UILayoutPriority(rawValue: 200.0), for: .horizontal)
        setContentCompressionResistancePriority(UILayoutPriority(rawValue: 800.0), for: .horizontal)
        let screenWidth = UIScreen.main.bounds.width
        autoSetDimension(.width, toSize: screenWidth * 0.5, relation: .greaterThanOrEqual)
        autoSetDimension(.height, toSize: screenWidth * 0.1)
        //let point = CGPoint(x: 10, y: 10)
    }
}
