//
//  ExpandingDescriptionView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/10/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

@IBDesignable class ExpandingDescriptionView: UIView {
    
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.setRoundedRect()
        }
    }
    
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.titleLabel?.font = FontHelper.font(type: .franklin, size: .medium)
            button.setTitle(mainText, for: .normal)
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.font = FontHelper.font(type: .roboto, size: .small)
            descriptionLabel.text = descriptionText
        }
    }
    
    @IBOutlet weak var descriptionLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionLabelBottomConstraint: NSLayoutConstraint!
    
    @IBInspectable var mainText: String = ""
    
    @IBInspectable var descriptionText: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    func commonInit() {
//        containerView.setRoundedRect()
//        button.titleLabel?.font = FontHelper.font(type: .franklin, size: .medium)
//        button.setTitle(mainText, for: .normal)
//        descriptionLabel.font = FontHelper.font(type: .roboto, size: .small)
//        descriptionLabel.text = descriptionText
    }
    
    func toggle(expanded: Bool) {
        if descriptionLabelHeightConstraint != nil {
            descriptionLabelHeightConstraint?.isActive = !expanded
        }
        if descriptionLabelBottomConstraint != nil {
            descriptionLabelBottomConstraint?.constant = expanded ? -25.0 : 0.0
        }
        if expanded {
            containerView.backgroundColor = UIColor(red: 0.7, green: 1.0, blue: 0.7, alpha: 1.0)
        } else {
            containerView.backgroundColor = .white
        }
    }
}
