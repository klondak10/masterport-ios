//
//  ListDisclosureItem.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/23/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ListDisclosureItem: UIView {

    @IBInspectable var text: String? {
        set {
            label.text = newValue
        }
        get {
            return label.text
        }
    }
    
    let button = UIButton(type: .system)
    let imageView = UIImageView()
    let label = UILabel()
    let disclosureImageView = UIImageView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addSubview(button)
        addSubview(imageView)
        addSubview(label)
        addSubview(disclosureImageView)
        
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = false
        label.font = FontHelper.font(type: .myriad, size: .medium)
        label.textColor = .black
        label.isUserInteractionEnabled = false
        disclosureImageView.contentMode = .scaleAspectFit
        disclosureImageView.image = UIImage(named: "disclosure-icon")
        disclosureImageView.isUserInteractionEnabled = false
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(pressButton(_:)), for: .touchDown)
        button.addTarget(self, action: #selector(freeButton(_:)), for: .touchUpInside)
        button.addTarget(self, action: #selector(freeButton(_:)), for: .touchUpOutside)
        self.sendSubviewToBack(button)
        
        button.autoPinEdgesToSuperviewEdges()
        imageView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 7.0, left: 7.0, bottom: 7.0, right: 7.0), excludingEdge: .right)
        imageView.autoMatch(.width, to: .height, of: imageView)
        label.autoPinEdge(toSuperviewEdge: .top)
        label.autoPinEdge(toSuperviewEdge: .bottom)
        label.autoPinEdge(.left, to: .right, of: imageView, withOffset: 10.0)
        disclosureImageView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0), excludingEdge: .left)
        disclosureImageView.autoMatch(.width, to: .height, of: disclosureImageView)
        disclosureImageView.autoPinEdge(.left, to: .right, of: label, withOffset: 10.0)
        autoMatch(.width, to: .height, of: self, withMultiplier: 8.0)
    }
    
    @objc func pressButton(_ sender: UIButton) {
        let color = self.label.textColor
        self.label.textColor = color?.withAlphaComponent(0.3)
        imageView.alpha = 0.3
        disclosureImageView.alpha = 0.3
    }
    
    @objc func freeButton(_ sender: UIButton) {
        let color = self.label.textColor
        self.label.textColor = color?.withAlphaComponent(1.0)
        imageView.alpha = 1.0
        disclosureImageView.alpha = 1.0
    }
}
