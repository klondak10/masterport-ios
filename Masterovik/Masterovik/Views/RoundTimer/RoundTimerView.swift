//
//  RoundTimerView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/15/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class RoundTimerView: UIView {
    
    let progressView = RoundProgressView()
    let timerLabel = UILabel()
    var timer = Timer()
    
    weak var delegate: RoundTimerDelegate?
    
    @IBInspectable var fullTime: CGFloat = 60.0
    
    var elapsedTime: CGFloat = 0.0 {
        didSet {
            progressView.progress = progress
            let remainingTime = fullTime - elapsedTime
            let truncedTime: CGFloat = trunc(remainingTime)
            timerLabel.text = String(format: "%d", Int(truncedTime))
        }
    }
    
    var progressColor: UIColor = .black {
        didSet {
            progressView.progressColor = progressColor
        }
    }
    
    var emptyColor: UIColor = .white {
        didSet {
            progressView.emptyColor = emptyColor
        }
    }
    
    var lineWidth: CGFloat = 3.3
    
    var progress: CGFloat {
        get {
//            return CGFloat(elapsedTime) / CGFloat(fullTime)
            return elapsedTime / fullTime
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = .clear
        self.addSubview(progressView)
        self.addSubview(timerLabel)
        progressView.progress = progress
        progressView.progressColor = progressColor
        progressView.emptyColor = emptyColor
        progressView.lineWidth = lineWidth
        timerLabel.font = FontHelper.font(type: .franklin, size: .medium)
        timerLabel.textColor = .white
        progressView.autoPinEdgesToSuperviewEdges()
        timerLabel.autoCenterInSuperview()
    }

    func startTimer() {
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerFire), userInfo: nil, repeats: true)
    }
    
    func pauseTimer() {
        timer.invalidate()
    }
    
    func stopTimer() {
        timer.invalidate()
        elapsedTime = 0
    }
    
    @objc func timerFire() {
        elapsedTime += 1.0
        progressView.progress = progress
        if elapsedTime >= fullTime {
            stopTimer()
            if delegate != nil {
                delegate!.timerEnded()
            }
        }
    }
}

protocol RoundTimerDelegate: class {
    func timerEnded()
}
