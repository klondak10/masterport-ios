//
//  CodeHistoryTableViewCell.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/8/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class HistoryTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var iconView: UIImageView!
    
    @IBOutlet weak var operationTypeLabel: UILabel! {
        didSet {
            operationTypeLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var cardNameLabel: UILabel! {
        didSet {
            cardNameLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var costLabel: UILabel! {
        didSet {
            costLabel.font = FontHelper.font(type: .roboto, size: .medium)
        }
    }
    
    @IBOutlet weak var balanceLabel: UILabel! {
        didSet {
            balanceLabel.font = FontHelper.font(type: .roboto, size: .small)
        }
    }
    
    
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
}
