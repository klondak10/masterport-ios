//
//  CodeExpandingDescriptionView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CodeExpandingDescriptionView: UIView {
    
    @IBInspectable var mainText: String = "" {
        didSet {
            button.titleLabel?.text = mainText
            button.setTitle(mainText, for: .normal)
        }
    }
    
    @IBInspectable var descriptionText: String = "" {
        didSet {
            descriptionLabel.text = descriptionText
        }
    }
    
    var shadowView = ShadowView()
    let button = UIButton(type: .system)
    let descriptionLabel = UILabel()
    var descriptionHeightConstraint: NSLayoutConstraint?
    var descriptionBottomConstraint: NSLayoutConstraint?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupSubviews()
    }
    
    func setupSubviews() {
//        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 5.0).cgPath
        addSubview(shadowView)
        shadowView.addSubview(button)
        shadowView.addSubview(descriptionLabel)
        shadowView.backgroundColor = .white
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.textColor = .darkGreen
        button.setTitleColor(.darkGreen, for: .normal)
        button.titleLabel?.font = FontHelper.font(type: .franklin, size: .medium)
        descriptionLabel.isUserInteractionEnabled = false
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = FontHelper.font(type: .roboto, size: .small)
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.numberOfLines = 0
        shadowView.autoPinEdgesToSuperviewEdges()
        button.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .bottom)
        button.autoSetDimension(.height, toSize: 50.0, relation: .greaterThanOrEqual)
        descriptionBottomConstraint = descriptionLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0), excludingEdge: .top)[1]
        descriptionLabel.autoPinEdge(.top, to: .bottom, of: button)
        descriptionHeightConstraint = descriptionLabel.autoSetDimension(.height, toSize: 0.0)
    }
    
    func toggle(expanded: Bool) {
        if descriptionHeightConstraint != nil {
            descriptionHeightConstraint?.isActive = !expanded
        }
        if descriptionBottomConstraint != nil {
            descriptionBottomConstraint?.constant = expanded ? -25.0 : 0.0
        }
        if expanded {
            shadowView.borderView.backgroundColor = UIColor(red: 0.7, green: 1.0, blue: 0.7, alpha: 1.0)
        } else {
            shadowView.borderView.backgroundColor = .white
        }
    }
}
