//
//  BackgroundGradientView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BackgroundGradientView: UIView {
    
    let gradient = CAGradientLayer()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        gradient.colors = [UIColor.darkGreen.cgColor, UIColor.lightGreen.cgColor]
        layer.addSublayer(gradient)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        gradient.colors = [UIColor.darkGreen.cgColor, UIColor.lightGreen.cgColor]
        layer.addSublayer(gradient)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = self.bounds
    }
    
}
