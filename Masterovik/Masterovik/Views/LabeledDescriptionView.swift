//
//  LabeledDescriptionView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/10/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class LabeledDescriptionView: UIView {

    let label = UILabel()
    let textView = UITextView()
    var offset: CGFloat = 5.0
    
    var minHeight: CGFloat?
    var maxHeight: CGFloat?
    
    
    @IBInspectable var labelText: String {
        didSet {
            label.text = labelText
            if isRequired {
                setAsterisk()
            }
            else
            {
                label.attributedText = nil
                label.text = labelText
            }
        }
    }
    
    @IBInspectable var isRequired: Bool = false {
        didSet {
            if isRequired {
                setAsterisk()
            }
            else
            {
                label.attributedText = nil
            }
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        labelText = ""
        super.init(coder: aDecoder)
        backgroundColor = .clear
        let screenWidth = UIScreen.main.bounds.width
        let sizeAdjustment = trunc((screenWidth - 320.0) / 40.0)
        offset += sizeAdjustment
        setupSubviews()
    }
    
    func setupSubviews()
    {
        self.addSubview(label)
        self.addSubview(textView)
        //        let fontIncrease = trunc((UIScreen.main.bounds.width - 320.0) / 20.0)
        //        label.font = UIFont(name: franklinFontName, size: smallTextFontSize + fontIncrease)
        label.font = FontHelper.font(type: .franklin, size: .small)
        label.textColor = .lightGrayText
        textView.setRoundedRect()
        textView.cornerRadius = 5.0
        //        textField.font = UIFont(name: robotoFontName, size: smallTextFontSize + fontIncrease)
        textView.font = FontHelper.font(type: .roboto, size: .small)
        textView.bounces = false
        textView.setContentHuggingPriority(UILayoutPriority(rawValue: 200.0), for: .vertical)
        label.autoPinEdge(toSuperviewEdge: .top)
        label.autoPinEdge(toSuperviewEdge: .left)
        label.autoSetDimension(.height, toSize: 20.0)
        textView.autoPinEdge(.top, to: .bottom, of: label, withOffset: offset)
        textView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        textView.autoSetDimension(.height, toSize: 50.0, relation: .greaterThanOrEqual)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateTextViewFrame()
    }
    
    func setAsterisk() {
        guard label.text != nil else {
            return
        }
        let asteriskText = label.text! + " *"
        
        let attrText = NSMutableAttributedString(string: asteriskText)
        let range = (asteriskText as NSString).range(of: " *")
        attrText.setAttributes([.foregroundColor: UIColor.red], range: range)
        label.attributedText = attrText
    }
    
    func updateTextViewFrame() {
        let frame = textView.frame
        var newHeight = textView.contentSize.height
        if maxHeight != nil {
            newHeight = min(newHeight, maxHeight!)
        }
        if minHeight != nil {
            newHeight = max(newHeight, minHeight!)
        }
        textView.frame = CGRect(origin: frame.origin, size: CGSize(width: frame.width, height: newHeight))
    }
}
