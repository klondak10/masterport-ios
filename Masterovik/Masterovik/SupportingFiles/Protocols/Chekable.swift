//
//  Chekable.swift
//  Masterovik
//
//  Created by Roman Haiduk on 6/14/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//
import UIKit

protocol Chekable {
    
    func getTextField() -> UITextField
    
    func checkIfEmpty() -> Bool
}
