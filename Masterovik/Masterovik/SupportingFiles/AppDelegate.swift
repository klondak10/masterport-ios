//
//  AppDelegate.swift
//  Masterovik
//
//  Created by Dmitriy Yurchenko on 4/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SideMenuSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        window!.makeKeyAndVisible()
        let splashScreenViewController = SplashScreenViewController.instanceFromStoryboard
        let navController = UINavigationController(rootViewController: splashScreenViewController)
        navController.navigationBar.tintColor = .white
        let font = FontHelper.font(type: .franklin, size: .title)
        navController.navigationBar.titleTextAttributes = [.font: font, .foregroundColor: UIColor.white]

        SideMenuController.preferences.basic.enablePanGesture = true
        SideMenuController.preferences.basic.menuWidth = UIScreen.main.bounds.width * 0.75
        let sideMenuController = SideMenuController(contentViewController: navController, menuViewController: LeftMenuViewController.instanceFromStoryboard)
//        window!.rootViewController = navController
        
        window?.rootViewController = sideMenuController
        setupNavbar()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        return true
    }
    
    func setupNavbar() {
        let sideController = window?.rootViewController as! SideMenuController
        let navController = sideController.contentViewController as! UINavigationController
        let navBar = navController.navigationBar
        navBar.setBackgroundImage(UIImage(), for: .default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        navBar.isOpaque = false
        navBar.backgroundColor = .clear
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

