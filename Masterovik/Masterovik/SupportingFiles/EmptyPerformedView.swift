//
//  EmptyPerformedView.swift
//  Masterovik
//
//  Created by Ирина on 4/21/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class EmptyCanceledView: UIView {

    @IBOutlet weak var emptyPerformedView: UIView!
    @IBOutlet weak var emptyPerfImageView: UIImageView!
    @IBOutlet weak var emptyPerformedLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }

}
