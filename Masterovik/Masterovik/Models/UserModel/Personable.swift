//
//  Person.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 11.06.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

enum PersonableKeys : String, CodingKey {
    case id
    case cityId = "city_id"
    case firstName = "first_name"
    case lastName = "last_name"
    case phone
    case balance
    case roleId = "role_id"
    case staticticsId = "statistics_id"
    case email
    case invite
    case invitedBy = "invited_by"
    case companyName = "company_name"
    case companyDescription = "company_description"
}

protocol Personable : Codable{
    
    var id: Int  { get }
    var cityId: Int { get }
    var firstName: String { get }
    var lastName: String { get }
    var phone: String { get }
    var balance: Int? { get }
    var roleId: Int? { get }
    var staticticsId: Int? { get }
    var email: String? { get }
    var invite: String? { get }
    var invitedBy: Int? { get }
}

extension Personable {
    var Name : String {
        return "\(firstName) + \(lastName)"
    }
    
    var City : String {
        if let cityDesc = CityList(rawValue: cityId)?.description {
            return cityDesc
        } else {
            return "Неизвестный город"
        }
    }
    
    var Balance : String {
        return "\(balance!) ₽"
    }
}

