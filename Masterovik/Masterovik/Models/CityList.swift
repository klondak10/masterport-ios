//
//  CityList.swift
//  Masterovik
//
//  Created by Roman Haiduk on 6/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

enum CityList : Int {
    case Khabarovsk
    case Taganrog
    case Azov
}

extension CityList : CustomStringConvertible, CaseIterable {

    var description: String {
        switch self {
            
        case .Khabarovsk:  return "Хабаровск"
        case .Taganrog: return "Таганрог"
        case .Azov: return "Азов"
        }
    }
}
