//
//  FontHelper.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

enum FontNames: String {
    case franklin = "FranklinGothic-Medium"
    case roboto = "Roboto-Regular"
    case myriad = "MyriadPro-Regular"
}

enum FontSize: CGFloat {
    case small = 12.0
    case medium = 16.0
    case large = 24.0
    case title = 20.0
}

class FontHelper: NSObject {
    
    static private var fontIncreaseForScreenSize: CGFloat {
        get {
            return trunc((UIScreen.main.bounds.width - 320.0) / 20.0)
        }
    }
    
    let screenTitleFontSize: CGFloat = 24.0
    let usualTextFontSize: CGFloat = 16.0
    let smallTextFontSize: CGFloat = 12.0
    
    let franklinFontName = "FranklinGothic-Medium"
    let robotoFontName = "Roboto-Regular"
    
    static func font(type: FontNames, size: FontSize) -> UIFont {
        if let font = UIFont(name: type.rawValue, size: size.rawValue + fontIncreaseForScreenSize) {
            return font
        } else {
            return UIFont.systemFont(ofSize: UIFont.systemFontSize)
        }
    }

}
