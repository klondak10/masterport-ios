//
//  CardTableViewCell.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CardTableViewCell: BaseTableViewCell {

    
    @IBOutlet weak var cardImageView: UIImageView!
    
    @IBOutlet weak var cardNumberLabel: UILabel! {
        didSet {
            cardNumberLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    @IBOutlet weak var optionsButton: UIButton!
    
    
    @IBAction func touchOptionsButton(_ sender: Any) {
    }
    
}
