//
//  MyCardsViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class MyCardsViewController: UIViewController, TabControllerSelectable, UITableViewDataSource, UITableViewDelegate {
    
    
    var items: Array<Any> = [NSObject()]
    
    
    @IBOutlet weak var helloLabel: UILabel!
    
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var myCardsLabel: UILabel!
    
    @IBOutlet weak var tableView: AutoHeightTableView! {
        didSet {
            CardTableViewCell.registerNib(for: tableView)
        }
    }
    
    @IBOutlet weak var addCardButtonView: AddButtonView! {
        didSet {
            addCardButtonView.button.addTarget(self, action: #selector(touchAddCardButton(_:)), for: .touchUpInside)
        }
    }
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        // Do any additional setup after loading the view.
    }
    
    func selectedBy(tabController: TabViewController) {
        tabController.navigationItem.rightBarButtonItem = nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CardTableViewCell.dequeue(tableView: tableView)
        cell.cardImageView.image = UIImage(named: "visa-card-icon")
        cell.cardNumberLabel.text = "12345678******90"
        cell.optionsButton.imageView!.contentMode = .scaleAspectFit
        return cell
    }
    
    @objc func touchAddCardButton(_ sender: UIButton) {
        items.append(NSObject())
        tableView.reloadData()
    }
    
    
    @IBAction func touchDepositButton(_ sender: Any) {
        pushVC(BalanceViewController.instanceFromStoryboard)
    }
    

}
