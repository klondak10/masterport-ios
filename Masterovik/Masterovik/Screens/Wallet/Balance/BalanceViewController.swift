//
//  BalanceViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import iOSDropDown

class BalanceViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    var items: Array<Any> = [NSObject()]
    var selectedItemsIndexes: Array<Int> = []
    
    @IBOutlet weak var payMethodLabel: UILabel! {
        didSet {
            payMethodLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var payMethodDropDown: DropDown! {
        didSet {
            payMethodDropDown.font = FontHelper.font(type: .franklin, size: .small)
            payMethodDropDown.optionArray = ["Кредитная карта"]
            payMethodDropDown.selectedIndex = 0
            payMethodDropDown.setRoundedRect()
            let methods = payMethodDropDown.optionArray
            payMethodDropDown.text = methods[payMethodDropDown.selectedIndex!]
        }
    }
    
    @IBOutlet weak var cardsLabel: UILabel! {
        didSet {
            cardsLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var tableView: AutoHeightTableView! {
        didSet {
            BalanceTableViewCell.registerNib(for: tableView)
        }
    }
    
    @IBOutlet weak var addButtonView: AddButtonView! {
        didSet {
            addButtonView.button.addTarget(self, action: #selector(touchAddCardButton(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var sumField: LabeledFieldView!
    
    @IBOutlet weak var nextButton: NextButton!
    
    var tableHeightConstraint: NSLayoutConstraint?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.bringSubviewToFront(payMethodDropDown)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        tableView.maxHeight =
    }
    
//    override func updateViewConstraints() {
//        let contentHeight = tableView.contentSize.height
//        if tableHeightConstraint == nil {
//            tableHeightConstraint = tableView.autoSetDimension(.height, toSize: contentHeight)
//        }
//        else {
//            tableHeightConstraint?.constant = contentHeight
//        }
//        super.updateViewConstraints()
//        tableView.layoutIfNeeded()
//    }

    
    //MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BalanceTableViewCell.dequeue(tableView: tableView)
        if selectedItemsIndexes.contains(indexPath.row) {
            cell.selectImageView.image = UIImage(named: "check-green")
        } else {
            cell.selectImageView.image = UIImage(named: "check-gray")
        }
        cell.cardImageView.image = UIImage(named: "visa-card-icon")
        cell.cardNumberLabel.text = "12345678******90"
        return cell
    }
    
    
    @objc func touchAddCardButton(_ sender: UIButton) {
        items.append(NSObject())
        tableView.reloadData()
    }
    

    @IBAction func touchNextButton(_ sender: Any) {
        
    }
    
    
    
}
