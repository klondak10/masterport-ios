//
//  WalletTabViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/7/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class WalletTabViewController: TabViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let myCardsController = MyCardsViewController.instanceFromStoryboard
        let historyController = HistoryViewController.instanceFromStoryboard
        let partnersController = PartnersViewController.instanceFromStoryboard
        setControllers([myCardsController, historyController, partnersController])
        title = "Платежные карты"
        navCon?.navigationBar.isHidden = false
        navCon?.viewControllers = [self]
    }
}
