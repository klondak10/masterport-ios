//
//  HistoryViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, TabControllerSelectable {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            HistoryTableViewCell.registerNib(for: tableView)
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func selectedBy(tabController: TabViewController) {
        tabController.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "sort-icon-white"), target: self, action: #selector(touchSortButton(_:)))
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HistoryTableViewCell.dequeue(tableView: tableView)
        cell.translatesAutoresizingMaskIntoConstraints = false
        cell.iconView.image = UIImage(named: "card-deposit-icon-green")
        cell.operationTypeLabel.text = "Пополнение баланса"
        cell.cardNameLabel.text = "Название карты"
        cell.costLabel.text = "3000 руб"
        cell.dateLabel.text = "12.12.2012"
        cell.balanceLabel.text = "1000 руб"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    @objc func touchSortButton(_ sender: UIBarButtonItem)
    {
//        pushVC(SortingPurseController.instanceFromStoryboard)
    }
}
