//
//  OrderCostViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OrderCostViewController: BaseViewController {

    
    @IBOutlet weak var currentCostLabel: UILabel! {
        didSet {
            currentCostLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    
    @IBOutlet weak var newCostFieldView: LabeledFieldView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func touchNewCost(_ sender: Any) {
    }
    
}
