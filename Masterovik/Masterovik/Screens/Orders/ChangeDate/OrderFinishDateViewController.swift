//
//  OrderFinishDateViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OrderFinishDateViewController: BaseViewController {

    @IBOutlet weak var beginDateLabel: UILabel! {
        didSet {
            beginDateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var finishDateLabel: UILabel! {
        didSet {
            finishDateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var remainLabel: UILabel! {
        didSet {
            remainLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    
    @IBOutlet weak var timeRemainingLabel: UILabel! {
        didSet {
            timeRemainingLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var proposedDateLabel: UILabel! {
        didSet {
            proposedDateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var addDateView: AddButtonView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    
    @IBAction func touchProposeDateButton(_ sender: Any) {
    }
    


}
