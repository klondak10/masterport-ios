//
//  SortOrdersViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class SortOrdersViewController: BaseViewController {
    
    @IBOutlet weak var townSortDisclosureItem: ListDisclosureItem! {
        didSet {
            townSortDisclosureItem.imageView.image = UIImage(named: "pin-icon-green")
        }
    }
    
    @IBOutlet weak var serviceSortDisclosureItem: ListDisclosureItem! {
        didSet {
            serviceSortDisclosureItem.imageView.image = UIImage(named: "list-icon-green")
        }
    }
    
    @IBOutlet weak var costSortDisclosureItem: ListDisclosureItem! {
        didSet {
            costSortDisclosureItem.imageView.image = UIImage(named: "wallet-icon-green")
        }
    }
    
    @IBOutlet weak var findOrderDisclosureItem: ListDisclosureItem! {
        didSet {
            findOrderDisclosureItem.imageView.image = UIImage(named: "search-icon-green")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "close-icon-white"), target: self, action: #selector(touchCloseButton(_:)))
        // Do any additional setup after loading the view.
    }
    
    
    @objc func touchCloseButton(_ sender: UIBarButtonItem) {
        navCon?.popViewController(animated: true)
    }
    
    
    
    @IBAction func touchShowResultButton(_ sender: Any) {
    }
    
    
    
    

    
    
}
