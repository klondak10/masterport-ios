//
//  CurrentOrdersViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CurrentOrdersViewController: OrdersViewController, UITextViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Текущие"
        backgroundImageView.image = UIImage(named: "tools-icon-green")
        backgroundTextView.delegate = self
        backgroundTextView.linkTextAttributes = [.foregroundColor : UIColor.darkGreen]
        let advertisementsLink = NSAttributedString(string: "Объявления", attributes: [.link : NSURL(string: "someURL") as Any])
        let attributedText = NSMutableAttributedString(string: "У вас еще нет заказов. Для того, чтобы найти новые заказы перейдите в ")
        attributedText.append(advertisementsLink)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        attributedText.addAttributes([.paragraphStyle : paragraphStyle], range: NSMakeRange(0, attributedText.string.count))
        backgroundTextView.attributedText = attributedText
        // Do any additional setup after loading the view.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = OrderTableViewCell.dequeue(tableView: tableView)
        cell.timeRemainingLabel.text = String(indexPath.row) + " Дней"
        cell.customerReactionLabel.backgroundColor = .lime
        cell.customerReactionLabel.textColor = .darkGreen
        cell.customerReactionLabel.text = "Заказчик внес предложение *xy месяца abcd*"
        cell.customerReviewWidthConstraint.constant = 0
        cell.customerReviewView.isHidden = true
        return cell
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        return false
    }

}
