//
//  OrderTableViewCell.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OrderTableViewCell: BaseTableViewCell {

    @IBOutlet weak var orderNameLabel: UILabel! {
        didSet {
            orderNameLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var customerNameLabel: UILabel! {
        didSet {
            customerNameLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var remainLabel: UILabel! {
        didSet {
            remainLabel.font = FontHelper.font(type: .franklin, size: .small)
            remainLabel.text = "Осталось"
        }
    }
    
    @IBOutlet weak var timeRemainingLabel: UILabel! {
        didSet {
            timeRemainingLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var customerReactionLabel: UILabel! {
        didSet {
            customerReactionLabel.font = FontHelper.font(type: .roboto, size: .small)
        }
    }
    
    @IBOutlet weak var customerReviewView: UIView!
    
    
    @IBOutlet weak var customerReviewImageView: UIImageView! {
        didSet {
//            customerReviewImageView.image = UIImage(named: "text-icon-template")
        }
    }
    
    
    @IBOutlet weak var customerReactionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var customerReviewWidthConstraint: NSLayoutConstraint!
    
    
}
