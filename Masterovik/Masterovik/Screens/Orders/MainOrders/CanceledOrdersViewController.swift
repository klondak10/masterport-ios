//
//  CanceledOrdersViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CanceledOrdersViewController: OrdersViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Отмененные"
        backgroundImageView.image = UIImage(named: "tools-minus-icon-red")
        backgroundTextView.text = "У вас еще нет отмененных заказов"
        // Do any additional setup after loading the view.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = OrderTableViewCell.dequeue(tableView: tableView)
        cell.timeRemainingLabel.text = String(indexPath.row) + " Дней"
        cell.customerReactionLabel.backgroundColor = .warningRed
        cell.customerReactionLabel.textColor = .white
        cell.customerReactionLabel.text = "Отменено *х*"
        cell.customerReviewWidthConstraint.constant = 0
        cell.customerReviewView.isHidden = true
        return cell
    }

}
