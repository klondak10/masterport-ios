//
//  OrdersTabViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/8/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OrdersTabViewController: TabViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sortButton = UIBarButtonItem(image: UIImage(named: "sort-icon-white"), target: self, action: #selector(touchSortButton(_:)))
        
        let sortSettingsButton = UIBarButtonItem(image: UIImage(named: "settings-icon-black"), target: self, action: #selector(touchSortSettingsButton(_:)), leftInset: 10.0)
        
        navigationItem.rightBarButtonItems = [sortSettingsButton, sortButton]
        
        let currentOrdersController = CurrentOrdersViewController()
        let doneOrdersController = DoneOrdersViewController()
        let canceledOrdersController = CanceledOrdersViewController()
        setControllers([currentOrdersController, doneOrdersController, canceledOrdersController])
        title = "Заказы"
        navCon?.navigationBar.isHidden = false
    }
    
    @objc func touchSortButton(_ sender: UIBarButtonItem) {
        let currentController = controller(at: currentTab) as! OrdersViewController
        currentController.items = []
        currentController.tableView.reloadData()
    }
    
    @objc func touchSortSettingsButton(_ sender: UIBarButtonItem) {
        pushVC(SortOrdersViewController.instanceFromStoryboard)
    }
}
