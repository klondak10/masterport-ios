//
//  LeftMenuViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import SideMenuSwift

class LeftMenuViewController: UIViewController {
    
    @IBOutlet weak var helloLabel: UILabel! {
        didSet {
            helloLabel.font = FontHelper.font(type: .myriad, size: .medium)
            helloLabel.textColor = .white
        }
    }
    
    @IBOutlet weak var balanceLabel: UILabel! {
        didSet {
            balanceLabel.font = FontHelper.font(type: .myriad, size: .small)
            balanceLabel.textColor = .white
        }
    }
    
    @IBOutlet weak var ordersItem: MenuItemView! {
        didSet {
            ordersItem.imageView.image = UIImage(named: "tools-icon-green")
            ordersItem.button.addTarget(self, action: #selector(touchOrdersItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var advertisementsItem: MenuItemView! {
        didSet {
            advertisementsItem.imageView.image = UIImage(named: "papers-icon-green")
//            advertisementsItem.button.addTarget(self, action: #selector(touchAdvertisementsItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var requestsItem: MenuItemView! {
        didSet {
            requestsItem.imageView.image = UIImage(named: "tools-arrow-icon-green")
//            requestsItem.button.addTarget(self, action: #selector(touchRequestsItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var ratingItem: MenuItemView! {
        didSet {
            ratingItem.imageView.image = UIImage(named: "star-icon-green")
//            ratingItem.button.addTarget(self, action: #selector(touchRatingItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var walletItem: MenuItemView! {
        didSet {
            walletItem.imageView.image = UIImage(named: "wallet-icon-green")
            walletItem.button.addTarget(self, action: #selector(touchWalletItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var profileItem: MenuItemView! {
        didSet {
            profileItem.imageView.image = UIImage(named: "user-icon-green")
            profileItem.button.addTarget(self, action: #selector(touchProfileItem(_:)), for: .touchUpInside)
        }
    }
    
    
    @IBOutlet weak var orderServicesButton: UIButton! {
        didSet {
            orderServicesButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
        }
    }
    
    @IBOutlet weak var faqButton: UIButton! {
        didSet {
            faqButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
        }
    }
    
    
    @IBOutlet weak var exitButton: UIButton! {
        didSet {
            exitButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
        }
    }
    
    
    @IBOutlet weak var contentSideOffsetConstraint: NSLayoutConstraint!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let width = SideMenuController.preferences.basic.menuWidth
        contentSideOffsetConstraint.constant = UIScreen.main.bounds.width - width
//        helloLabel.font = FontHelper.font(type: .myriad, size: .medium)
//        helloLabel.textColor = .white
//        balanceLabel.font = FontHelper.font(type: .myriad, size: .small)
//        balanceLabel.textColor = .white
//        ordersItem.imageView.image = UIImage(named: "tools-icon-green")
//        advertisementsItem.imageView.image = UIImage(named: "papers-icon-green")
//        requestsItem.imageView.image = UIImage(named: "tools-arrow-icon-green")
//        ratingItem.imageView.image = UIImage(named: "star-icon-green")
//        walletItem.imageView.image = UIImage(named: "wallet-icon-green")
//        profileItem.imageView.image = UIImage(named: "user-icon-green")
//        orderServicesButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
//        faqButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
//        exitButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
//        ordersItem.button.addTarget(self, action: #selector(touchOrdersItem(_:)), for: .touchUpInside)
//        advertisementsItem.button.addTarget(self, action: #selector(touchAdvertisementsItem(_:)), for: .touchUpInside)
//        requestsItem.button.addTarget(self, action: #selector(touchRequestsItem(_:)), for: .touchUpInside)
//        ratingItem.button.addTarget(self, action: #selector(touchRatingItem(_:)), for: .touchUpInside)
//        walletItem.button.addTarget(self, action: #selector(touchWalletItem(_:)), for: .touchUpInside)
//        profileItem.button.addTarget(self, action: #selector(touchProfileItem(_:)), for: .touchUpInside)
    }
    
    // MARK: - @objc button actions
    
    @objc func touchOrdersItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(OrdersTabViewController())
    }
    
//    @objc func touchAdvertisementsItem(_ sender: UIButton) {
//
//            sideMenuController?.hideMenu()
//
//            let adsTabVC = AdsTabViewController(nibName: "AdsTabViewController", bundle: nil)
//            let navCon = sideMenuController?.contentViewController as! UINavigationController
//            navCon.pushViewController(adsTabVC, animated: true)
//    }
//
//    @objc func touchRequestsItem(_ sender: UIButton) {
//
//        sideMenuController?.hideMenu()
//
//        let requestsTabVC = RequestsTabViewController(nibName: "RequestsTabViewController", bundle: nil)
//        let navCon = sideMenuController?.contentViewController as! UINavigationController
//        navCon.pushViewController(requestsTabVC, animated: true)
//    }
    
//    @objc func touchRatingItem(_ sender: UIButton) {
//        sideMenuController?.hideMenu()
//        
//        let myRatingTabVC = MyRatingTabViewController(nibName: "MyRatingTabViewController", bundle: nil)
//        let navCon = sideMenuController?.contentViewController as! UINavigationController
//        navCon.pushViewController(myRatingTabVC, animated: true)
//    }
    
    @objc func touchWalletItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(WalletTabViewController())
    }
    
    @objc func touchProfileItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(MainSettingsViewController.instanceFromStoryboard)
    }
    
    // MARK: - IBActions
    
    @IBAction func touchOrderServicesButton(_ sender: Any) {
    }
    
    @IBAction func touchFAQButton(_ sender: Any) {
        sideMenuController?.hideMenu()
        pushVC(FAQTabViewController())
        
    }
    
    @IBAction func touchExitButton(_ sender: Any) {
        sideMenuController?.hideMenu()
        pushVC(SignInViewController.instanceFromStoryboard)
    }

}
