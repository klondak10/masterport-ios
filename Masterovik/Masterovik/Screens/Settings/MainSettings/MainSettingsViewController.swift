//
//  MainSettingsViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/23/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class MainSettingsViewController: BaseViewController {
    
    @IBOutlet weak var setupAccountItem: ListDisclosureItem!
    
    @IBOutlet weak var changePhoneItem: ListDisclosureItem!
    
    @IBOutlet weak var changeEmailItem: ListDisclosureItem!
    
    @IBOutlet weak var notificationsItem: ListDisclosureItem!
    
    @IBOutlet weak var servicesItem: ListDisclosureItem!

    @IBOutlet weak var deleteAccountButton: NextButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
//        if let navBar = navigationController?.navigationBar
//        {
//            navBar.isHidden = false
//            topGradientView.autoSetDimension(.height, toSize: navBar.frame.maxY)
//        }
        
        setMenuButton()
        
        setupAccountItem.imageView.image = UIImage(named: "user-icon-green")
        setupAccountItem.button.addTarget(self, action: #selector(touchSetupAccountButton(_:)), for: .touchUpInside)
        changePhoneItem.imageView.image = UIImage(named: "phone-icon-green")
        changePhoneItem.button.addTarget(self, action: #selector(touchChangePhoneButton(_:)), for: .touchUpInside)
        changeEmailItem.imageView.image = UIImage(named: "mail-icon-green")
        changeEmailItem.button.addTarget(self, action: #selector(touchChangeEmailButton(_:)), for: .touchUpInside)
        notificationsItem.imageView.image = UIImage(named: "bell-icon-green")
        notificationsItem.button.addTarget(self, action: #selector(touchNotificationsButton(_:)), for: .touchUpInside)
        servicesItem.imageView.image = UIImage(named: "list-icon-green")
        servicesItem.button.addTarget(self, action: #selector(touchServicesButton(_:)), for: .touchUpInside)
        deleteAccountButton.backgroundColor = .white
        deleteAccountButton.titleLabel?.textColor = .warningRed
        deleteAccountButton.setTitleColor(.warningRed, for: .normal)
        deleteAccountButton.layer.borderWidth = 1.0
        deleteAccountButton.layer.borderColor = UIColor.warningRed.cgColor
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - @objc button actions
    
    @objc func touchMenuButton(_ sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
    }
    
    @objc func touchSetupAccountButton(_ sender: UIButton) {
        pushVC(PersonalDataViewController.instanceFromStoryboard)
    }
    
    @objc func touchChangePhoneButton(_ sender: UIButton) {
        pushVC(ChangePhoneViewController.instanceFromStoryboard)
    }
    
    @objc func touchChangeEmailButton(_ sender: UIButton) {
        pushVC(ChangeEmailViewController.instanceFromStoryboard)
    }
    
    @objc func touchNotificationsButton(_ sender: UIButton) {
        pushVC(NotificationSettingsViewController.instanceFromStoryboard)
    }
    
    @objc func touchServicesButton(_ sender: UIButton) {
        pushVC(ServicesViewController.instanceFromStoryboard)
    }
    
    
    
    // MARK: - IBActions
    
    @IBAction func touchDeleteAccountButton(_ sender: Any) {
        pushVC(SignInViewController.instanceFromStoryboard)
    }
    

}
