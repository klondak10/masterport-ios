//
//  NotificationSettingsViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class NotificationSettingsViewController: BaseViewController {
    
    @IBOutlet weak var chosenServicesSwitchView: SwitchItemView!
    
    @IBOutlet weak var changesOrderSwitchView: SwitchItemView!
      
    @IBOutlet weak var newCommentSwitchView: SwitchItemView!
    
    @IBOutlet weak var infoSwitchView: SwitchItemView!
    
    @IBOutlet weak var denyNotificationLabel: UILabel!
    
    @IBOutlet weak var infoDescriptionLabel: UILabel!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        chosenServicesSwitchView.switchView.addTarget(self, action: #selector(toggleSwitch(_:)), for: .valueChanged)
        changesOrderSwitchView.switchView.addTarget(self, action: #selector(toggleSwitch(_:)), for: .valueChanged)
        newCommentSwitchView.switchView.addTarget(self, action: #selector(toggleSwitch(_:)), for: .valueChanged)
        infoSwitchView.switchView.addTarget(self, action: #selector(toggleSwitch(_:)), for: .valueChanged)
        
        denyNotificationLabel.font = FontHelper.font(type: .franklin, size: .small)
        infoDescriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
    }
    
    
    @objc func toggleSwitch(_ sender: UISwitch) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            if sender.isOn {
                sender.thumbTintColor = .darkGreen
            } else {
                sender.thumbTintColor = .lightGrayText
            }
        }
    }
    
    
    
    
    
    @IBAction func touchSaveButton(_ sender: Any) {
//        navigationController?.popViewController(animated: true)
        infoSwitchView.switchView.thumbTintColor = .darkGreen
    }
    
    
    
    
    
    
}
