//
//  PersonalDataViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class PersonalDataViewController: BaseViewController {
    
    @IBOutlet weak var nameField: LabeledFieldView!
    
    @IBOutlet weak var surnameField: LabeledFieldView!
    
    @IBOutlet weak var cityField: LabeledFieldView!
    
    @IBOutlet weak var companyField: LabeledFieldView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var nextButton: NextButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
        descriptionTextView.font = FontHelper.font(type: .roboto, size: .small)
        descriptionTextView.setRoundedRect()
        nextButton.backgroundColor = .darkGreen
    }
    
    @IBAction func touchNextButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
