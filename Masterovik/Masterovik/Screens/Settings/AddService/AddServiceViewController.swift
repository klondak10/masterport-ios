//
//  AddServiceViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class AddServiceViewController: BaseViewController {
    
    @IBOutlet weak var serviceNameField: LabeledFieldView!
    
    @IBOutlet weak var shortDescriptionField: LabeledFieldView!
    
    @IBOutlet weak var addResonsField: LabeledFieldView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func touchSendRequestButton(_ sender: Any) {
    }
    
    

}
