//
//  ChangeEmailViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ChangeEmailViewController: BaseViewController {
    
    @IBOutlet weak var emailField: LabeledFieldView!
    @IBOutlet weak var confirmEmailField: LabeledFieldView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func touchSaveButton(_ sender: Any) {
        if emailField.textField.text == confirmEmailField.textField.text {
            navigationController?.popViewController(animated: true)
        }
    }
}
