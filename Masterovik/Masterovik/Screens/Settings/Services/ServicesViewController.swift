//
//  ServicesViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ServicesViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var addServiceButtonView: AddButtonView! {
        didSet {
            addServiceButtonView.button.addTarget(self, action: #selector(touchAddServiceButton(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var addRequestButtonView: AddButtonView! {
        didSet {
            addRequestButtonView.button.addTarget(self, action: #selector(touchAddRequestButton(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var serviceNotFoundLabel: UILabel! {
        didSet {
            serviceNotFoundLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var addRequestLabel: UILabel! {
        didSet {
            addRequestLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        addRequestButtonView.button.addTarget(self, action: #selector(touchAddRequestButton(_:)), for: .touchUpInside)
        
//        serviceNotFoundLabel.font = FontHelper.font(type: .franklin, size: .medium)
//        addRequestLabel.font = FontHelper.font(type: .franklin, size: .medium)
        
    }
    
    //MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
    
    // MARK: - Actions
    
    @objc func touchAddServiceButton(_ sender: UIButton) {
    }
    
    @objc func touchAddRequestButton(_ sender: UIButton) {
        pushVC(AddServiceViewController.instanceFromStoryboard)
    }
    
    @IBAction func touchSaveButton(_ sender: Any) {
    }
}
