//
//  ChangePhoneViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ChangePhoneViewController: BaseViewController, RoundTimerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var phoneField: LabeledFieldView!
    
    @IBOutlet weak var smsCodeField: LabeledFieldView!
    
    @IBOutlet weak var getCodeButton: NextButton!
    
    @IBOutlet weak var roundTimerView: RoundTimerView!
    
    @IBOutlet weak var saveButton: NextButton!
    
    
    let sendCodeAgain = "Выслать код повторно"
    
    var canInputSMSCode = false {
        didSet {
            smsCodeField.isUserInteractionEnabled = canInputSMSCode
            
        }
    }
    
    var canSendSMSCode = false {
        didSet {
            getCodeButton.isUserInteractionEnabled = canSendSMSCode
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneField.textField.delegate = self
        smsCodeField.textField.delegate = self
        smsCodeField.isUserInteractionEnabled = false
        phoneField.textField.addTarget(self, action: #selector(checkValidNumber), for: .editingChanged)
        smsCodeField.textField.addTarget(self, action: #selector(removeErrorEmphasize), for: .editingChanged)
        getCodeButton.isUserInteractionEnabled = false
        getCodeButton.layer.borderColor = UIColor.darkGreen.cgColor
        getCodeButton.layer.borderWidth = 1.0
        roundTimerView.delegate = self
        roundTimerView.emptyColor = .red
        roundTimerView.timerLabel.textColor = .red
    }

    @IBAction func touchGetCodeButton(_ sender: Any) {
        canInputSMSCode = true
        canSendSMSCode = false
        getCodeButton.setTitle(sendCodeAgain, for: .normal)
        roundTimerView.elapsedTime = 0
        roundTimerView.startTimer()
        roundTimerView.isHidden = false
    }
    
    func plusMask() {
        guard let text = phoneField.textField.text else {
            return
        }
        if text.first != "+" && text.count > 2
        {
            phoneField.textField.text = "+" + text
        }
    }
    
    @objc func removeErrorEmphasize() {
        smsCodeField.textField.layer.borderWidth = 0.0
    }
    
    @objc func checkValidNumber() {
        plusMask()
        canSendSMSCode = true
    }
    
    func checkValidSMSCode() -> Bool {
        return true
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Timer delegate method
    
    func timerEnded() {
        roundTimerView.isHidden = true
        canSendSMSCode = true
    }
    
    @IBAction func touchSaveButton(_ sender: Any) {
    }
    
    
    
    
    
    
    
}
