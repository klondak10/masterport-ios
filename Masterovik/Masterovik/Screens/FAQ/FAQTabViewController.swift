//
//  FAQTabViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/8/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class FAQTabViewController: TabViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let feedbackController = FeedbackViewController.instanceFromStoryboard
        let faqController = FAQViewController.instanceFromStoryboard
        setControllers([feedbackController, faqController])
        title = "FAQ"
    }
}
