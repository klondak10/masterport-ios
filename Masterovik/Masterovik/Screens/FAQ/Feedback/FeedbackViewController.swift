//
//  FeedbackViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/8/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var topicField: LabeledFieldView!
    
    @IBOutlet weak var letterTextLabeledView: LabeledDescriptionView! {
        didSet {
            letterTextLabeledView.textView.delegate = self
            letterTextLabeledView.textView.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        }
    }
    
    @IBOutlet weak var sendMailButton: NextButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        letterTextLabeledView.maxHeight = sendMailButton.frame.minY - (letterTextLabeledView.frame.minY + letterTextLabeledView.textView.frame.minY) - 10
    }
    
    func textViewDidChange(_ textView: UITextView) {
        letterTextLabeledView.updateTextViewFrame()
    }
    
    
    @IBAction func touchSendMailButton(_ sender: Any) {
    }
}
