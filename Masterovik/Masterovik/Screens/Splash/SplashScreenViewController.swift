//
//  SplashScreenViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SplashScreenViewController: BaseViewController {
    
    @IBOutlet weak var spinnerView: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        view.bringSubviewToFront(spinnerView)
        spinnerView.startAnimating()
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(pushSignInScreen), userInfo: nil, repeats: false)
        topViewHeightConstraint?.isActive = false
        topGradientView.autoPinEdge(toSuperviewEdge: .bottom)
    }
    
    @objc func pushSignInScreen() {
        spinnerView.stopAnimating()
        if let token = TokenManager.getAccessToken() {
           navigationController?.pushViewController(OrdersTabViewController(), animated: true)
        } else {
            navigationController?.pushViewController(SignInViewController.instanceFromStoryboard, animated: true)
        }
    }
}
