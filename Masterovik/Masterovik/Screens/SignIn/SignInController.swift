//
//  SignInController.swift
//  Masterovik
//
//  Created by Roman Haiduk on 6/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct SignInController {
    private let provider = NetworkProvider()
    
    func checkValidSMSCode(phone: String?, codeText: String?) -> Bool {
        if codeText == nil || codeText != "" {
            
            provider.loginRequest(phone: phone, smsCode: codeText) {
               // <#code#>
            }
            if codeText != "0000" {             // ХАРДКОД, ПОСЛЕ СОЗДАНИЯ БЭКЕНД ФУНКЦИИ НАДО ЗАМЕНИТЬ
                return false
            } else {
                return true}
        } else {
            return false
        }
    }
}
