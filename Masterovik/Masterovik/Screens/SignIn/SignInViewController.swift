//
//  SignInViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController, UITextFieldDelegate, RoundTimerDelegate {
    
    private let controller = SignInController()
    
    private static var phone: String!
    
    let maxLength = 12
    
    @IBOutlet weak var shadowView: ShadowView!
    
    @IBOutlet weak var phoneLabeledField: LabeledFieldView! {
        didSet {
            phoneLabeledField.textField.delegate = self
            phoneLabeledField.textField.addTarget(self, action: #selector(checkValidNumber), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var smsCodeLabeledField: LabeledFieldView! {
        didSet {
            smsCodeLabeledField.textField.delegate = self
            smsCodeLabeledField.isUserInteractionEnabled = false
            smsCodeLabeledField.textField.addTarget(self, action: #selector(removeErrorEmphasize), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var getCodeButton: UIButton! {
        didSet {
            getCodeButton.isUserInteractionEnabled = false
            getCodeButton.layer.borderColor = UIColor.darkGreen.cgColor
            getCodeButton.layer.borderWidth = 1.0
        }
    }
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var timerView: RoundTimerView! {
        didSet {
            timerView.delegate = self
        }
    }
    
    let sendCodeAgain = "Выслать код повторно"
    
    var canInputSMSCode = false {
        didSet {
            smsCodeLabeledField.isUserInteractionEnabled = canInputSMSCode
        }
    }
    
    var canSendSMSCode = false {
        didSet {
            getCodeButton.isUserInteractionEnabled = canSendSMSCode
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationItem.hidesBackButton = true
        navigationController?.setViewControllers([self], animated: false)
        topViewHeightConstraint?.isActive = false
        topGradientView.autoPinEdge(toSuperviewEdge: .bottom)
//        phoneLabeledField.textField.delegate = self
//        smsCodeLabeledField.textField.delegate = self
//        smsCodeLabeledField.isUserInteractionEnabled = false
//        phoneLabeledField.textField.addTarget(self, action: #selector(checkValidNumber), for: .editingChanged)
//        smsCodeLabeledField.textField.addTarget(self, action: #selector(removeErrorEmphasize), for: .editingChanged)
//        getCodeButton.isUserInteractionEnabled = false
//        getCodeButton.layer.borderColor = UIColor.darkGreen.cgColor
//        getCodeButton.layer.borderWidth = 1.0
//        timerView.delegate = self
//        smsCodeLabeledField.textField.isEnabled = false
        canSendSMSCode = true
        shadowView.shadowCorners = 15.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        shadowView.shadowCorners = 15.0
      //  self.phoneLabeledField.addNumberPlaceholder()
    }
    
    func plusMask() {
        guard let text = phoneLabeledField.textField.text else { return }
        if text.first != "+" && text.count > 2 {
            phoneLabeledField.textField.text = "+" + text
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count
        return newLength <= maxLength
    }
    
    @objc func removeErrorEmphasize() {
        smsCodeLabeledField.textField.layer.borderWidth = 0.0
    }
    
    @objc func checkValidNumber() {
        plusMask()
        canSendSMSCode = true
    }
    
    func checkValidSMSCode() -> Bool {
        if smsCodeLabeledField.textField.text == nil || smsCodeLabeledField.textField.text == "" {
            return true
        } else {
            return true
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Timer delegate method
    
    func timerEnded() {
        timerView.isHidden = true
        canSendSMSCode = true
    }
    
    private func changePhoneNumber(number: String) {
        SignInViewController.phone = number
    }
    
    static func getPhoneNumber() -> String {
        return SignInViewController.phone
    }
    
    // MARK: - IBActions
    
    @IBAction func touchGetCodeButton(_ sender: Any) {
        canInputSMSCode = true
        canSendSMSCode = false
        getCodeButton.setTitle(sendCodeAgain, for: .normal)
        timerView.elapsedTime = 0
        timerView.startTimer()
        timerView.isHidden = false
        smsCodeLabeledField.textField.isEnabled = true
    }
    
    @IBAction func touchNextButton(_ sender: Any) {
        let phone = phoneLabeledField.textField.text
        let code = smsCodeLabeledField.textField.text
        if controller.checkValidSMSCode(phone: phone, codeText: code) {
            pushVC(ChooseRoleViewController.instanceFromStoryboard)
            changePhoneNumber(number: phone!)
        } else {
            let alert = UIAlertController(title: "Неправильный код!", message: "Вы ввели неправильный код.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            smsCodeLabeledField.textField.validationIsFalse()
            smsCodeLabeledField.textField.text = ""
        }
//        self.sideMenuController?.revealMenu()
    }
}
