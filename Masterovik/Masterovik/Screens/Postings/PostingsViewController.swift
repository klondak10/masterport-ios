//
//  PostingsViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 25.06.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class PostingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var items: [Any] = [NSObject(), NSObject(), NSObject(), NSObject(), NSObject(), NSObject(), NSObject(), NSObject()]
    
    let tableView = UITableView()
    
    let backgroundImageView = UIImageView()
    let backgroundTextView = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        PostingTableViewCell.registerNib(for: tableView)
        
        let backgroundView = UIView()
        
        tableView.backgroundView = backgroundView
        backgroundView.addSubview(backgroundImageView)
        backgroundView.addSubview(backgroundTextView)
        backgroundImageView.contentMode = .scaleAspectFit
        backgroundTextView.textAlignment = .center
        backgroundTextView.textColor = .lightGrayText
        backgroundTextView.isUserInteractionEnabled = false
        
        tableView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0))
        backgroundImageView.autoAlignAxis(toSuperviewAxis: .vertical)
        backgroundTextView.autoPinEdge(.top, to: .bottom, of: backgroundImageView, withOffset: 20.0)
        backgroundTextView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = items.count
        if count == 0 {
            tableView.backgroundView?.isHidden = false
        } else {
            tableView.backgroundView?.isHidden = true
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = OrderTableViewCell.dequeue(tableView: tableView)
        return cell
    }
}
