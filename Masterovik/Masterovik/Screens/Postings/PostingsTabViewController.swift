//
//  PostingsTabViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 25.06.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class PostingsTabViewController: TabViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sortButton = UIBarButtonItem(image: UIImage(named: "sort-icon-white"), target: self, action: #selector(touchSortButton(_:)))
        
        let sortSettingsButton = UIBarButtonItem(image: UIImage(named: "settings-icon-black"), target: self, action: #selector(touchSortSettingsButton(_:)))
        
        navigationItem.rightBarButtonItems = [sortSettingsButton, sortButton]
        
    }
    
    @objc func touchSortButton(_ sender: UIBarButtonItem) {
        let currentController = controller(at: currentTab) as! PostingsViewController
        currentController.items = []
        currentController.tableView.reloadData()
    }
    
    @objc func touchSortSettingsButton(_ sender: UIBarButtonItem) {
        pushVC(SortOrdersViewController.instanceFromStoryboard)
    }
}
