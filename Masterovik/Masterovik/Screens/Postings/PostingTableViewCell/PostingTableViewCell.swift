//
//  PostingTableViewCell.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 25.06.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class PostingTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var postingNameLabel: UILabel! {
        didSet {
            postingNameLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var priceLabel: UILabel! {
        didSet {
            priceLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
}
