//
//  ChooseRoleViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

enum Role {
    case none
    case contractor
    case customer
}

class ChooseRoleViewController: BaseViewController {
    
    @IBOutlet weak var chooseLabel: UILabel! {
        didSet {
            chooseLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var selectContractorView: CodeExpandingDescriptionView! {
        didSet {
            selectContractorView.button.titleLabel?.textColor = .darkGreen
            selectContractorView.button.addTarget(self, action: #selector(touchSelectContractorButton(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var selectCustomerView: CodeExpandingDescriptionView! {
        didSet {
            selectCustomerView.button.titleLabel?.textColor = .darkGreen
            selectCustomerView.button.addTarget(self, action: #selector(touchSelectCustomerButton(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var nextButton: UIButton!
    
    var role: Role = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        navigationItem.hidesBackButton = true
//        chooseLabel.font = FontHelper.font(type: .franklin, size: .medium)
//        selectCustomerView.button.titleLabel?.textColor = .darkGreen
//        selectContractorView.button.titleLabel?.textColor = .darkGreen
//        selectContractorView.button.addTarget(self, action: #selector(touchSelectContractorButton(_:)), for: .touchUpInside)
//        selectCustomerView.button.addTarget(self, action: #selector(touchSelectCustomerButton(_:)), for: .touchUpInside)
//        nextButton.backgroundColor = .darkGreen
    }
    
    //MARK: - @objc button actions
    
    @objc func touchSelectContractorButton(_ sender: UIButton) {
        role = .contractor
        selectContractorView.toggle(expanded: true)
        selectCustomerView.toggle(expanded: false)
    }
    
    @objc func touchSelectCustomerButton(_ sender: UIButton) {
        role = .customer
        selectContractorView.toggle(expanded: false)
        selectCustomerView.toggle(expanded: true)
    }
    
    // MARK: - IBActions
    
    @IBAction func touchNextButton(_ sender: Any) {
        if role == .contractor {
            pushVC(RegContractorViewController.instanceFromStoryboard)
        } else if role == .customer {
            pushVC(RegCustomerViewController.instanceFromStoryboard)
        }
    }
}
