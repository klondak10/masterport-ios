//
//  RegCustomerViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import WebKit

class RegCustomerViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet var labeledFields: [UIView]! {
        didSet {
            labeledFields.forEach {
                if let labeled = $0 as? LabeledFieldView {
                    labeled.getTextField().delegate = self
                }
            }
        }
    }
    
    var acceptedLicense: Bool = false
    
    @IBOutlet weak var firstName: LabeledFieldView!
    @IBOutlet weak var lastName: LabeledFieldView!
    @IBOutlet weak var emailField: LabeledFieldView!
    @IBOutlet weak var cityField: DropDownFieldView!
    @IBOutlet weak var checkmarkButton: UIButton!
    @IBOutlet weak var licenseButton: UIButton!
    @IBOutlet weak var licenseView: UIView!
    @IBOutlet weak var nextButton: NextButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        nameField.textField.delegate = self
//        surnameField.textField.delegate = self
//        emailField.textField.delegate = self
//        cityField.textField.delegate = self
        var attributes: [NSAttributedString.Key : Any] = [:]
        attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
        attributes[.font] = FontHelper.font(type: .franklin, size: .small)
        let underlinedString = NSAttributedString(string: licenseButton.titleLabel?.text ?? "", attributes: attributes)
        licenseButton.setAttributedTitle(underlinedString, for: .normal)
        navigationItem.hidesBackButton = true
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - IBActions
    
    @IBAction func touchCheckmarkButton(_ sender: Any) {
        acceptedLicense = !acceptedLicense
        if acceptedLicense {
            let checkmarkImage = UIImage(named: "ok-icon-black-vector")
            checkmarkButton.contentMode = .scaleAspectFit
            checkmarkButton.tintColor = .black
            checkmarkButton.setImage(checkmarkImage, for: .normal)
            licenseView.backgroundColor = .white
        } else {
            checkmarkButton.setImage(nil, for: .normal)
            checkmarkButton.backgroundColor = .clear
        }
    }
    
    @IBAction func touchLicenseButton(_ sender: Any) {
        let webView = WKWebView()
    }
    
    @IBAction func touchNextButton(_ sender: Any) {
        let incorrectTextField = (labeledFields as? [Chekable])?.filter({$0.checkIfEmpty()})
        guard acceptedLicense else {
            licenseView.backgroundColor = UIColor.red.withAlphaComponent(0.5)
            licenseView.cornerRadius = 5.0
            return
        }
        
        if incorrectTextField?.count == 0 {
            if let fName = firstName.textField.text,
                let lName = lastName.textField.text,
                let idCity = cityField.getTextField().text,
                let email = emailField.textField.text
            {

                let phone = SignInViewController.getPhoneNumber()

                let customer = Executor(id: 0, cityId: 1, firstName: fName, lastName: lName, phone: phone, balance: nil, roleId: 2, staticticsId: nil, email: email, invite: nil, invitedBy: nil, companyName: nil, companyDescription: nil)

                NetworkProvider().createUser(user: customer) { (error) in
                    if let error = error  {
                        let alert = UIAlertController(title: "Регистрация невозможна", message: "Повторите попытку позже", preferredStyle: .alert)
                        let actionCancel = UIAlertAction(title: "Отмена", style: .cancel)
                        alert.addAction(actionCancel)
                    } else {
                        self.pushVC(ContractorAgreementViewController.instanceFromStoryboard)
                    }
                }
            }
        }
    }
}
