//
//  RegContractorViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import WebKit
import IQKeyboardManagerSwift

class RegContractorViewController: BaseViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet var labeledFields: [UIView]! {
        didSet {
            labeledFields.forEach {
                if let labeled = $0 as? LabeledFieldView {
                labeled.getTextField().delegate = self }
            }
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstName: LabeledFieldView!
    @IBOutlet weak var lastName: LabeledFieldView!
    @IBOutlet weak var emailField: LabeledFieldView!
    @IBOutlet weak var cityField: DropDownFieldView!
    @IBOutlet weak var companyField: LabeledFieldView!
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var descriptionTextView: UITextView! {
        didSet {
            descriptionTextView.delegate = self
            descriptionTextView.setRoundedRect()
            descriptionTextView.font = FontHelper.font(type: .roboto, size: .small)
        }
    }
    
    @IBOutlet weak var checkInviteCodeButton: UIButton!
    @IBOutlet weak var checkResultImageView: UIImageView!
    @IBOutlet weak var checkResultLabel: UILabel!
    @IBOutlet weak var licenseContentView: UIView!
    @IBOutlet weak var licenseCheckmarkButton: UIButton!
    @IBOutlet weak var licenseTextButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var checkResultHeightConstraint: NSLayoutConstraint!
    
    var correctInviteCode: Bool = false
    var acceptedLicense: Bool = false
    
    let maxLength = 25
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
//        nameField.textField.delegate = self
//        surnameField.textField.delegate = self
//        emailField.textField.delegate = self
//        cityField.textField.delegate = self
//        companyField.textField.delegate = self
//        descriptionTextView.delegate = self
//        descriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
//        descriptionTextView.setRoundedRect()
//        descriptionTextView.layer.borderWidth = 0.5
//        descriptionTextView.layer.borderColor = UIColor.textFieldBorderColor.cgColor
//        descriptionTextView.font = FontHelper.font(type: .roboto, size: .small)
        var attributes: [NSAttributedString.Key : Any] = [:]
        attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
        attributes[.font] = FontHelper.font(type: .franklin, size: .small)
        let underlinedCheckInvite = NSAttributedString(string: checkInviteCodeButton.titleLabel?.text ?? "", attributes: attributes)
        checkInviteCodeButton.setAttributedTitle(underlinedCheckInvite, for: .normal)
        if checkResultHeightConstraint != nil {
            checkResultHeightConstraint.constant = 0
        }
        checkResultLabel.font = FontHelper.font(type: .roboto, size: .small)
        let underlinedLicense = NSAttributedString(string: licenseTextButton.titleLabel?.text ?? "", attributes: attributes)
        licenseTextButton.setAttributedTitle(underlinedLicense, for: .normal)
    }
    
    func checkInviteCode(code: String) {
        correctInviteCode = true
        if correctInviteCode {
            checkResultImageView.tintColor = .darkGreen
            checkResultLabel.text = "Код введен верно"
            checkResultLabel.textColor = .darkGreen
        } else {
            checkResultImageView.tintColor = .red
        }
        if checkResultHeightConstraint != nil {
            checkResultHeightConstraint.isActive = false
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newLength = text.count - (range.length - 1) + string.count
        return newLength <= maxLength
    }
    
    // MARK: - UITextViewDelegate
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        IQKeyboardManager.shared.enableAutoToolbar = true
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    // MARK: - IBActions
    
    @IBAction func touchCheckInviteButton(_ sender: Any) {
        let alert = UIAlertController(title: "Код приглашения пользователя", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: nil)
        let textField = alert.textFields?.first
        let checkCodeAction = UIAlertAction(title: "Продолжить", style: .default, handler: { (action) in
            if let text = textField?.text {
                self.checkInviteCode(code: text)
            }
        })
        alert.addAction(checkCodeAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func touchLicenseCheckmarkButton(_ sender: Any) {
        acceptedLicense = !acceptedLicense
        if acceptedLicense {
            let checkmarkImage = UIImage(named: "ok-icon-black-vector")
            licenseCheckmarkButton.contentMode = .scaleAspectFit
            licenseCheckmarkButton.tintColor = .black
            licenseCheckmarkButton.setImage(checkmarkImage, for: .normal)
            licenseContentView.backgroundColor = .white
        } else {
            licenseCheckmarkButton.setImage(nil, for: .normal)
            licenseCheckmarkButton.backgroundColor = .clear
        }
    }
    
    @IBAction func touchLicenseTextButton(_ sender: Any) {
        let webView = WKWebView()
    }
    
    @IBAction func touchNextButton(_ sender: Any) {
        let incorrectTextField = (labeledFields as? [Chekable])?.filter({$0.checkIfEmpty()})
        
        guard acceptedLicense else {
            licenseContentView.backgroundColor = UIColor.red.withAlphaComponent(0.5)
            licenseContentView.cornerRadius = 5.0
            scrollView.setContentOffset(CGPoint(x: 0.0, y: scrollView.contentSize.height - scrollView.bounds.height), animated: true)
            return
        }
        
        if incorrectTextField?.count == 0 {
            
            if let fName = firstName.textField.text,
                let lName = lastName.textField.text,
                let idCity = cityField.getTextField().text {
                
                let phone = SignInViewController.getPhoneNumber()
                
                let customer = Executor(id: 0, cityId: 1, firstName: fName, lastName: lName, phone: phone, balance: nil, roleId: 2, staticticsId: nil, email: nil, invite: nil, invitedBy: nil, companyName: nil, companyDescription: nil)

                NetworkProvider().createUser(user: customer) { (error) in
                    if let error = error {
                        let alert = UIAlertController(title: "Ругистрация невозможна", message: "Повторите попытку позже", preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
                        alert.addAction(cancelAction)
                    } else {
                        self.pushVC(ContractorAgreementViewController.instanceFromStoryboard)
                    }
                }
            }
        }
    }
}


