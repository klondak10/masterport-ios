//
//  TabView.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class TabView: UIView {
    
    private var buttons: Array<UIButton> = []
    
    let containerView = UIView()
    
    let underlineView = UIView()
    
    var underlineLeftConstraint: NSLayoutConstraint? = nil
    var underlineRightConstraint: NSLayoutConstraint? = nil
    
    weak var tabController: TabViewController?
    
    var currentTabIndex: Int = -1
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    convenience init(viewCount: Int) {
        self.init(frame: .zero)
        setViewCount(viewCount)
    }
    
    func commonInit() {
        backgroundColor = .white
        cornerRadius = 5.0
        addSubview(containerView)
        addSubview(underlineView)
        containerView.backgroundColor = .clear
        underlineView.backgroundColor = .lightGreen
        containerView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .bottom)
        underlineView.autoSetDimension(.height, toSize: 2.0)
        underlineView.autoPinEdge(toSuperviewEdge: .bottom)
        underlineView.autoPinEdge(.top, to: .bottom, of: containerView)
    }
    
    func setViewCount(_ viewCount: Int) {
        buttons = []
        let count = viewCount
        for index in 0..<count {
            let button = UIButton()
            buttons.append(button)
            containerView.addSubview(button)
            button.tag = index
            button.titleLabel?.font = FontHelper.font(type: .franklin, size: .small)
            button.titleLabel?.textAlignment = .center
            button.setTitleColor(.darkGrayText, for: .normal)
            button.addTarget(self, action: #selector(touchTab(_:)), for: .touchUpInside)
            button.autoPinEdge(toSuperviewEdge: .top)
            button.autoPinEdge(toSuperviewEdge: .bottom)

            self.autoMatch(.width, to: .width, of: button, withMultiplier: CGFloat(integerLiteral: count), relation: .greaterThanOrEqual)
            button.autoSetDimension(.width, toSize: 80.0, relation: .greaterThanOrEqual)
            if index == 0 {
                button.autoPinEdge(toSuperviewEdge: .left)
            } else {
                button.autoPinEdge(.left, to: .right, of: buttons[index - 1])
            }
            if index == count - 1 {
                button.autoPinEdge(toSuperviewEdge: .right)
            }
        }
        touchTab(buttons[0])
    }
    
    func setTitle(_ title : String, forTabAt index: Int) {
        buttons[index].setTitle(title, for: .normal)
    }
    
    func selectTabAt(index: Int) {
        underlineLeftConstraint?.isActive = false
        underlineRightConstraint?.isActive = false
        let selectedButton = buttons[index]
        underlineLeftConstraint = underlineView.autoPinEdge(.left, to: .left, of: selectedButton, withOffset: 5.0)
        underlineRightConstraint = underlineView.autoPinEdge(.right, to: .right, of: selectedButton, withOffset: -5.0)
        buttons = buttons.map { (button: UIButton) in
            button.setTitleColor(.darkGrayText, for: .normal)
            button.titleLabel?.textColor = .darkGrayText
            return button
        }
        selectedButton.setTitleColor(.darkGreen, for: .normal)
        currentTabIndex = index
        tabController?.selectTabAt(index: currentTabIndex)
        self.setNeedsLayout()
    }
    
    @objc func touchTab(_ sender: UIButton) {
        guard sender.tag != currentTabIndex else {
            return
        }
        selectTabAt(index: sender.tag)
    }

}
