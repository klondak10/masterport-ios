//
//  BaseViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    let topGradientView = BackgroundGradientView()
    
    var topViewHeightConstraint: NSLayoutConstraint?
    
    @IBInspectable var topViewHeight: CGFloat {
        set {
            topViewHeightConstraint?.isActive = false
            topViewHeightConstraint = topGradientView.autoSetDimension(.height, toSize: newValue)
        }
        get {
            if topViewHeightConstraint != nil {
                return topViewHeightConstraint!.constant
            } else {
                return 0.0
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(topGradientView)
        view.sendSubviewToBack(topGradientView)
        topGradientView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .bottom)
        if let navBar = navigationController?.navigationBar {
            topViewHeightConstraint = topGradientView.autoSetDimension(.height, toSize: navBar.frame.maxY)
        }
    }
    
    func setMenuButton() {
        let barButton = UIBarButtonItem(image: UIImage(named: "menu-icon-white"), target: self, action: #selector(showMenu))
        navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func showMenu() {
        sideMenuController?.revealMenu(animated: true, completion: nil)
    }
}
