//
//  UserService.swift
//  Masterovik
//
//  Created by Roman Haiduk on 6/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya

enum MasterovikApi {
    //GET
    case users(id: Int)
    //POST
    case login(pnone: String?, code: String?)
    case refreshToken(token: String)
    case createUser(user: Executor)
}

/// MOYA enum implementation
extension MasterovikApi: TargetType {
    
    var enviromentBaseURL : String {
        
        switch NetworkProvider.environment {
        case .test :
            return "http://masterovik.net/api/v1/"
        case .production:
            return "huiznaet.com"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: enviromentBaseURL)
            else { fatalError("URL doesn't exist. (BaseURL: \(enviromentBaseURL).")}
        return url
    }
        
    var path: String {
        switch self {
        case .users(let id):
            return "user/\(id)"
        case .refreshToken:
            return "user/refresh-token"
        case .login:
            return "user/login"
        case .createUser:
            return "user/create-user"
        }
    }
    
    var method: Method {
        switch self {
        case .createUser, .login, .refreshToken:    // POST requests
            return .post
            
        case .users:                                // GET requests
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
            
        case .createUser(let user):
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            return .requestCustomJSONEncodable(user, encoder: encoder)
            
        case .refreshToken(let token):
            return .requestParameters(parameters: ["refresh_token":token], encoding: JSONEncoding.default)
            
        case .login(let phone, let code):
            return .requestParameters(parameters: ["phone":phone ?? "", "code":code ?? ""], encoding: JSONEncoding.default)
            
        case .users:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        var header = [String : String]()
        header["Accept"] = "application/json"
        let accessToken = TokenManager.getAccessToken() ?? "noToken"
        header["Authorization"] = "Bearer " + accessToken
        return header
    }
}
