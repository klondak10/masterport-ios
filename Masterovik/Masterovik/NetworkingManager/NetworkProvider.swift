//
//  NetworkProvider.swift
//  Masterovik
//
//  Created by Roman Haiduk on 6/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import Moya

enum Environment {
    case test
    case production
}

struct NetworkProvider {
    static let environment = Environment.test
    
    fileprivate let provider = MoyaProvider<MasterovikApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
    
    
    
    //MARK: Получение данных пользователя
    //После получения данных мы конвертируем в объект Executor, поскольку кол-во его полей больше
    //затем проверяем role_id поле и в случае, если это Customer делаем конвертацию
    //TODO: Проверить соответствие role_id с бэкендом
    func getUserInfo(id: Int, onComplition: @escaping (Personable) -> ()) {
        
        provider.requestWithRefreshingToken(provider, .users(id: id)) { (result) in
            switch result {
            case let .failure(error):
                    print(error.localizedDescription)
            case let .success(response):

                let decoder = JSONDecoder()
                decoder.dataDecodingStrategy = .base64
                if let executor = try? decoder.decode(Executor.self, from: response.data){
                    
                    if let role = executor.roleId, role == 1 {
                        if let customer = try? decoder.decode(Customer.self, from: response.data) {
                            onComplition(customer)
                        } else { print("ОШИБКА приведения типа при парсинге.") }
                    }
                    onComplition(executor)
                } else {
                    print("ОШИБКА приведения типа при парсинге.")
                }
            }
        }
    }
    
    //TODO: Добавить реализацию, после того как будет готова логика бэкенда
    func loginRequest(phone: String?, smsCode: String?, onComplition: @escaping ( ()->()) ) {
        
        provider.requestWithRefreshingToken(provider, .login(pnone: phone, code: smsCode)) { (result) in
            
        }
    }
    
    func createUser(user : Executor, onComplition: @escaping (Error?) -> ()) {
        
        provider.request(.createUser(user: user)) { (result) in
            switch result {
            case let .failure(error):
                print(error.localizedDescription)
                onComplition(error)
            case let .success(response):
                
                if let json = try? JSONSerialization.jsonObject(with: response.data, options: .mutableContainers) as? [String:String] {
                    
                    guard let token = json!["access_token"] else {return}
                    TokenManager.changeAccess(token: token)
                    guard let rtoken = json!["refresh_token"] else {return}
                    TokenManager.changeRefreshToken(token: rtoken)
                    onComplition(nil)
                } else {
                    let error = NSError(domain: "Json parse exception", code: 0, userInfo: nil)
                    onComplition(error)
                }
            }
        }
    }
}

extension MoyaProvider {
    
    func requestWithRefreshingToken(_ provider: MoyaProvider<MasterovikApi>,
                                    _ target: MasterovikApi,
                                    _ completion: @escaping Completion) {
        provider.request(target) { (result) in
            
            if let response = result.value {
                
                if response.statusCode == 426 {
                    guard let token = TokenManager.getRefreshToken() else { print("Токен рефреша не существует!"); return}
                    provider.request(.refreshToken(token: token)) { tokenResult in
                        
                        switch tokenResult {
                        case let .failure(error):
                            print(error.errorDescription ?? "Ошибка при получении нового токена.")
                        case let .success(response):
                            if let json = try? JSONSerialization.jsonObject(with: response.data, options: .mutableContainers) as? [String:String] {
                                
                                guard let token = json!["access_token"] else {return}
                                TokenManager.changeAccess(token: token)
                            }}
                        provider.request(target, completion: completion)
                        return
                    }
                }
            }
            completion(result)
        }
    }
}
