//
//  TabViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/2/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

protocol TabControllerSelectable {
    func selectedBy(tabController: TabViewController)
}

class TabViewController: BaseViewController, UIScrollViewDelegate {
    
    let tabView = TabView()
    
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    private var controllers: Array<UIViewController> = []
    
    var currentControllerLeftConstraint: NSLayoutConstraint?
    var currentControllerRightConstraint: NSLayoutConstraint?
    
    var currentTab: Int = -1
    
    var tabCount: Int {
        get {
            return controllers.count
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let shadowView = ShadowView()
        view.addSubview(shadowView)
        shadowView.addSubview(tabView)
        tabView.tabController = self
        shadowView.autoAlignAxis(toSuperviewAxis: .vertical)
        shadowView.autoPinEdge(toSuperviewEdge: .left, withInset: 10.0)
        shadowView.autoPinEdge(toSuperviewSafeArea: .top)
        shadowView.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 800), for: .vertical)
        view.addSubview(scrollView)
        scrollView.bounces = false
        scrollView.delegate = self
        scrollView.addSubview(contentView)
        tabView.autoPinEdgesToSuperviewEdges()
        topViewHeightConstraint?.isActive = false
        topViewHeightConstraint = topGradientView.autoPinEdge(.bottom, to: .top, of: tabView, withOffset: 20.0)
        scrollView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        scrollView.autoPinEdge(.top, to: .bottom, of: shadowView)
        contentView.autoPinEdgesToSuperviewEdges()
        contentView.autoMatch(.height, to: .height, of: scrollView)
        
        setMenuButton()
        navCon?.setViewControllers([self], animated: false)
        navCon?.navigationBar.isHidden = false
    }
    
    func setControllers(_ controllers: Array<UIViewController>) {
        tabView.tabController = self
        self.controllers = controllers
        let controllerWidth = view.bounds.width
        contentView.autoSetDimension(.width, toSize: controllerWidth * CGFloat(controllers.count))
        for (index, controller) in controllers.enumerated() {
            self.addChild(controller)
//            self.addChildController(controller)
            if let view = controller.view {
                scrollView.addSubview(controller.view)
                view.autoPinEdge(toSuperviewEdge: .top)
                view.autoPinEdge(toSuperviewEdge: .bottom)
                view.autoSetDimension(.width, toSize: controllerWidth)
                if index == 0 {
                    view.autoPinEdge(toSuperviewEdge: .left)
                } else {
                    view.autoPinEdge(.left, to: .right, of: controllers[index - 1].view)
                }
                if index == controllers.count - 1 {
                    view.autoPinEdge(toSuperviewEdge: .right)
                }
//                currentControllerRightConstraint = controllers.first?.view.autoPinEdge(toSuperviewEdge: .right)
            }
            controller.didMove(toParent: self)
        }
        tabView.setViewCount(controllers.count)
        view.bringSubviewToFront(tabView)
        for (index, controller) in controllers.enumerated() {
            let title = controller.title ?? String(format: "Tab %d", index)
            tabView.setTitle(title, forTabAt: index)
        }
    }
    
    func controller(at index: Int) -> UIViewController {
        return controllers[index]
    }
    
    func selectTabAt(index: Int) {
//        view.bringSubviewToFront(tabView)
        let nextController = controllers[index]
        guard let nextView = nextController.view else { return }
        (nextController as? TabControllerSelectable)?.selectedBy(tabController: self)
        currentControllerLeftConstraint?.isActive = false
        currentControllerRightConstraint?.isActive = false
        scrollView.setContentOffset(nextView.frame.origin, animated: true)

//        currentControllerLeftConstraint = nextView.autoPinEdge(.left, to: .left, of: view)
//        currentControllerRightConstraint = nextView.autoPinEdge(.right, to: .right, of: view)
        currentTab = index
        if let shadowView = tabView.superview {
            view.bringSubviewToFront(shadowView)
        }
    }
    
//    func addChildController(_ child: UIViewController) {
//        self.addChild(child)
//        scr.addSubview(child.view)
//        child.didMove(toParent: self)
//    }
    
    func removeChildController(_ child: UIViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollView.layer.removeAllAnimations()
        stopScroll()
        findTab()
    }
    
    func stopScroll() {
        let x = scrollView.contentOffset.x
        let y = scrollView.contentOffset.y
        scrollView.setContentOffset(CGPoint(x: x + 1, y: y + 1), animated: true)
        scrollView.setContentOffset(CGPoint(x: x, y: y), animated: false)
    }
    
    func findTab() {
        let controllerViewWidth = view.bounds.width
        let index = Int(round(scrollView.contentOffset.x / controllerViewWidth))
        print(index)
        tabView.selectTabAt(index: index)
    }
}
