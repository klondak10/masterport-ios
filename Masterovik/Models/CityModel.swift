//
//  CityList.swift
//  Masterovik
//
//  Created by Roman Haiduk on 6/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import SwiftyJSON


struct CityModel: Jsonable, Codable {
    
    enum CodingKyes: String, CodingKey {
        case cityId = "id"
        case cityName = "city"
    }
    
    var cityId: Int
    var cityName: String
    
    init(from json: JSON) {
        self.cityId = json["id"].intValue
        self.cityName = json["city"].stringValue
    }
}
