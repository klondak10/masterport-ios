//
//  AdsModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/21/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import SwiftyJSON

struct AdsModel: Codable, Jsonable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case services = "announcement_title"
        case name
        case phone
        case email
        case dateStart = "create_at"
        case dateEnd = "date_end"
        case price
        case cityId = "city_id"
        case placeDescription = "place_of_work"
        case shortDescription = "description"
        case fullDescription = "description_full"
    }

    var id: Int
    var services: [ServiceModel]
    var name: String
    var phone: String
    var email: String?
    var dateStart: String
    var dateEnd: String
    var price: Double
    var cityId: Int
    var placeDescription: String?
    var shortDescription: String
    var fullDescription: String?

    init(from json: JSON) {
        id = json["id"].intValue
        services = json["announcement_title"].arrayValue
                        .map { ServiceModel(from: $0)}
        name = json["name"].stringValue
        phone = json["phone"].stringValue
        email = json["email"].string
        dateStart = json["create_at"].stringValue
        dateEnd = json["date_end"].stringValue
        price = json["price"].doubleValue
        placeDescription = json["place_of_work"].string
        shortDescription = json["description"].stringValue
        fullDescription = json["description_full"].string
        cityId = json["city_id"].intValue
    }
    
    var duration: Int  {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        formatter.locale = Locale(identifier: "ru_RU")
        
        guard let from = formatter.date(from: dateStart)else {return 0}
        
        guard let to = formatter.date(from: dateEnd) else { return 0}
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: from) else {
            return 0
        }
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: to) else {
            return 0
        }
        return end - start
    }
}


