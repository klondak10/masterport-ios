//
//  HistoryWalletModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import SwiftyJSON

struct HistoryWalletModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case userId = "user_id"
        case cardId = "card_id"
        case createdAt = "created_at"
        case sum
        case balance
        case operation
    }
    
    var id: Int
    var userId: Int
    var cardId: Int?
    var createdAt: String
    var sum: Double
    var balance: Double
    var operation: String
    
    init(from json: JSON) {
        id = json["id"].intValue
        userId = json["user_id"].intValue
        cardId = json["card_id"].int
        createdAt = json["created_at"].stringValue
        sum = json["sum"].doubleValue
        balance = json["balance"].doubleValue
        operation = json["operation"].stringValue
    }
}

