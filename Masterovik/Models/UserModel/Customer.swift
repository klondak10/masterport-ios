//
//  Customer.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 11.06.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct Customer : Personable {
    
    var id: Int
    
    var cityId: Int
    
    var firstName: String
    
    var lastName: String
    
    var phone: String
    
    var balance: Double?
    
    var roleId: Int?
    
    var staticticsId: Int?
    
    var email: String?
    
    var invite: String?
    
    var invitedBy: Int?
    
    var createdAt: Date?
}

extension Customer {
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: PersonableKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        cityId = try container.decode(Int.self, forKey: .cityId)
        firstName = try container.decode(String.self, forKey: .firstName)
        lastName = try container.decode(String.self, forKey: .lastName)
        phone = try container.decode(String.self, forKey: .phone)
        balance = try container.decode(Double?.self, forKey: .balance)
        roleId = try container.decode(Int?.self, forKey: .roleId)
        staticticsId = try container.decode(Int?.self, forKey: .staticticsId)
        email = try container.decode(String?.self, forKey: .email)
        invite = try container.decode(String?.self, forKey: .invite)
        invitedBy = try container.decode(Int?.self, forKey: .invitedBy)
        createdAt = try container.decode(Date?.self, forKey: .createdAt)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: PersonableKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(cityId, forKey: .cityId)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(phone, forKey: .phone)
        try container.encode(balance, forKey: .balance)
        try container.encode(roleId, forKey: .roleId)
        try container.encode(staticticsId, forKey: .staticticsId)
        try container.encode(email, forKey: .email)
        try container.encode(invite, forKey: .invite)
        try container.encode(invitedBy, forKey: .invitedBy)
        try container.encode(createdAt, forKey: .createdAt)
    }
}
