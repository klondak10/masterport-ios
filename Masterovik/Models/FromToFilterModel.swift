//
//  FromToFilterModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/21/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

struct FromToFilterModel {
    let from: Int?
    let to: Int?
}
