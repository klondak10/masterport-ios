//
//  CellConstantSize.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/21/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

struct CellConstantSize {
    static let leftOffset : CGFloat = 0
    static let rightOffset : CGFloat = 0
    static let topOffset : CGFloat = 15
    static let bottomOffset: CGFloat = 5
    static let spacing : CGFloat = 20
    static let width = (UIScreen.main.bounds.width - 30 -
        (CellConstantSize.spacing * 3))/3
    static let height = width*157/170
    
}
