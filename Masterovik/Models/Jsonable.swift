//
//  Jsonable.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/9/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import SwiftyJSON

protocol Jsonable
{
    init(from json: JSON)
}
