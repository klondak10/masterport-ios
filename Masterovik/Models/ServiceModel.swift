//
//  ServiceModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ServiceModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case logo
    }
    
    var id: Int
    var name: String
    var logo: String?
    
     init(from json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        logo = json["logo"].string
    }
    
    init(_ id: Int, _ name: String, _ logo: String?) {
        self.id = id
        self.name = name
        self.logo = logo
    }
}

extension ServiceModel: Hashable {
   
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
        hasher.combine(self.name)
        hasher.combine(self.logo)
    }
}
