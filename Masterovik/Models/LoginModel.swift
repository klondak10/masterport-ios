//
//  LoginModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/7/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct LoginModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case access_token
        case refresh_token
        case role_id
    }
    
    var access_token: String
    var refresh_token: String
    var role_id: Int
}
