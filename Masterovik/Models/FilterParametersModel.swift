//
//  FilterParametersModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct FilterParametersModel {
    var city: CityModel? = nil
    var services: [ServiceModel]? = nil
    var priceFrom: Int? = nil
    var priceTo: Int? = nil
    var durationFrom: Int? = nil
    var durationTo: Int? = nil
    var dateFrom: Double? = nil
    var dateTo: Double? = nil
    var fromNewToOld: Bool = true
    var fromHeighRate: Bool = true
    var rateEstimate: Int? = nil
}
