//
//  CreditCardModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/15/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CreditCardModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case card = "card"
    }
    
    var id: Int
    var card: String
    
    init(from json: JSON) {
        id = json["id"].intValue
        card = json["card"].stringValue
    }
}
