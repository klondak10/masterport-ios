//
//  UserCoreDataManager.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/10/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol UserDataUpdatingDelegate: class {
    
    func updateUsername(firstName: String, lastName: String)
    
    func updateBalance(on balance: Double)
}

class UserCoreDataManager {
    
    weak var delegate: UserDataUpdatingDelegate?
    
    public static var shared: UserCoreDataManager {
        return UserCoreDataManager()
    }
    
    private init() {
    }
    
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    //Mark: Get Data
    func getUser() -> User? {
        let request = NSFetchRequest<User>(entityName: "User")
        do {
            let searchResults = try context.fetch(request)
            
            for user in searchResults {
               return user
            }
        } catch {
            print("Error with request: \(error)")
        }
        return nil
    }
    
    //Mark: Save function
    func save(user of: Executor) {
        
        let user = User(context: context)
        
        user.firstName = of.firstName
        user.lastName = of.lastName
        user.phone = of.phone
        user.cityId = Int64(exactly: of.cityId)!
        user.balance = Double(exactly: of.balance ?? 0)!
        user.email = of.email
        user.roleId = Int64(exactly: of.roleId ?? 1)!
        user.id = Int64(exactly: of.id)!
        user.companyName = of.companyName
        user.companyDescription = of.companyDescription
        user.invite = of.invite
        user.inviteBy = Int64(exactly: of.invitedBy ?? 0)!
        
        self.delegate?.updateUsername(firstName: of.firstName, lastName: of.lastName)
        self.delegate?.updateBalance(on: of.balance!)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    //Mark: Update functions
    func updateUserNumber(on number: String) {
        
        let request = NSFetchRequest<User>(entityName: "User")
        do {
            let searchResults = try context.fetch(request)
        
            for user in searchResults {
                user.phone = number
            }
        } catch {
            print("Error with request: \(error)")
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    func updateUserRole(on role: Int) {
        
        let request = NSFetchRequest<User>(entityName: "User")
        do {
            let searchResults = try context.fetch(request)
            
            for user in searchResults {
                user.roleId = Int64(exactly: role)!
            }
        } catch {
            print("Error with request: \(error)")
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    func updateUserEmail(on email: String) {
        
        let request = NSFetchRequest<User>(entityName: "User")
        do {
            let searchResults = try context.fetch(request)
            
            for user in searchResults {
                user.email = email
            }
        } catch {
            print("Error with request: \(error)")
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    func  updateUserName(firstName: String, lastName: String) {
        let request = NSFetchRequest<User>(entityName: "User")
        do {
            let searchResults = try context.fetch(request)
            
            for user in searchResults {
                user.firstName = firstName
                user.lastName = lastName
            }
            self.delegate?.updateUsername(firstName: firstName, lastName: lastName)
        } catch {
            print("Error with request: \(error)")
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    func updateUserBalance(on balance: Double) {
        let request = NSFetchRequest<User>(entityName: "User")
        do {
            let searchResults = try context.fetch(request)
            
            for user in searchResults {
                user.balance = balance
            }
            self.delegate?.updateBalance(on: balance)
        } catch {
            print("Error with request: \(error)")
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    func updateUserCity(on cityId: Int) {
        let request = NSFetchRequest<User>(entityName: "User")
        do {
            let searchResults = try context.fetch(request)
            
            for user in searchResults {
                user.cityId = Int64(exactly: cityId)!
            }
        } catch {
            print("Error with request: \(error)")
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    func updateUserCompanyInfo(on name: String, description: String) {
        let request = NSFetchRequest<User>(entityName: "User")
        do {
            let searchResults = try context.fetch(request)
            
            for user in searchResults {
                user.companyName = name
                user.companyDescription = description
            }
        } catch {
            print("Error with request: \(error)")
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    //Mark: Delete function
    
    func deleteUser() {
        let request = NSFetchRequest<User>(entityName: "User")
        do {
            let searchResults = try context.fetch(request)
            for user in searchResults {
                // delete user
                context.delete(user)
            }
            self.delegate?.updateUsername(firstName: "", lastName: "")
            self.delegate?.updateBalance(on: 0)
        } catch {
            print("Error with request: \(error)")
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}
