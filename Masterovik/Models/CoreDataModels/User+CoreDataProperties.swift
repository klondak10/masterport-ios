//
//  User+CoreDataProperties.swift
//  
//
//  Created by Roman Haiduk on 8/10/19.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var id: Int64
    @NSManaged public var phone: String?
    @NSManaged public var balance: Double
    @NSManaged public var roleId: Int64
    @NSManaged public var statisticId: Int64
    @NSManaged public var email: String?
    @NSManaged public var invite: String?
    @NSManaged public var inviteBy: Int64
    @NSManaged public var companyName: String?
    @NSManaged public var companyDescription: String?
    @NSManaged public var createdAt: NSDate?
    @NSManaged public var cityId: Int32
}
