//
//  EditOrderTableViewCell.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class EditOrderTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    @IBOutlet weak var suggestionAndValueLabel: UILabel! {
        didSet {
            suggestionAndValueLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    func configure(date: String, isUserSuggestion: Bool, value: String) {
        self.dateLabel.text = date
        
        self.suggestionAndValueLabel.text = isUserSuggestion ? "Предложено вами: \(value)"
                                                            : "Предложено заказчиком: \(value)"
    }
}
