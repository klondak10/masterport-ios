//
//  ChangeOrderDetailsViewController.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChangeOrderDetailsViewController: BaseViewController {
    
    @IBOutlet weak var editNamingLabel: UILabel! { didSet {  editNamingLabel.font = FontHelper.font(type: .franklin, size: .medium) }}
    @IBOutlet weak var editValueLabel: UILabel!{ didSet{
        editValueLabel.font = FontHelper.font(type: .franklin, size: .medium)
        editValueLabel.adjustsFontSizeToFitWidth = true
        }}
    
    @IBOutlet weak var tableView: UITableView! { didSet{ EditOrderTableViewCell.registerNib(for: tableView)}}
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var valueTextField: UITextField!
    
    @IBOutlet weak var addButton: AddButtonView! {didSet{ addButton.label.font = FontHelper.font(type: .franklin, size: .medium) }}
    @IBOutlet weak var proposeButton: NextButton! {
        didSet {
            proposeButton.titleLabel?.font = FontHelper.font(type: .franklin, size: .medium)
            proposeButton.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    private let disposeBag = DisposeBag()
    
    private var editingHistory: [(date: String, value: String, userSug: Bool)] = [] {
        didSet { tableView.reloadData() } }
    
    private let cellHeight: CGFloat = 90
    
    var isTimingEditor = true
    
    var titleName: String {
        return isTimingEditor ? "Изменить сроки" : "Изменить сумму"
    }
    
    var valueNaming: String {
        return isTimingEditor ? "Сроки выполнения:" : "Актуальная сумма заказа:"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = titleName
        editNamingLabel.text = valueNaming
         navigationController?.navigationBar.isHidden = false
        valueTextField.addDoneOnKeyboardWithTarget(self, action: #selector(doneAction))
        self.tableView.delegate = self
        self.tableView.dataSource = self

        for i in 0..<10 {
            editingHistory.append((date: "\(10 + i) июля 2019", value: "\(4+i) дней", userSug: i.isMultiple(of: 2)))
        }
        configureRx()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let height = cellHeight * CGFloat(editingHistory.count)
        let maxTableHeight =  self.proposeButton.frame.minY - self.tableView.frame.minY - 65
        tableViewHeightConstraint.constant = height > maxTableHeight ? maxTableHeight : height
    }
    
    private func configureRx() {
        
        addButton.button.rx.tap
            .subscribe { [unowned self] (_) in
                UIView.transition(with: self.valueTextField, duration: 0.5, options: .transitionCurlDown, animations: {
                    self.addButton.isHidden = true
                    self.valueTextField.isHidden = false
                }, completion: nil)
            }.disposed(by: disposeBag)
        
        proposeButton.rx.tap
            .subscribe {[unowned self] (_) in
                if !self.checkOnDigitsIn(string: self.valueTextField.text!) && self.valueTextField.text != "" {
                    self.valueTextField.notValidValueIn(withPlaceholder: "Ввести можно только цифры")
                    return
                } else {
                    guard let value = Int(self.valueTextField.text!) else {
                        self.valueTextField.notValidValueIn(withPlaceholder: "Число невозможно преобразовать")
                        return
                    }
                    if self.isTimingEditor {
                        guard value > 0 && value < 183 else {
                            self.valueTextField.notValidValueIn(withPlaceholder: "Диапазон от 1 до 182")
                            return
                        }
                    } else {
                        guard value > 0 && value < 99999999 else {
                            self.valueTextField.notValidValueIn(withPlaceholder: "Диапазон от 1 до 99 999 999")
                            return
                        }
                    }
                    //TODO: вставить запрос на изменения
                    self.navCon?.popViewController(animated: true)
                }
            }.disposed(by: disposeBag)
    }
    
    private func checkOnDigitsIn(string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }

    
    @objc func doneAction() {
        self.view.endEditing(true)
    }
}


//MARK: UITableView....
extension ChangeOrderDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return editingHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = EditOrderTableViewCell.dequeue(tableView: tableView)
        let item = editingHistory[indexPath.row]
        cell.configure(date: item.date, isUserSuggestion: item.userSug, value: item.value)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

