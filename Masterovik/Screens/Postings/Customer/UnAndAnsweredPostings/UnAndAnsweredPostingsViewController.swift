//
//  UnAndAnsweredPostingsViewController.swift
//  Masterovik
//
//  Created by Roman Haiduk on 9/2/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UnAndAnsweredPostingsViewController: UIViewController, TabControllerSelectable {
    
    @IBOutlet weak var myCityLabel: UILabel! {
        didSet {
            myCityLabel.font = FontHelper.font(type: .franklin, size: .title)
        }
    }
    @IBOutlet weak var addPostingButton: AddButtonView!
    @IBOutlet weak var tableView: UITableView! {
        didSet { MyPostingsTableViewCell.registerNib(for: tableView) }
    }
    
    private let adsViewModel = AdsViewModel()
    private let cityViewModel = RegistrationViewModel()
    private let disposeBag = DisposeBag()
    
    private var postingItems: [AdsModel] = []
    
    var isAnsweredPostings = true
    
    private var userCityName: String? { didSet { if let city = userCityName {
        self.myCityLabel.text = "Мой Город: \(city)" } } }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        if isAnsweredPostings {
            title = "Отвеченные объявления"
        } else { title = "Неотвеченные объявления"}
        
    }
    
    func selectedBy(tabController: TabViewController) {
        
    }
}

extension UnAndAnsweredPostingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postingItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MyPostingsTableViewCell.dequeue(tableView: tableView)
        cell.configure(postingItems[indexPath.row])
        cell.selectionStyle = .none
        configureCell(cell: cell, index: indexPath.row)
        return cell
    }
    private func configureCell(cell: MyPostingsTableViewCell, index: Int) {
        cell.postingDurationLabel.isHidden = true
        
        if postingItems[index].id % 2 == 0 {
            cell.postingLastChangingLabel.text = "Исполнитель не вносил предложений"
        } else {
            cell.postingLastChangingLabel.text = "Исполнитель внес предложение: \(postingItems[index].dateStart)"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}
