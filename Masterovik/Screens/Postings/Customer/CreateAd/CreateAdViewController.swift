//
//  CreateAdViewController.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CreateAdViewController: BaseViewController {
    
    @IBOutlet weak var servicesDropDownView: DropDownFieldView!
    @IBOutlet weak var durationWorkingTF: LabeledFieldView!
    @IBOutlet weak var priceWorkingTF: LabeledFieldView!
    @IBOutlet weak var placeDescription: LabeledFieldView!
    @IBOutlet weak var cityDropDownView: DropDownFieldView!
    @IBOutlet weak var shortDescriptionTF: LabeledFieldView!
    @IBOutlet weak var fullDescriptionTF: LabeledFieldView!
    
    @IBOutlet weak var createAdButton: NextButton!
    
    private let cityViewModel = RegistrationViewModel()
    private let servicesViewModel = ServiceViewModel()
    private let adsViewModel = AdsViewModel()
    private let disposeBag = DisposeBag()
    
    private var allServices: [ServiceModel] = [] {
        didSet {
            self.servicesDropDownView.textField.optionArray = allServices.map { $0.name }
        }
    }
    private var choisenServices: [ServiceModel] = [] 
    
    private var allCityList: [CityModel] = [] {
        didSet {
            self.cityDropDownView.textField.optionArray = allCityList.map { $0.cityName }
        }
    }
    private var choisenCity: CityModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureRx()
        dropDownDidSelectComplitions()
    }
    
    private func configureUI() {
        title = "Создать объявление"
        UIView.showLoadIndicatiorOn(view: self.view)
        
        self.createAdButton.titleLabel?.adjustsFontSizeToFitWidth = true
        servicesDropDownView.textField.arrowSize = 20.0
        servicesDropDownView.textField.arrowColor = .darkGreen
        servicesDropDownView.textField.checkMarkEnabled = true
        servicesDropDownView.textField.isSearchEnable = false
        servicesDropDownView.textField.listHeight = 400
        servicesDropDownView.textField.selectedRowColor = .white
        servicesDropDownView.textField.isMultipleSelected = true
        servicesDropDownView.textField.maxSelectedCount = 3
        
        cityDropDownView.textField.arrowSize = 20.0
        cityDropDownView.textField.arrowColor = .darkGreen
        cityDropDownView.textField.checkMarkEnabled = true
        cityDropDownView.textField.isSearchEnable = false
        cityDropDownView.textField.listHeight = view.bounds.height - cityDropDownView.frame.maxY - 180
        cityDropDownView.textField.selectedRowColor = .white
        
        durationWorkingTF.validateType = .digit
        priceWorkingTF.validateType = .digit
        priceWorkingTF.textField.keyboardType = .numberPad
        durationWorkingTF.textField.keyboardType = .numberPad
        
        priceWorkingTF.textField.addDoneOnKeyboardWithTarget(self, action: #selector(doneAction))
        durationWorkingTF.textField.addDoneOnKeyboardWithTarget(self, action: #selector(doneAction))
        shortDescriptionTF.textField.addDoneOnKeyboardWithTarget(self, action: #selector(doneAction))
        fullDescriptionTF.textField.addDoneOnKeyboardWithTarget(self, action: #selector(doneAction))
        placeDescription.textField.addDoneOnKeyboardWithTarget(self, action: #selector(doneAction))
    }
    
    private func configureRx() {
        
        let citiesIsLoaded = BehaviorRelay<Bool>(value: false)
        let servicesIsLoaded = BehaviorRelay<Bool>(value: false)
        let allIsLoaded = Observable.combineLatest(citiesIsLoaded, servicesIsLoaded) { $0 && $1 }.share(replay: 1)
        
        servicesViewModel.getServiceList()
        
        cityViewModel.onCityList
            .subscribe(onNext: { [unowned self] (list) in
                self.allCityList = list
                citiesIsLoaded.accept(true)
            }).disposed(by: disposeBag)
        servicesViewModel.onServiceList
            .drive(onNext: { [unowned self] (list) in
                self.allServices = list
                servicesIsLoaded.accept(true)
            }).disposed(by: disposeBag)
        
        allIsLoaded.subscribe(onNext: { [unowned self] (next) in
            if next { UIView.removeLoadIndicatorFrom(view: self.view)}
        }).disposed(by: disposeBag)
        
        createAdButton.rx.tap
            .subscribe { [unowned self] (_) in
                if self.fieldsValidation() {
                    self.createAdd()
                }
        }.disposed(by: disposeBag)
        
        adsViewModel.onAdCreated
            .subscribe(onNext: { [unowned self] (succes) in
                if succes {
                    
                    self.showWireframe(yesHandler: {
                        self.reloadViewForNewInput()
                    }, noHandler: {
                        self.pushVC(MyPostingsViewController.instanceFromStoryboard)
                    }, title: "Объявление успешно создано", description: "Хотите создать еще одно?", image: #imageLiteral(resourceName: "checked"), okButtonHandler: nil)
                } else {
                    
                    self.showWireframe(yesHandler: nil, noHandler: nil, title: "Не получилось создать объявление", description: "Попробуйте еще раз", image: #imageLiteral(resourceName: "errorAd"), okButtonHandler: nil)
                }
            }).disposed(by: disposeBag)
    }
    
    private func dropDownDidSelectComplitions() {
        servicesDropDownView.textField.didSelect { [unowned self] (text, index, _) in
          
            if self.choisenServices.count >= 3 {
                self.showWireframe(yesHandler: nil, noHandler: nil, title: "",
                                    description: "Возможно выбрать максимум 3 услуги")
            } else {
                self.choisenServices.append(self.allServices[index])
            }
        }
        cityDropDownView.textField.didSelect { [unowned self] (_, index, _) in
            self.choisenCity = self.allCityList[index]
        }
        servicesDropDownView.textField.didDeselect { [unowned self] (index) in
            self.choisenServices.removeAll(where: { $0.id == self.allServices[index].id })
        }
        cityDropDownView.textField.didDeselect { [unowned self] (index) in
            self.choisenCity = nil
        }
    }
    
    @objc func doneAction() {
        self.view.endEditing(true)
    }
    
    private func fieldsValidation() -> Bool {
        // СЕРВИСЫ
        if choisenServices.count > 0 && choisenServices.count < 4 {
            servicesDropDownView.textField.notValidValueIn(withPlaceholder: "Выберите от 1 до 3 услуг")
            return false
        }
        // ПРОДОЛЖИТЕЛЬНОСТЬ РАБОТ
        do{
            let text = try self.durationWorkingTF.validateText()
            guard let duration = Int(text)  else {
                self.durationWorkingTF.textField.notValidValueIn(withPlaceholder: "Число невозможно преобразовать")
                return false
            }
            guard duration > 0 && duration < 183 else {
                self.durationWorkingTF.textField.notValidValueIn(withPlaceholder: "Диапазон от 1 до 182")
                return false
            }
        }
        catch { durationWorkingTF.textField.notValidValueIn(withPlaceholder: "Ввести можно только цифры")
            return false
        }
        // ЦЕНА ЗА РАБОТУ
        do{
            let text = try self.priceWorkingTF.validateText()
            guard let price = Int(text)  else {
                self.priceWorkingTF.textField.notValidValueIn(withPlaceholder: "Число невозможно преобразовать")
                return false
            }
            guard price > 0 && price <= 99999999 else {
                self.priceWorkingTF.textField.notValidValueIn(withPlaceholder: "Диапазон от 1 до 99999999")
                return false
            }
        }
        catch { priceWorkingTF.textField.notValidValueIn(withPlaceholder: "Ввести можно только цифры")
            return false
        }
        // ГОРОД
        if choisenCity == nil {
            cityDropDownView.textField.notValidValueIn(withPlaceholder: "Выберите город")
            return false
        }
        // ОПИСАНИЕ МЕСТА
        if let place = placeDescription.textField.text, place != "" {
            if place.count > 95 {
                placeDescription.textField.notValidValueIn(withPlaceholder: "Не более 95 символов")
                return false
            }
        }
        // КРАТКОЕ ОПИСАНИЕ РАБОТ
        guard let short = shortDescriptionTF.textField.text, short != "" else {
            shortDescriptionTF.textField.notValidValueIn(withPlaceholder: "Поле является обязательным")
            return false
        }
        if short.count > 95 {
            shortDescriptionTF.textField.notValidValueIn(withPlaceholder: "Не более 95 символов")
            return false
        }
        //ПОЛНОЕОПИСАНИЕ РАБОТ
        if let full = fullDescriptionTF.textField.text, full != "" {
            if full.count > 255 {
                fullDescriptionTF.textField.notValidValueIn(withPlaceholder: "Не более 255 символов")
                return false
            }
        }
        return true
    }
    
    
    private func createAdd() {
        let cityId = choisenCity!.cityId
        let services = choisenServices.map { $0.id }
        let term = Int(durationWorkingTF.textField.text!)!
        let price = Int(priceWorkingTF.textField.text!)!
        let place = placeDescription.textField.text
        let shortDescription = shortDescriptionTF.textField.text!
        let fullDescription = fullDescriptionTF.textField.text
        
        adsViewModel.createNewAd(city_id: cityId, service_id: services,
                                 term: term, price: price, place_for_work: place,
                                 description: shortDescription, description_full: fullDescription)
    }
    
    private func reloadViewForNewInput() {
        self.choisenServices.removeAll()
        self.servicesDropDownView.textField.multipleSelectedIndex.removeAll()
        self.cityDropDownView.textField.selectedIndex = nil
        self.choisenCity = nil
        self.servicesDropDownView.textField.text = ""
        self.durationWorkingTF.textField.text = ""
        self.priceWorkingTF.textField.text = ""
        self.cityDropDownView.textField.text = ""
        self.placeDescription.textField.text = ""
        self.shortDescriptionTF.textField.text = ""
        self.fullDescriptionTF.textField.text = ""
    }
}
