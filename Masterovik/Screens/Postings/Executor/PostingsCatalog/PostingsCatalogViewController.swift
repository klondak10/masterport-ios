//
//  PostingsCatalogViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 02.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PostingsCatalogViewController: UIViewController, TabControllerSelectable,
                                    UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var chooseCity: ListDisclosureItem!
    @IBOutlet weak var chooseService: ListDisclosureItem!
    
    @IBOutlet weak var tableView: AutoHeightTableView! {
        didSet {
            MyPostingsTableViewCell.registerNib(for: tableView)
        }
    }
    
    private let cityViewModel = RegistrationViewModel()
    private let adsViewModel = AdsViewModel()
    private let serviceViewModel = ServiceViewModel()
    private let disposeBag = DisposeBag()
    
    private var filters = FilterParametersModel() { didSet { self.forFiler(filters)}}
    private var catalogAds: [AdsModel] = [] {
        didSet { tableView.reloadData() }
    }
    private var cityList: [CityModel] = []
    private var serviceList: [ServiceModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        configureRx()
    }
    
    private func configureUI() {
        let image = UIImage(named: "list-icon-green")
        chooseService.imageView.image = image
        chooseCity.imageView.image = image
    }
    
    private func configureRx() {
        adsViewModel.filterAllAds()
        serviceViewModel.getServiceList()
        
        adsViewModel.onFilteredAds
            .drive(onNext: { [unowned self] (list) in
                self.catalogAds = list
            }).disposed(by: disposeBag)
        
        cityViewModel.onCityList
            .subscribe(onNext: { [unowned self] (list) in
                self.cityList = list
            }).disposed(by: disposeBag)
        
        serviceViewModel.onServiceList
            .drive(onNext: { [unowned self] (list) in
                self.serviceList = list
            }).disposed(by: disposeBag)
        
        chooseCity.button.rx.tap
            .subscribe { [unowned self] (_) in
                let indexOfChoisenItem = self.cityList.firstIndex(where: {$0.cityId == self.filters.city?.cityId})
                let array = indexOfChoisenItem != nil ? [indexOfChoisenItem!] : nil
                self.showPopupTable(items: self.cityList.map{ $0.cityName },
                                    multipleChoise: false,
                                    selectedCellsIndex: array,
                                    addHandler: { [unowned self] (oneCity) in
                                        
                                        if oneCity.count > 0 {
                                            self.filters.city = self.cityList.first { $0.cityName == oneCity[0]}
                                        }
                })
        }.disposed(by: disposeBag)
        
        chooseService.button.rx.tap
            .subscribe { [unowned self] (_) in
                let array = self.filters.services?.map({ self.serviceList.firstIndex(of: ($0))!})
                
                self.showPopupTable(items: self.serviceList.map { $0.name },
                                    multipleChoise: true,
                                    selectedCellsIndex: array,
                                    addHandler: { (list) in
                                        
                                        let choisenServices = self.serviceList
                                            .filter({ (item) -> Bool in
                                                list.contains(item.name)
                                            })
                                        self.filters.services = choisenServices
                })
        }.disposed(by: disposeBag)
    }
    
    func forFiler(_ filter: FilterParametersModel) {
        adsViewModel.filterAllAds(city_id: filter.city?.cityId,
                                  services_id: filter.services?.map { $0.id },
                                  priceFrom: filter.priceFrom, priceTo: filter.priceTo,
                                  dateFrom: filter.dateFrom, dateTo: filter.dateTo,
                                  durationFrom: filter.durationFrom, durationTo: filter.durationTo)
    }
    
    
    func selectedBy(tabController: TabViewController) {
        tabController.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "settings-icon-black"), target: self, action: #selector(touchSortSettingsButton(_:)))
    }
    
    @objc func touchSortSettingsButton(_ sender: UIBarButtonItem) {
        let filterVC = SortOrdersViewController.instanceFromStoryboard as! SortOrdersViewController
        filterVC.cityList = self.cityList
        filterVC.serviceList = self.serviceList
        filterVC.changedFiltersAction = { [weak self] params in
            self?.filters = params
        }
        pushVC(filterVC)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return catalogAds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MyPostingsTableViewCell.dequeue(tableView: tableView) 
        cell.configure(catalogAds[indexPath.row])
        cell.postingLastChangingLabel.isHidden = true
        cell.selectionStyle = .none
        return cell
    }
}

