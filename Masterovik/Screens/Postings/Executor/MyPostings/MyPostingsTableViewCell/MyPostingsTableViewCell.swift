//
//  MyPostingsTableViewCell.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 02.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class MyPostingsTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var postingNameLabel: UILabel! {
        didSet {
            postingNameLabel.font = FontHelper.font(type: .franklin, size: .medium)
            postingNameLabel.adjustsFontSizeToFitWidth = true
        }
    }
    
    @IBOutlet weak var postingDateLabel: UILabel! {
        didSet {
            postingDateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var postingPriceLabel: UILabel! {
        didSet {
            postingPriceLabel.font = FontHelper.font(type: .myriad, size: .title)
        }
    }
    
    @IBOutlet weak var postingDescriptionLabel: UILabel! {
        didSet {
            postingDescriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    @IBOutlet weak var postingLastChangingLabel: UILabel! {
        didSet {
            postingLastChangingLabel.layer.cornerRadius = postingLastChangingLabel.bounds.height/3
            postingLastChangingLabel.font = FontHelper.font(type: .roboto, size: .small)
        }
    }
    
    @IBOutlet weak var postingDurationLabel: UILabel! {
        didSet {
            postingDurationLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    private var advertisement: AdsModel?

    func configure(_ ad: AdsModel) {
        self.advertisement = ad
        
        var summaryRes = ad.services.reduce("", { (res, item)in
            res + item.name + ", " })
        if summaryRes.count > 2{
            summaryRes.removeLast()
            summaryRes.removeLast()
        }
        
        
        self.postingNameLabel.text = summaryRes
        self.postingDateLabel.text = ad.dateStart
        self.postingDescriptionLabel.text = ad.shortDescription
        self.postingPriceLabel.text = String(format: "%.02f", ad.price) + " руб"
        
        let delta = ad.duration % 10

        switch delta {
        case 1:
            if ad.duration != 11 { self.postingDurationLabel.text = "1 день"}
            else {  self.postingDurationLabel.text = "\(ad.duration) дней" }
        case 2...4:
            if ad.duration > 10 && ad.duration < 20  { self.postingDurationLabel.text = "\(ad.duration) дней"}
            else { self.postingDurationLabel.text = "\(ad.duration) дня" }
        default:
            self.postingDurationLabel.text = "\(ad.duration) дней"
        }
        
    }
}
