//
//  MyPostingsViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 02.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MyPostingsViewController: UIViewController, TabControllerSelectable, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(UINib(nibName: "AddServiceCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "AddServiceCollectionViewCell")
        }
    }
    
    @IBOutlet weak var myCityLabel: UILabel! {
        didSet {
            myCityLabel.font = FontHelper.font(type: .franklin, size: .title)
        }
    }
    
    @IBOutlet weak var advertisementsLabel: UILabel! {
        didSet {
            advertisementsLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var postingsForMeLabel: UILabel! {
        didSet {
            postingsForMeLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var tableView: AutoHeightTableView! {
        didSet {
            MyPostingsTableViewCell.registerNib(for: tableView)
        }
    }
    
    private let servicesViewModel = ServiceViewModel()
    private let adsViewModel = AdsViewModel()
    private let cityViewModel = RegistrationViewModel()
    private let disposeBag = DisposeBag()
    
    private var userServicesSet: Set<ServiceModel> = [] {didSet {
                self.userServicesArray = userServicesSet.sorted { $0.id > $1.id } } }
    private var allServicesSet: Set<ServiceModel> = []
    private var userServicesArray: [ServiceModel] = [] { didSet { self.collectionView.reloadData() } }
    private var myAds: [AdsModel] = [] { didSet { tableView.reloadData() } }
    
    private var userCityName: String? { didSet { if let city = userCityName {
                self.myCityLabel.text = "Мой Город: \(city)" } } }
    private var cityList: [CityModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ServiceCollectionViewCell.identyfier)
        collectionView.contentInset = UIEdgeInsets(top: CellConstantSize.topOffset,
                                                   left: CellConstantSize.leftOffset,
                                                   bottom: CellConstantSize.bottomOffset,
                                                   right: CellConstantSize.rightOffset)
        configureRx()
    }
    
    private func configureRx() {
        adsViewModel.getMyAds()
        
        //Получает данные о текущих сервисах пользователя и всех
        servicesViewModel.onUserServices.drive(onNext: { [unowned self] (services) in
            self.userServicesSet = Set(services)
        }).disposed(by: disposeBag)
        servicesViewModel.onServiceList.drive(onNext: { [unowned self] (services) in
            self.allServicesSet = Set(services)
        }).disposed(by: disposeBag)
        //Управление событиями добавления и удаления сервисов
        servicesViewModel.onAddServicesSuccess
            .subscribe(onNext: { [unowned self] (result) in
                if result { self.servicesViewModel.getUserServices() }
            }).disposed(by: disposeBag)
        servicesViewModel.onRemoveServicesSuccess
            .subscribe(onNext: { [unowned self] (result) in
                if result { self.servicesViewModel.getUserServices() }
            }).disposed(by: disposeBag)
        
        cityViewModel.onCityList
            .subscribe(onNext: { [unowned self] (cityList) in
                self.cityList = cityList
                let userCityId = UserCoreDataManager.shared.getUser()?.cityId
                self.userCityName = cityList.first { $0.cityId == Int(userCityId!) }?.cityName
            }).disposed(by: disposeBag)
        
        //Работа с моими объявлениями
        adsViewModel.onMyAds
            .drive(onNext: { [unowned self] (ads) in
                self.myAds = ads
            })
            .disposed(by: disposeBag)
    }
    
    
    func selectedBy(tabController: TabViewController) {
       tabController.navigationItem.rightBarButtonItem = nil
    }
    
    //Mark: TABLEVIEW DELEGATE, DATASOURCE
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myAds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MyPostingsTableViewCell.dequeue(tableView: tableView)
        cell.configure(myAds[indexPath.row])
        cell.postingLastChangingLabel.isHidden = true
        cell.selectionStyle = .none 
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailAdVC = PostingNameViewController.instanceFromStoryboard as! PostingNameViewController
        let currentCity = cityList.first { $0.cityId == myAds[indexPath.row].cityId }?.cityName
        detailAdVC.ad = myAds[indexPath.row]
        detailAdVC.cityName = currentCity
        pushVC(detailAdVC)
    }
}

extension MyPostingsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userServicesArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ServiceCollectionViewCell.identyfier,
                                                      for: indexPath) as! ServiceCollectionViewCell
        let addServiceCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddServiceCollectionViewCell", for: indexPath)
        if indexPath.row == userServicesArray.count {
            return addServiceCell
        } else {
            cell.configure(from: userServicesArray[indexPath.row])
    
            //Подписка на события нажатия на кнопку удаления
            cell.deleteButton.rx.tap
                .subscribe { [weak self] (_) in
                    if let item = self?.allServicesSet.first(where: { $0.name == cell.serviceNameLabel.text!}) {
                        self?.servicesViewModel.removeServices(servicesID: [item.id])
                    }
                }.disposed(by: disposeBag)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CellConstantSize.width, height: CellConstantSize.height)
    }
    
    //TODO: Добавить форму добавления сервисов
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == userServicesArray.count {
        
            let result = self.allServicesSet.subtracting(userServicesSet)
                .map { $0.name }
                .sorted (by: >)
            
            if result.count > 0 {
                self.showPopupTable(items: result, addHandler: { [unowned self] (list) in
                    
                    let toAdd = self.allServicesSet.filter {
                        list.contains($0.name) }
                        .map { $0.id }
                    self.servicesViewModel.addServices(servicesID: toAdd)
                    })
                
            } else {
                self.showWireframe(yesHandler: nil, noHandler: nil, title: "", description: "У вас выбраны все возможные заявки.")
            }
        }
    }
}
