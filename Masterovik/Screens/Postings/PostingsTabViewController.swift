//
//  PostingsTabViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 02.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class PostingsTabViewController: TabViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if currentRoleIsExecutor {
            let myPostingsController = MyPostingsViewController.instanceFromStoryboard
            let postingsCatalog = PostingsCatalogViewController.instanceFromStoryboard
            setControllers([myPostingsController, postingsCatalog])
        } else {
            let answeredPostingsVC = UnAndAnsweredPostingsViewController.instanceFromStoryboard
            let unAnsweredPostingsVC = UnAndAnsweredPostingsViewController.instanceFromStoryboard
            (unAnsweredPostingsVC as? UnAndAnsweredPostingsViewController)?.isAnsweredPostings = false
            setControllers([answeredPostingsVC, unAnsweredPostingsVC])
        }
        title = "Объявления"
        navCon?.navigationBar.isHidden = false
        navCon?.viewControllers = [self]
    }
}
