//
//  AdsViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/21/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya
import SwiftyJSON

class AdsViewModel {
    
    //Fields
    
    private let newAdCreatedSuccess = PublishSubject<Bool>()
    private let myAds = PublishSubject<[AdsModel]>()
    private let filteredAds = PublishSubject<[AdsModel]>()
    
    private let provider: myRxProvider<AdsRequestAPI>
    private let disposeBag = DisposeBag()
    
    init() {
        provider = myRxProvider<AdsRequestAPI>()
    }
    
    //Properties
    var onAdCreated : Observable<Bool> {
        return newAdCreatedSuccess
            .asObservable()
    }
    
    var onMyAds: Driver<[AdsModel]> {
        return myAds
            .asObservable()
            .distinctUntilChanged{ $0.count == $1.count }
            .asDriver(onErrorJustReturn: [])
    }
    
    var onFilteredAds: Driver<[AdsModel]> {
        return filteredAds
            .asObservable()
            .distinctUntilChanged{ $0.count == $1.count }
            .asDriver(onErrorJustReturn: [])
    }
    
    func getMyAds() {
        provider.request(.my)
            .subscribe {[weak self] (response) in
                switch response {
                case .success(let result):
                    do {
                        let json = try JSON(data: result.data)
                        let list = json["response"].arrayValue.map { AdsModel(from: $0) }
                        self?.myAds.onNext(list)
                    } catch {
                        print("Ошибка трансформации объявлений из Data")
                    }
                case .error(let error):
                    print(error.localizedDescription)
                    self?.myAds.onError(error)
                }
        }.disposed(by: disposeBag)
    }
    
    func createNewAd(city_id: Int, service_id: [Int], term: Int, price: Int,
                     place_for_work: String?, description: String, description_full: String?) {
        
        provider.request(.create(city_id: city_id, service_id: service_id, term: term, price: price,
                                 place_for_work: place_for_work, description: description, description_full: description_full))
            .subscribe { [weak self] (response) in
                switch response {
                case .success( let result):
                    do {
                        let json = try JSON(data: result.data)
                        let list = json["error_message"].stringValue
                        if list == "" {
                            self?.newAdCreatedSuccess.onNext(true)
                        } else { self?.newAdCreatedSuccess.onNext(false)}
                    } catch {
                        print("Ошибка response при создании объявления (case: success)")
                    }
                case .error(let error):
                    print("Ошибка response при создании объявления (case: \(error.localizedDescription))")
                    self?.newAdCreatedSuccess.onNext(false)
                }
        }.disposed(by: disposeBag)
    }
    
    /// Filtering all Ads. Date format must be since 1970's.
    func filterAllAds(city_id: Int? = nil, services_id: [Int]? = nil, priceFrom: Int? = nil,
                      priceTo: Int? = nil,  dateFrom: Double? = nil, dateTo: Double? = nil,
                      durationFrom: Int? = nil, durationTo: Int? = nil) {
        let priceFromTo = FromToFilterModel(from: priceFrom, to: priceTo)
        let durationFromTo = FromToFilterModel(from: durationFrom, to: durationTo)
        
        
        provider.request(.all(city_id: city_id, services_id: services_id, priceFromTo: priceFromTo, dateFrom: dateFrom, dateTo: dateTo, durationFromTo: durationFromTo))
            .subscribe { [weak self] (response) in
                switch response {
                case .success(let result):
                    do {
                        let json = try JSON(data: result.data)
                        let list = json["response"].arrayValue.map { AdsModel(from: $0) }
                        self?.filteredAds.onNext(list)
                    } catch {
                        print("Ошибка трансформации объявлений из Data")
                    }
                case .error(let error):
                    print(error.localizedDescription)
                    self?.myAds.onError(error)
                }
            }.disposed(by: disposeBag)
    }
}

