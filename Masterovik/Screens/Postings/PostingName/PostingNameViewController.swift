//
//  PostingNameViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 31.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

//TODO: После создания запроса на бэке сделать фукционал кнопки
class PostingNameViewController: BaseViewController {
    
    @IBOutlet weak var shadowView: ShadowView!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var orderPriceLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var quickDescriptionLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var commisionLabel: UILabel!
    @IBOutlet weak var readyToWorkButton: NextButton!
    
    var ad: AdsModel?
    var cityName: String?
    var adName: String = "Название объявления"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = adName
        navigationController?.navigationBar.isHidden = false
        readyToWorkButton.titleLabel?.font = FontHelper.font(type: .franklin, size: .medium)
        readyToWorkButton.titleLabel?.adjustsFontSizeToFitWidth = true
        guard let ad = ad else { return }
        configure(for: ad, cityName: cityName ?? "")
    }
    
    private func configure(for detail: AdsModel, cityName: String) {
        self.ad = detail
    
        var summaryRes = detail.services.reduce("", { (res, item)in
            res + item.name + ", " })
        if summaryRes.count > 2{
            summaryRes.removeLast()
            summaryRes.removeLast()
        }
        title = summaryRes
        customerNameLabel.text = ad?.name
        orderPriceLabel.text = String(format: "%.02f", detail.price) + " руб"
        cityLabel.text = cityName
        addressLabel.text = detail.placeDescription ?? ""
        quickDescriptionLabel.text = detail.shortDescription
        descriptionLabel.text = detail.fullDescription ?? ""
        commisionLabel.text = "Ком. ??"
        switch detail.duration {
        case 1:
            self.durationLabel.text = "1 день"
        case 2...4:
            self.durationLabel.text = "\(detail.duration) дня"
        default:
            self.durationLabel.text = "\(detail.duration) дней"
        }
    }
    
    @IBAction func editTiming(_ sender: Any) {
        let vc = ChangeOrderDetailsViewController.instanceFromStoryboard as! ChangeOrderDetailsViewController
        vc.isTimingEditor = true
        pushVC(vc)
    }
    
    @IBAction func editPrice(_ sender: Any) {
        let vc = ChangeOrderDetailsViewController.instanceFromStoryboard as! ChangeOrderDetailsViewController
        vc.isTimingEditor = false
        pushVC(vc)
    }
}
