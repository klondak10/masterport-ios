//
//  LeftMenuViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import SideMenuSwift

class LeftMenuExecutorViewController: UIViewController {
    
    @IBOutlet weak var helloLabel: UILabel! {
        didSet {
            helloLabel.font = FontHelper.font(type: .myriad, size: .medium)
            helloLabel.textColor = .white
        }
    }
    
    @IBOutlet weak var balanceLabel: UILabel! {
        didSet {
            balanceLabel.font = FontHelper.font(type: .myriad, size: .small)
            balanceLabel.textColor = .white
        }
    }
    
    @IBOutlet weak var ordersItem: MenuItemView! {
        didSet {
            ordersItem.imageView.image = UIImage(named: "tools-icon-green")
            ordersItem.button.addTarget(self, action: #selector(touchOrdersItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var postingsItem: MenuItemView! {
        didSet {
            postingsItem.imageView.image = UIImage(named: "papers-icon-green")
            postingsItem.button.addTarget(self, action: #selector(touchPostingsItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var applicationsItem: MenuItemView! {
        didSet {
            applicationsItem.imageView.image = UIImage(named: "tools-arrow-icon-green")
            applicationsItem.button.addTarget(self, action: #selector(touchApplicationsItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var ratingItem: MenuItemView! {
        didSet {
            ratingItem.imageView.image = UIImage(named: "star-icon-green")
            ratingItem.button.addTarget(self, action: #selector(touchRatingItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var walletItem: MenuItemView! {
        didSet {
            walletItem.imageView.image = UIImage(named: "wallet-icon-green")
            walletItem.button.addTarget(self, action: #selector(touchWalletItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var profileItem: MenuItemView! {
        didSet {
            profileItem.imageView.image = UIImage(named: "user-icon-green")
            profileItem.button.addTarget(self, action: #selector(touchProfileItem(_:)), for: .touchUpInside)
        }
    }
    
    
    @IBOutlet weak var orderServicesButton: UIButton! {
        didSet {
            orderServicesButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
        }
    }
    
    @IBOutlet weak var faqButton: UIButton! {
        didSet {
            faqButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
        }
    }
    
    
    @IBOutlet weak var exitButton: UIButton! {
        didSet {
            exitButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
        }
    }
    
    @IBOutlet weak var contentSideOffsetConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let width = SideMenuController.preferences.basic.menuWidth
        contentSideOffsetConstraint.constant = UIScreen.main.bounds.width - width
        
        getUserData()
        UserCoreDataManager.shared.delegate = self
    }
    
    private func getUserData() {
        
        if let user = UserCoreDataManager.shared.getUser(),
                user.firstName != "" || user.firstName != nil {
            updateUsername(firstName: user.firstName!, lastName: user.lastName!)
            updateBalance(on: user.balance)
            
        }
    }
    
    // MARK: - @objc button actions
    @objc func touchOrdersItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(OrdersTabViewController())
    }
    
    @objc func touchPostingsItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(PostingsTabViewController())
    }

    @objc func touchApplicationsItem(_ sender: UIButton) {

        sideMenuController?.hideMenu()
        pushVC(ApplicationTabViewController())
    }
    
    @objc func touchRatingItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(RatingsTabViewController())
    }
    
    @objc func touchWalletItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(WalletTabViewController())
    }
    
    @objc func touchProfileItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(MainSettingsViewController.instanceFromStoryboard)
    }
    
    // MARK: - IBActions
    
    @IBAction func touchOrderServicesButton(_ sender: Any) {
        UserCoreDataManager.shared.updateUserRole(on: 2)
        
        (UIApplication.shared.delegate as! AppDelegate).startSetupSideMenu()
    }
    
    @IBAction func touchFAQButton(_ sender: Any) {
        sideMenuController?.hideMenu()
        pushVC(FAQTabViewController())
    }
    
    @IBAction func touchExitButton(_ sender: Any) {
        
       self.userExit()
    }
}

extension LeftMenuExecutorViewController: UserDataUpdatingDelegate {
    
    func updateUsername(firstName: String, lastName: String) {
        DispatchQueue.main.async {
            self.helloLabel.text = "Привет, \(firstName)"
        }
        self.view.layoutIfNeeded()
    }
    
    func updateBalance(on balance: Double) {
        DispatchQueue.main.async {
            self.balanceLabel.text = "На вашем счету: \(balance) руб."
        }
        self.view.layoutIfNeeded()
    }
}
