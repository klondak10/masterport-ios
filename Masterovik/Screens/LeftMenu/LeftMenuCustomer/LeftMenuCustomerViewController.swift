//
//  LeftMenuCustomerViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 31.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import SideMenuSwift

class LeftMenuCustomerViewController: UIViewController {

    @IBOutlet weak var contentSideOffsetConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var helloLabel: UILabel! {
        didSet {
            helloLabel.font = FontHelper.font(type: .myriad, size: .medium)
            helloLabel.textColor = .white
        }
    }
    
    @IBOutlet weak var addPostingButton: AddButtonView! {
        didSet {
            addPostingButton.button.addTarget(self, action: #selector(addPostingButtonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var ordersItem: MenuItemView! {
        didSet {
            ordersItem.imageView.image = UIImage(named: "tools-icon-green")
            ordersItem.button.addTarget(self, action: #selector(touchOrdersItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var postingsItem: MenuItemView! {
        didSet {
            postingsItem.imageView.image = UIImage(named: "papers-icon-green")
            postingsItem.button.addTarget(self, action: #selector(touchPostingsItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var executorsItem: MenuItemView! {
        didSet {
            executorsItem.imageView.image = UIImage(named: "tools-arrow-icon-green")
            executorsItem.button.addTarget(self, action: #selector(touchExecutorsItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var profileItem: MenuItemView! {
        didSet {
            profileItem.imageView.image = UIImage(named: "user-icon-green")
            profileItem.button.addTarget(self, action: #selector(touchProfileItem(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var orderServicesButton: UIButton! {
        didSet {
            orderServicesButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
        }
    }
    
    @IBOutlet weak var FAQButton: UIButton! {
        didSet {
            FAQButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
        }
    }
    
    @IBOutlet weak var exitButton: UIButton! {
        didSet {
            exitButton.titleLabel?.font = FontHelper.font(type: .myriad, size: .small)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let width = SideMenuController.preferences.basic.menuWidth
        contentSideOffsetConstraint.constant = UIScreen.main.bounds.width - width
        
        getUserData()
        UserCoreDataManager.shared.delegate = self
    }
    
    private func getUserData() {
        
        if let user = UserCoreDataManager.shared.getUser(),
            user.firstName != "" || user.firstName != nil {
            updateUsername(firstName: user.firstName!, lastName: user.lastName!)
            updateBalance(on: user.balance)
        }
    }
    
    @objc func addPostingButtonPressed(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(CreateAdViewController.instanceFromStoryboard)
    }
    
    @objc func touchOrdersItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(OrdersTabViewController())
    }
    
    @objc func touchPostingsItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(PostingsTabViewController())
    }
    
    @objc func touchExecutorsItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
//        pushVC(<#T##viewController: UIViewController##UIViewController#>) // Добавить реализацию как будет ясно что это, вообще
    }
    
    @objc func touchProfileItem(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(MainSettingsViewController.instanceFromStoryboard)
    }
    
    @IBAction func orderServicesPressed(_ sender: UIButton) {
        UserCoreDataManager.shared.updateUserRole(on: 1)
        (UIApplication.shared.delegate as! AppDelegate).startSetupSideMenu()
    }
    
    @IBAction func FAQButtonPressed(_ sender: UIButton) {
        sideMenuController?.hideMenu()
        pushVC(FAQTabViewController())
    }
    
    @IBAction func exitButtonPressed(_ sender: UIButton) {
       self.userExit()
    }
}

extension LeftMenuCustomerViewController: UserDataUpdatingDelegate {
    
    func updateUsername(firstName: String, lastName: String) {
        DispatchQueue.main.async {
            self.helloLabel.text = "Привет, \(firstName)"
        }
    }
    
    func updateBalance(on balance: Double) {
    }
}
