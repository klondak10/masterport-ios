//
//  SignInViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import RxCocoa
import SwiftyJSON
import Moya
import Foundation

class SignInViewModel {
    
    private let userLoginProvider: MoyaProvider<UserRequestAPI>

    private let phoneNumberExist = PublishSubject<Bool>()
    
    private let changePhoneStart = PublishSubject<Bool>()
    private let changePhoneFinish = PublishSubject<Bool>()
    
    private let smsIsValid = PublishSubject<Bool>()

    private let disposeBag = DisposeBag()
    
    var onPhoneExistingChanged: Observable<Bool> {
        return phoneNumberExist
            .asObservable()
            .distinctUntilChanged()
    }
    
    var onSmsIsValid: Observable<Bool> {
        return smsIsValid
            .asObservable()
    }
    
    var onChangePhoneFirstStep: Observable<Bool> {
        return changePhoneStart.asObserver()
    }
    
    var onChangePhoneLastStep: Observable<Bool> {
        return changePhoneFinish.asObserver()
    }
    
    init() {
        userLoginProvider = MoyaProvider<UserRequestAPI>.init(plugins: [NetworkLoggerPlugin(verbose: true)])
    }
    
    func checkNumberExist(phone number: String) {
        
        userLoginProvider.rx.request(.checkPhoneExist(phone: number))
            .subscribe { [weak self] (response) in
                switch response {
                case .success(let result):
                    if result.statusCode == 200 {
                        self?.phoneNumberExist.onNext(true)
                    } else if result.statusCode == 401 {
                        self?.phoneNumberExist.onNext(false)
                    } else {
                        print("Ошибка response запроса на login")
                    }
                case .error(let error):
                    print(error.localizedDescription)
                    self?.phoneNumberExist.onError(error)
                }
            }.disposed(by: disposeBag)
    }
    
    func checkValidSms(phone number: String, with sms: String) {
        
        userLoginProvider.rx.request(.login(phone: number, sms: sms))
            .subscribe { [weak self] (response) in
                switch response {
                case .success(let result):
                    if result.statusCode == 200 {
                        self?.responseHandler(result.data)

                    } else if result.statusCode == 404 {
                        self?.smsIsValid.onNext(false)
                    } else {
                        self?.smsIsValid.onError(NSError(domain: "Ошибка запроса", code: 1, userInfo: nil))
                    }
                case .error(let error):
                    print(error.localizedDescription)
                    self?.smsIsValid.onError(error)
                }
            }.disposed(by: disposeBag)
    }
    
    func changingPhoneGetSms(on phone: String) {
        let provider = myRxProvider<UserRequestAPI>()
        provider.request(.phoneChangeFirstStep(phone: phone))
            .subscribe(onSuccess: { [unowned self] (response) in
                if response.statusCode == 200{
                    self.changePhoneStart.onNext(true)
                } else {
                    self.changePhoneStart.onNext(false)
                }
            }).disposed(by: disposeBag)
    }
    
    func changingPhoneCheckSms(on phone: String, with sms: String) {
        let provider = myRxProvider<UserRequestAPI>()
        provider.request(.phoneChangeLastStep(phone: phone, sms: sms))
            .subscribe(onSuccess: { [unowned self] (response) in
                if response.statusCode == 200 {
                    self.changePhoneFinish.onNext(true)
                    UserCoreDataManager.shared.updateUserNumber(on: phone)
                } else {
                    self.changePhoneFinish.onNext(false)
                }
            }).disposed(by: disposeBag)
    }
    
    private func responseHandler(_ data: Data) {
        do {
            let response = try JSONDecoder().decode(LoginModel.self, from: data)
            TokenManager.changeAccess(token: response.access_token)
            TokenManager.changeRefreshToken(token: response.refresh_token)
            
            getUserData()
        } catch {
            self.smsIsValid.onNext(true)
            return
        }
    }
    
    func getUserData() {
        userLoginProvider.rx.request(.getUserData)
            .subscribe { [unowned self] (respone) in
                switch respone {
                case .success(let result):
                    do {
                        let user = try JSONDecoder().decode(Executor.self, from: result.data)
                        UserCoreDataManager.shared.save(user: user)
                        
                         UserDefaults.standard.set(true, forKey: userIsRegistered)
                        if user.roleId == 1 {
                            UserDefaults.standard.set(true, forKey: userWasYetExecutor)
                        } else { UserDefaults.standard.set(false, forKey: userWasYetExecutor) }
                        self.smsIsValid.onNext(true)
                        (UIApplication.shared.delegate as! AppDelegate).startSetupSideMenu()
                    } catch {
                        assertionFailure("Ошибка преобразования данных в экземпляр Executor")
                    }
                case .error(let error):
                    assertionFailure("""
                        Ошибка получения данных пользователя
                        \(error.localizedDescription)
                        """)
                }
        }.disposed(by: disposeBag)
    }
}
