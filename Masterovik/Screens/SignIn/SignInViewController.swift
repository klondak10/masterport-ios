//
//  SignInViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SideMenuSwift

class SignInViewController: BaseViewController, RoundTimerDelegate {
    
    
    @IBOutlet weak var shadowView: ShadowView!
    @IBOutlet weak var phoneLabeledField: LabeledFieldView! {
        didSet {
            phoneLabeledField.validateType = .phoneNumber
            phoneLabeledField.isPhoneNumberTF = true
        }
    }
    @IBOutlet weak var smsCodeLabeledField: LabeledFieldView! {
        didSet {
            smsCodeLabeledField.validateType = .sms
        }
    }
    @IBAction func tapOnSuperview(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBOutlet weak var getCodeButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var timerView: RoundTimerView!
    
    private let viewModel = SignInViewModel()
    private let disposeBag = DisposeBag()
    private var isRegister = true
    private let sendCodeAgain = "Выслать код повторно"
    private let sendCodeFirst = "Получить код"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureRx()
        timerView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        shadowView.shadowCorners = 15.0
    }
    
    private func configureUI() {
        SideMenuController.preferences.basic.enablePanGesture = false
        
        getCodeButton.layer.borderColor = UIColor.darkGreen.cgColor
        getCodeButton.layer.borderWidth = 1.0
        if #available(iOS 12.0, *) {
            smsCodeLabeledField.textField.textContentType = .oneTimeCode
        }
        navigationController?.setViewControllers([self], animated: false)
        topViewHeightConstraint?.isActive = false
        topGradientView.autoPinEdge(toSuperviewEdge: .bottom)
        phoneLabeledField.textField.keyboardType = .phonePad
        self.getCodeButton.isUserInteractionEnabled = false
    }
    
    private func configureRx() {
        
        ///ViewModel Subscribes
        
        viewModel.onPhoneExistingChanged
            .bind { [weak self] (next) in
                self?.isRegister = !next
        }.disposed(by: disposeBag)
        
        
        viewModel.onSmsIsValid
            .subscribeOn(MainScheduler.instance)
            .subscribe { [weak self] (event) in
                guard let result = event.element, let `self` = self else { return }
                if !result {
                    self.showWireframe(yesHandler: nil, noHandler: nil,
                                       title: "Неправильный код", description: "Пожалуйста, попробуйте снова")
                } else {
                    if self.isRegister {
                        let chooseRoleVC = ChooseRoleViewController.instanceFromStoryboard as! ChooseRoleViewController
                        chooseRoleVC.setPhoneNumber(with: self.getPhoneNumber())
                        self.pushVC(chooseRoleVC)
                    } else {
                        UserDefaults.standard.set(true, forKey: userIsRegistered)
                     //   (UIApplication.shared.delegate as! AppDelegate).startSetupSideMenu()
                    }
                }
            }.disposed(by: disposeBag)
        
        
        /// TextFields Subscribes
        
        phoneLabeledField.isValidText.subscribe(onNext: {  [weak self] (value) in
            guard let`self` = self else { return }
            
            if self.getCodeButton.isUserInteractionEnabled == false {
                if value {
                    self.getCodeButton.setTitle(self.sendCodeFirst, for: .normal)
                    self.timerEnded()
                }
            }
            self.getCodeButton.isUserInteractionEnabled = value
            self.nextButton.isUserInteractionEnabled = false
        }).disposed(by: disposeBag)
        
        
        /// Buttons Subscribes
        
        getCodeButton.rx.tap.subscribe { [weak self] (_) in
            guard let `self` = self else { return }
                self.viewModel.checkNumberExist(phone: self.getPhoneNumber())
                self.getCodeButton.setTitle(self.sendCodeAgain, for: .normal)
                self.smsCodeLabeledField.isUserInteractionEnabled = true
                self.timerView.elapsedTime = 0
                self.timerView.startTimer()
                self.timerView.isHidden = false
                self.getCodeButton.isUserInteractionEnabled = false
                self.nextButton.isUserInteractionEnabled = true
                self.smsCodeLabeledField.textField.becomeFirstResponder()
            
        }.disposed(by: disposeBag)
        
        nextButton.rx.tap.subscribe { [weak self] (_) in
            guard let `self` = self else { return }
            self.viewModel.checkValidSms(phone: self.getPhoneNumber(),
                                         with: self.smsCodeLabeledField.textField.text ?? "")
        }.disposed(by: disposeBag)
    }
    
    
    
    private func getPhoneNumber() -> String {
        if let text = phoneLabeledField.textField.text {
            let unsafeChars = CharacterSet.alphanumerics.inverted
            var cleanChars  = text.components(separatedBy: unsafeChars).joined(separator: "")
            cleanChars = "+" + cleanChars
            return cleanChars
        } else {
            return "+"
        }
    }
    
    // MARK: - Timer delegate method
    
    func timerEnded() {
        timerView.isHidden = true
        self.getCodeButton.isUserInteractionEnabled = true
        self.view.endEditing(true )
    }
}
