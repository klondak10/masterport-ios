//
//  ApplicationTabViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 02.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ApplicationTabViewController: TabViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let sortSettingsButton = UIBarButtonItem(image: UIImage(named: "settings-icon-black"), target: self, action: #selector(touchSortSettingsButton(_:)), leftInset: 10.0)

        navigationItem.rightBarButtonItems = [sortSettingsButton]
        
        let applicationInWork = ApplicationInWorkViewController.instanceFromStoryboard
        let cancelledApplication = CancelledApplicationViewController.instanceFromStoryboard

        setControllers([applicationInWork, cancelledApplication])
        title = "Заявки"
        navCon?.navigationBar.isHidden = false
        navCon?.viewControllers = [self]
    }
    
    @objc func touchSortSettingsButton(_ sender: UIBarButtonItem) {
        pushVC(SortOrdersViewController.instanceFromStoryboard)
    }
}
