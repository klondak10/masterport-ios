//
//  ApplicationInWorkViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 02.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ApplicationInWorkViewController: UIViewController, TabControllerSelectable, UITableViewDelegate, UITableViewDataSource {
    
    var items: Array<Any> = [NSObject(), NSObject(), NSObject(), NSObject(), NSObject()]
    
    @IBOutlet weak var chooseCity: ListDisclosureItem! {
        didSet {
            let image = UIImage(named: "pin-icon-green")
            chooseCity.imageView.image = image
        }
    }
    
    @IBOutlet weak var chooseService: ListDisclosureItem! {
        didSet {
            let image = UIImage(named: "list-icon-green")
            chooseService.imageView.image = image
        }
    }
    
    @IBOutlet weak var tableView: AutoHeightTableView! {
        didSet {
            ApplicationInWorkTableViewCell.registerNib(for: tableView)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func selectedBy(tabController: TabViewController) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ApplicationInWorkTableViewCell.dequeue(tableView: tableView)
        return cell
    }
}
