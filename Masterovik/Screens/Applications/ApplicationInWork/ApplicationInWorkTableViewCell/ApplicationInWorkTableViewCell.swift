//
//  ApplicationInWorkTableViewCell.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 02.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ApplicationInWorkTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var postingNameLabel: UILabel! {
        didSet {
            postingNameLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var postingDateLabel: UILabel! {
        didSet {
            postingDateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var postingPriceLabel: UILabel! {
        didSet {
            postingPriceLabel.font = FontHelper.font(type: .myriad, size: .title)
        }
    }
    
    @IBOutlet weak var postingDescriptionLabel: UILabel! {
        didSet {
            postingDescriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var customerReactionLabel: UILabel! {
        didSet {
            customerReactionLabel.font = FontHelper.font(type: .roboto, size: .small)
        }
    }
}
