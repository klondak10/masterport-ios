//
//  CancelledApplicationViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 02.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CancelledApplicationViewController: UIViewController, TabControllerSelectable, UITableViewDataSource, UITableViewDelegate {
    
    var items: Array<Any> = [NSObject(), NSObject(), NSObject(), NSObject(), NSObject()]

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            CancelledApplicationTableViewCell.registerNib(for: tableView)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func selectedBy(tabController: TabViewController) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CancelledApplicationTableViewCell.dequeue(tableView: tableView)
        return cell
    }
}
