//
//  ProfileViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var infoContainerView: UIView!
    
    @IBOutlet weak var customerNameLabel: UILabel! {
        didSet {
            customerNameLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var registrationDateLabel: UILabel! {
        didSet {
            registrationDateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var registrationDateTextLabel: UILabel! {
        didSet {
            registrationDateTextLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    
    @IBOutlet weak var phoneLabel: UILabel! {
        didSet {
            phoneLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var mailLabel: UILabel! {
        didSet {
            mailLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var townLabel: UILabel! {
        didSet {
            townLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var doneCountLabel: UILabel! {
        didSet {
            doneCountLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var doneCountTextLabel: UILabel! {
        didSet {
            doneCountTextLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var canceledCountLabel: UILabel! {
        didSet {
            canceledCountLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var canceledCountTextLabel: UILabel! {
        didSet {
            canceledCountTextLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var allAdvertisementsLabel: UILabel! {
        didSet {
            allAdvertisementsLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var allAdvertisementsTextLabel: UILabel! {
        didSet {
            allAdvertisementsTextLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var claimsCountLabel: UILabel! {
        didSet {
            claimsCountLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var claimTextLabel: UILabel! {
        didSet {
            claimsCountLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topGradientView.autoPinEdge(.bottom, to: .bottom, of: infoContainerView, withOffset: 10.0)
    }
}
