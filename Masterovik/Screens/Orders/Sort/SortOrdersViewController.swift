//
//  SortOrdersViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SortOrdersViewController: BaseViewController {
    
    @IBOutlet weak var townSortDisclosureItem: ListDisclosureItem!
    @IBOutlet weak var serviceSortDisclosureItem: ListDisclosureItem!
    @IBOutlet weak var costSortDisclosureItem: ListDisclosureItem!
    @IBOutlet weak var dateFilterDisclosureItem: ListDisclosureItem!
    @IBOutlet weak var durationFilterDisclosureItem: ListDisclosureItem!
   
    private var datePickerView = UIDatePicker()
    
    private let disposeBag = DisposeBag()
    
    var filters = FilterParametersModel()
    var cityList: [CityModel] = []
    var serviceList: [ServiceModel] = []
    
    var changedFiltersAction: ((FilterParametersModel) -> Void)?
    
    @IBAction func touchButtonAction(_ sender: Any) {
        self.navCon?.popViewController(animated: true)
        self.changedFiltersAction?(self.filters)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        configureRx()
    }
    
    private func configureUI() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "close-icon-white"), target: self, action: #selector(touchCloseButton(_:)))
        townSortDisclosureItem.imageView.image = UIImage(named: "pin-icon-green")
        serviceSortDisclosureItem.imageView.image = UIImage(named: "list-icon-green")
        costSortDisclosureItem.imageView.image = UIImage(named: "wallet-icon-green")
        dateFilterDisclosureItem.imageView.image = UIImage(named: "calendar-green")
        durationFilterDisclosureItem.imageView.image = UIImage(named: "time-green")
    }
    
    
    private func configureRx() {
//КНОПКА ФИЛЬТРА ПО ГОРОДУ
        townSortDisclosureItem.button.rx.tap
            .subscribe{ [unowned self] _ in
                let indexOfChoisenItem = self.cityList.firstIndex(where: {$0.cityId == self.filters.city?.cityId})
                let array = indexOfChoisenItem != nil ? [indexOfChoisenItem!] : nil
                self.showPopupTable(items: self.cityList.map{ $0.cityName },
                                    multipleChoise: false,
                                    selectedCellsIndex: array,
                                    addHandler: { [unowned self] (oneCity) in
                                        
                                        if oneCity.count > 0 {
                                            self.filters.city = self.cityList.first { $0.cityName == oneCity[0]}
                                        }
                })
            }.disposed(by: disposeBag)
//КНОПКА ФИЛЬТРОВ СТОРИЗ
        serviceSortDisclosureItem.button.rx.tap
            .subscribe { [unowned self] _ in
                let array = self.filters.services?.map({ self.serviceList.firstIndex(of: ($0))!})
                
                self.showPopupTable(items: self.serviceList.map { $0.name },
                                    multipleChoise: true,
                                    selectedCellsIndex: array,
                                    addHandler: { (list) in
                                        
                                        let choisenServices = self.serviceList
                                            .filter({ (item) -> Bool in
                                                list.contains(item.name)
                                            })
                                        self.filters.services = choisenServices
                })
            }.disposed(by: disposeBag)
//КНОПКА ФИЛЬТРА ПО СТОИМОСТИ РАБОТ
        costSortDisclosureItem.button.rx.tap
            .subscribe{ [unowned self] _ in
                self.showFromToAlert(fromPlaceholder: "от", toPlaceholder: "до",
                                     title: "Выберите стоимость от и до", example: "Например: 1000 - 5000",
                                     onComplition: { (textFrom, textTo) in
                                        do {
                                            let fromValue: Int? = try self.parseTextToInt(textFrom)
                                            let toValue: Int? = try self.parseTextToInt(textTo)
                                            
                                            if fromValue != nil && toValue != nil && toValue! < fromValue! {
                                                self.errorParametrs("Первый параметр должен быть меньше либо равен второму")
                                                return
                                            }
                                            self.filters.priceFrom = fromValue
                                            self.filters.priceTo = toValue
                                        } catch { self.errorParametrs() }
                })
            }.disposed(by: disposeBag)
//КНОПКА ФИЛЬТРА ПО ДАТЕ
        dateFilterDisclosureItem.button.rx.tap
            .subscribe { [unowned self] _ in
                let alert = UIAlertController(title: "Выберите даты", message: "", preferredStyle: .alert)
                
                alert.addTextField(configurationHandler: { (textfield) in
                    textfield.placeholder = "От"
                    textfield.tag = 1
                    textfield.rx.controlEvent(.editingDidBegin)
                        .subscribe { [unowned alert, unowned self] _ in
                           self.showDatePicker(textfield, alert)
                        }.disposed(by: self.disposeBag)
                })
                
                alert.addTextField(configurationHandler: { (textfield) in
                    textfield.placeholder = "До"
                    textfield.tag = 2
                    textfield.rx.controlEvent(.editingDidBegin)
                        .subscribe { [unowned alert, unowned self] _ in
                            self.showDatePicker(textfield, alert)
                        }.disposed(by: self.disposeBag)
                })
                
                let actionOk = UIAlertAction(title: "Добавить", style: .default) { [unowned self] _ in
                    let fromValue = self.filters.dateFrom
                    let toValue = self.filters.dateTo
                    if fromValue != nil && toValue != nil &&
                        toValue! < fromValue! {
                        self.errorParametrs("Первый параметр должен быть меньше либо равен второму")
                        self.filters.dateFrom = nil
                        self.filters.dateTo = nil
                        return
                    }
                }
                let actionCancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
                alert.addAction(actionCancel)
                alert.addAction(actionOk)
                self.present(alert, animated: true, completion: nil)
                
            }.disposed(by: disposeBag)
        
//КНОПКА ФИЛЬТРА ДЛИТЕЛЬНОСТИ ЗАКАЗА
        durationFilterDisclosureItem.button.rx.tap
            .subscribe { [unowned self] _ in
                self.showFromToAlert(fromPlaceholder: "от", toPlaceholder: "до",
                                     title: "Выберите срок действия заказа", example: "От 1 до 182 дней",
                                     onComplition: { (fromText, toText) in
                                    do {
                                        let fromValue: Int? = try self.parseTextToInt(fromText)
                                        let toValue: Int? = try self.parseTextToInt(toText)
                                        
                                        if fromValue != nil && toValue != nil &&
                                            toValue! < fromValue! && toValue! > 182 {
                                             self.errorParametrs("Первый параметр должен быть меньше либо равен второму")
                                            return
                                        }
                                        self.filters.durationFrom = fromValue
                                        self.filters.durationTo = toValue
                                    } catch { self.errorParametrs() }
                                   
                })
            }.disposed(by: disposeBag)
    }
    
    @objc func touchCloseButton(_ sender: UIBarButtonItem) {
        navCon?.popViewController(animated: true)
    }
    
    ///Обработка входящего текста. Возвращает nil, если текст пустой. Ошибка, если невозможно преобразовать
    private func parseTextToInt(_ parametr: String) throws -> Int? {
        var value: Int? = nil
        if  parametr != ""{
            guard let temp = Int(parametr) else { throw ValidatorError("Невозможно пробразовать в Int") }
            value = temp
        }
        return value
    }
    
    ///Показывает алекрт с 2-мя текстфилдами для введения данных от какого-то значени и до какого-то
    private func showFromToAlert(fromPlaceholder: String, toPlaceholder: String,
                                 title: String, example: String,
                                 onComplition: @escaping ((String, String) -> Void)) {
        
        let alert = UIAlertController(title: title, message: example, preferredStyle: .alert)
        alert.addTextField { (textFrom) in
            textFrom.placeholder = fromPlaceholder
            textFrom.keyboardType = .decimalPad
        }
        alert.addTextField { (textTo) in
            textTo.placeholder = toPlaceholder
            textTo.keyboardType = .decimalPad
        }
        let actionOk = UIAlertAction(title: "Добавить", style: .default) { [unowned self] _ in
            guard let textFrom = alert.textFields?[0].text else { self.errorParametrs(); return }
            guard let textTo = alert.textFields?[1].text else { self.errorParametrs(); return }
            onComplition(textFrom.trimmingCharacters(in: .whitespaces), textTo.trimmingCharacters(in: .whitespaces))
        }
        let actionCancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(actionCancel)
        alert.addAction(actionOk)
        self.present(alert, animated: true, completion: nil)
    }
    
    ///Отображет alert.
    private func errorParametrs(_ message: String = "Ошибка при добавлении данных" ) {
        self.showWireframe(yesHandler: nil, noHandler: nil, title: "", description: message)
    }
    
    ///Сложная функция, добавляющая datepicker при нажатии на поле текст филда, который находится на UIAlertController
    private func showDatePicker(_ textfield: UITextField, _ alert: UIAlertController) {
    
        let datepicker = UIDatePicker()
        datepicker.datePickerMode = .date
        
        //Добавить тубар над дата пикером
        let toolbar = UIToolbar ();
        toolbar.sizeToFit ()
        //кнопки для Готово и Отмена для тулбара
        let doneButton = UIBarButtonItem (title: "Готово", style: .plain,
                                          target: nil, action: nil)
        doneButton.rx.tap
            .subscribe { [unowned alert] _ in
                let formatter = DateFormatter()
                formatter.dateFormat = "dd.MM.yyyy"
                textfield.text = formatter.string(from: datepicker.date)
                if textfield.tag == 1 {
                    self.filters.dateFrom = datepicker.date.timeIntervalSince1970.rounded(.down)
                } else {
                    self.filters.dateTo = datepicker.date.timeIntervalSince1970.rounded(.down)
                }
                alert.view.endEditing(true)
            }.disposed(by: self.disposeBag)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Отмена", style: .plain,
                                           target: nil, action: nil)
        cancelButton.rx.tap
            .subscribe { [unowned alert] _ in
                alert.view.endEditing(true)
            }.disposed(by: self.disposeBag)
        
        toolbar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        
        //Привязка пикера и тулбара к текстфилду
        textfield.inputView = datepicker
        textfield.inputAccessoryView = toolbar
    }
}
