
//
//  ExecutorOrdersViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 23.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ExecutorOrdersViewController: UIViewController, TabControllerSelectable, UITableViewDataSource, UITableViewDelegate {
    
    var items: Array<Any> = [NSObject(), NSObject(), NSObject(), NSObject(), NSObject(), NSObject(), NSObject()]
    
    let tableView = UITableView()
    
    let backgroundImageView = UIImageView()
    let backgroundTextView = UITextView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        
        ExecutorOrderTableViewCell.registerNib(for: tableView)
        
        let backgroundView = UIView()
        tableView.backgroundView = backgroundView
        backgroundView.addSubview(backgroundImageView)
        backgroundView.addSubview(backgroundTextView)
        backgroundImageView.contentMode = .scaleAspectFit
        backgroundTextView.textAlignment = .center
        backgroundTextView.textColor = .lightGrayText
        backgroundTextView.isUserInteractionEnabled = false
        
        tableView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0))
        backgroundImageView.autoAlignAxis(toSuperviewAxis: .vertical)
        backgroundImageView.autoPinEdge(toSuperviewEdge: .top, withInset: UIScreen.main.bounds.height * 0.2)
        backgroundImageView.autoSetDimensions(to: buttonImageLargeSize)
        backgroundTextView.autoAlignAxis(toSuperviewAxis: .vertical)
        backgroundTextView.autoPinEdge(.top, to: .bottom, of: backgroundImageView, withOffset: 20.0)
        backgroundTextView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = items.count
        if count == 0 {
            tableView.backgroundView?.isHidden = false
        } else {
            tableView.backgroundView?.isHidden = true
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ExecutorOrderTableViewCell.dequeue(tableView: tableView)
        return cell
    }
    
    func selectedBy(tabController: TabViewController) {
        
    }
}
