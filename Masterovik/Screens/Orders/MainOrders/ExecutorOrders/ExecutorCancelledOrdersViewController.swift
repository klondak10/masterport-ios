//
//  ExecutorCancelledOrdersViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 23.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ExecutorCancelledOrdersViewController: ExecutorOrdersViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Отменённые"
        backgroundImageView.image = UIImage(named: "tools-minus-icon-red")
        backgroundTextView.text = "У вас ещё нет отменённых заказов"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ExecutorOrderTableViewCell.dequeue(tableView: tableView)
        
        return cell
    }
}
