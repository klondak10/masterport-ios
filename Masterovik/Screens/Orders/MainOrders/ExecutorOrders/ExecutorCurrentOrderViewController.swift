//
//  ExecutorCurrentOrderViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 24.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ExecutorCurrentOrderViewController: ExecutorOrdersViewController, UITextViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Текущие"
        backgroundImageView.image = UIImage(named: "tools-icon-green")
        backgroundTextView.delegate = self
        backgroundTextView.linkTextAttributes = [.foregroundColor : UIColor.darkGreen]
        let advertisementsLink = NSAttributedString(string: "Объявления", attributes: [.link : NSURL(string: "someURL")])
        let attributedText = NSMutableAttributedString(string: "У вас еще нет заказов. Для того, чтобы найти новые заказы перейдите в ")
        attributedText.append(advertisementsLink)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        attributedText.addAttributes([.paragraphStyle : paragraphStyle], range: NSMakeRange(0, attributedText.string.count))
        backgroundTextView.attributedText = attributedText
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ExecutorOrderTableViewCell.dequeue(tableView: tableView)
        return cell
    }
}
