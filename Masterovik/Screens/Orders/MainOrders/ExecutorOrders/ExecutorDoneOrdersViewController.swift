//
//  ExecutorDoneOrdersViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 24.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ExecutorDoneOrdersViewController: ExecutorOrdersViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Выполненные"
        let image = UIImage(named: "tools-minus-icon-red")?.withRenderingMode(.alwaysTemplate)
        backgroundImageView.image = image
        backgroundImageView.tintColor = .darkGreen
        backgroundTextView.text = "У вас ещё нет выполненных заказов"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ExecutorOrderTableViewCell.dequeue(tableView: tableView)
        return cell
    }
}
