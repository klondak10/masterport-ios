//
//  ExecutorOrderTabViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 24.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ExecutorOrderTabViewController: TabViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Заказы"
        let sortButton = UIBarButtonItem(image: UIImage(named: "sort-icon-white"), target: self, action: #selector(touchSortButton(_:)))
        
        let sortSettingsButton = UIBarButtonItem(image: UIImage(named: "settings-icon-black"), target: self, action: #selector(touchSortSettingsButton(_:)), leftInset: 10.0)
        
        navigationItem.rightBarButtonItems = [sortSettingsButton, sortButton]
        
        let executorCurrentOrderController = ExecutorCurrentOrderViewController()
        let executorDoneOrdersController = ExecutorDoneOrdersViewController()
        let executorCancelledOrdersController = ExecutorCancelledOrdersViewController()
        
        setControllers([executorCurrentOrderController, executorDoneOrdersController, executorCancelledOrdersController])
        
        navCon?.navigationBar.isHidden = false
    }
    
    @objc func touchSortButton(_ sender: UIBarButtonItem) {
        let currentController = controller(at: currentTab) as! OrdersViewController
        currentController.items = []
        currentController.tableView.reloadData()
    }
    
    @objc func touchSortSettingsButton(_ sender: UIBarButtonItem) {
        pushVC(SortOrdersViewController.instanceFromStoryboard)
    }
}

