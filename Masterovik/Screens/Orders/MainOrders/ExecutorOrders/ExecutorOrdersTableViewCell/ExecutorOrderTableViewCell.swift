//
//  ExecutorOrderTableViewCell.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 24.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ExecutorOrderTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var orderNameLabel: UILabel! {
        didSet {
            orderNameLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var executorFullNameLabel: UILabel! {
        didSet {
            executorFullNameLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var remainingLabel: UILabel! {
        didSet {
            remainingLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var daysRemainingLabel: UILabel! {
        didSet {
            daysRemainingLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var executorSuggestionLabel: UILabel! {
        didSet {
            executorSuggestionLabel.font = FontHelper.font(type: .roboto, size: .small)
            executorSuggestionLabel.layer.cornerRadius = 10.0
        }
    }
    
    private func orderOnDeadline() {
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.warningRed.cgColor
    }
}
