//
//  DoneOrdersViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class DoneOrdersViewController: OrdersViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Выполненные"
        let image = UIImage(named: "tools-minus-icon-red")?.withRenderingMode(.alwaysTemplate)
        backgroundImageView.image = image
        backgroundImageView.tintColor = .darkGreen
        backgroundTextView.text = "У вас ещё нет выполненных заказов"
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = OrderTableViewCell.dequeue(tableView: tableView)
        cell.timeRemainingLabel.text = String(indexPath.row) + " Дней"
        cell.customerReactionHeightConstraint.constant = 0
        cell.customerReactionLabel.isHidden = true
        cell.customerReviewView.backgroundColor = .lightGreen
//        cell.customerReviewImageView.tintColor = .white
        cell.customerReviewImageView.image = UIImage(named: "text-icon-white")
        return cell
    }

}
