//
//  OrderClaimViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class OrderClaimViewController: BaseViewController {
    
    @IBOutlet weak var statusImageView: UIImageView!
    
    
    @IBOutlet weak var statusLabel: UILabel! {
        didSet {
            statusLabel.font = FontHelper.font(type: .roboto, size: .medium)
        }
    }
    
    
    @IBOutlet weak var customerNameLabel: UILabel! {
        didSet {
            statusLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var orderNameLabel: UILabel! {
        didSet {
            statusLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    
    @IBOutlet weak var mapImageView: UIImageView!
    
    @IBOutlet weak var townLabel: UILabel! {
        didSet {
            statusLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var shortDescriptionFieldView: LabeledFieldView!
    
    @IBOutlet weak var fullDescriptionView: LabeledDescriptionView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func sendMailButton(_ sender: Any) {
    }
    

    

}
