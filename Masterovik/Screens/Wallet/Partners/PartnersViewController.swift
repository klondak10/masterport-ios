//
//  PartnersViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class PartnersViewController: UIViewController, TabControllerSelectable, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var infoLabel: UILabel! {
        didSet {
            infoLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    
    @IBOutlet weak var inviteCodeLabel: UILabel! {
        didSet {
            inviteCodeLabel.font = FontHelper.font(type: .franklin, size: .large)
        }
    }
    
    @IBOutlet weak var myPartnersLabel: UILabel! {
        didSet {
            myPartnersLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            PartnersTableViewCell.registerNib(for: tableView)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inviteCodeLabel.text = UserCoreDataManager.shared.getUser()?.invite ?? "Ошибка инвайта"

    }
    
    func selectedBy(tabController: TabViewController) {
        tabController.navigationItem.rightBarButtonItem = nil
    }
    
    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0//1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = PartnersTableViewCell.dequeue(tableView: tableView)
        return cell
    }
}
