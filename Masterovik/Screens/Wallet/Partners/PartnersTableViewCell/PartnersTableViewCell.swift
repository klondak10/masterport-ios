//
//  PartnersTableViewCell.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/10/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class PartnersTableViewCell: BaseTableViewCell {

    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
 
    @IBOutlet weak var inviteDescriptionLabel: UILabel! {
        didSet {
            inviteDescriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    
    @IBOutlet weak var inviteDateLabel: UILabel! {
        didSet {
            inviteDateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var isActivatedLabel: UILabel! {
        didSet {
            isActivatedLabel.font = FontHelper.font(type: .roboto, size: .small)
        }
    }
    
    
}
