//
//  BalanceViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BalanceViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    // Бесполезная хуйня
    
    var items: [CreditCardModel] = []
    private var selectedItemsIndexes: Array<Int> = []
    private let viewModel = MyCardViewModel()
    private let balanceViewModel = BalanceViewModel()
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var payMethodLabel: UILabel! {
        didSet {
            payMethodLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var payMethodDropDown: DropDown! {
        didSet {
            payMethodDropDown.font = FontHelper.font(type: .franklin, size: .small)
            payMethodDropDown.optionArray = ["Кредитная карта"]
            payMethodDropDown.selectedIndex = 0
            payMethodDropDown.setRoundedRect()
            let methods = payMethodDropDown.optionArray
            payMethodDropDown.text = methods[payMethodDropDown.selectedIndex!]
        }
    }
    
    @IBOutlet weak var cardsLabel: UILabel! {
        didSet {
            cardsLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var tableView: AutoHeightTableView! {
        didSet {
            BalanceTableViewCell.registerNib(for: tableView)
        }
    }
    
    @IBOutlet weak var addButtonView: AddButtonView!
    
    @IBOutlet weak var sumField: LabeledFieldView! {
        didSet {
            sumField.validateType = .balance
        }
    }
    
    @IBOutlet weak var nextButton: NextButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.bringSubviewToFront(payMethodDropDown)
        if items.count == 0 {
            viewModel.getUserCards()
        }
        
        configureRx()
    }
    
    private func configureRx() {
        
        viewModel.onCardReceive
            .subscribe(onNext: { [unowned self] (list) in
                self.items = list
            }).disposed(by: disposeBag)
        
        //Добавление карты
        addButtonView.button.rx.tap
            .observeOn(MainScheduler.instance)
            .subscribe { [unowned self] _ in
                self.alertForAddingCard()
            }.disposed(by: disposeBag)
        
        //Событие удачного добавления карты
        viewModel.onCardAddedSuccess
            .subscribe(onNext: { [unowned self] (event) in
                if !event {
                    self.showWireframe(yesHandler: nil, noHandler: nil, title: "Ошибка", description: "Карта не была добавлена!")
                } else {
                    self.viewModel.getUserCards()
                }
            }).disposed(by: disposeBag)
        
        nextButton.rx.tap
            .subscribe { [unowned self] (_) in
                do {
                    _ = try self.sumField.validateText()
                    if let id = self.tableView.visibleCells
                        .firstIndex(where: { (cell) -> Bool in
                            cell.isSelected == true
                        }){
                        self.balanceViewModel.increaseBalance(cardId:
                            self.items[id].id, summary: Double(self.sumField.textField.text!)!)
                    }
                    
                } catch {
                    self.sumField.textField.text = ""
                    self.sumField.textField.placeholder = "xxxxx,xx"
                }
        }.disposed(by: disposeBag)
        
        balanceViewModel.onCardReceive
            .subscribe(onNext: { [unowned self] (result) in
            if result {
                self.navigationController?.popViewController(animated: true)
                return
            } else {
                self.showWireframe(yesHandler: nil, noHandler: nil, title: "Ошибка", description: "Средства не были добавлены.")
            }
        }).disposed(by: disposeBag)
        
        sumField.textField.rx.controlEvent([.editingDidEndOnExit]).subscribe { [unowned self] _ in
            self.view.endEditing(true)
            }.disposed(by: disposeBag)
    }
    
    private func alertForAddingCard() {
        let alert = UIAlertController(title: "Добавление карты", message: "Введите номер", preferredStyle: .alert)
        alert.addTextField { [unowned self] (textField) in
            textField.keyboardType = .numberPad
            textField.rx.text.orEmpty
                .subscribe(onNext: { (text) in
                    if text.count == 4 || text.count == 9 || text.count == 14 {
                        textField.text?.append(" ")
                    }
                    if text.count > 19 {
                        textField.text?.removeLast()
                    }
                }).disposed(by: self.disposeBag)
        }
        
        let actionYes = UIAlertAction(title: "Добавить", style: .default) { [unowned self] (_) in
            if alert.textFields![0].text!.count == 19 {
                self.viewModel.addCard(number: alert.textFields![0].text!)
            } else {
                alert.view.shakeAnimation()
            }
        }
        let actionNo = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alert.addAction(actionYes)
        alert.addAction(actionNo)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BalanceTableViewCell.dequeue(tableView: tableView)
        if selectedItemsIndexes.contains(indexPath.row) {
            cell.selectImageView.image = UIImage(named: "check-green")
        } else {
            cell.selectImageView.image = UIImage(named: "check-gray")
        }
        cell.configure(items[indexPath.row])
        return cell
    }
}
