//
//  BalanceTableViewCell.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BalanceTableViewCell: BaseTableViewCell {

    @IBOutlet weak var selectImageView: UIImageView!
    
    @IBOutlet weak var cardImageView: UIImageView!
    
    
    @IBOutlet weak var cardNumberLabel: UILabel! {
        didSet {
            cardNumberLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            selectImageView.image = UIImage(named: "check-green")
        } else {
            selectImageView.image = UIImage(named: "check-gray")
        }
    }
    
    func configure(_ card: CreditCardModel) {
        self.cardNumberLabel.text = (card.card.prefix(4) + " **** **** " + card.card.suffix(4))
        cardImageView.image = card.card.first == "5" ? UIImage(named: "mastercard-card-icon") :
            UIImage(named: "visa-card-icon")
    }
}
