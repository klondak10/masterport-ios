//
//  BalanceViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya

class BalanceViewModel {
    
    //Fields
    private let increaseUserBalance = PublishSubject<Bool>()
    
    private let provider: myRxProvider<WalletRequestAPI>
    private let disposeBag = DisposeBag()
    
    init() {
        provider = myRxProvider<WalletRequestAPI>()
    }
    
    //Properties
    var onCardReceive : Observable<Bool> {
        return increaseUserBalance
            .asObservable()
    }
    
    //Mark: Methods
    func increaseBalance(cardId: Int, summary: Double) {
        provider.request(.addMoney(cardID: cardId, summary: summary))
            .subscribe { [weak self] (response) in
                switch response {
                case .success(let result):
                    if result.statusCode == 200 {
                        self?.increaseUserBalance.onNext(true)
                        let oldBalance = UserCoreDataManager.shared.getUser()!.balance
                        UserCoreDataManager.shared.updateUserBalance(on: Double(oldBalance) + summary)
                    } else {
                        self?.increaseUserBalance.onNext(false)
                        
                    }
                case .error:
                    self?.increaseUserBalance.onNext(false)
                }
            }.disposed(by: disposeBag)
    }
}
