//
//  MyCardsViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MyCardsViewController: UIViewController, TabControllerSelectable {
    
    
    @IBOutlet weak var helloLabel: UILabel!
    
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var myCardsLabel: UILabel!
    
    @IBOutlet weak var tableView: AutoHeightTableView! {
        didSet {
            CardTableViewCell.registerNib(for: tableView)
        }
    }
    
    @IBOutlet weak var addCardButtonView: AddButtonView!
    
    private var creditCards: [CreditCardModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    private let viewModel = MyCardViewModel()
    private let disposeBag = DisposeBag()
   
    private let historyViewModel = HistoryWalletViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureRx()
    }
    
    private func configureUI() {
        helloLabel.text = "Добрый день, \(UserCoreDataManager.shared.getUser()?.firstName ?? "")!"
        balanceLabel.text = "На вашем счету \(UserCoreDataManager.shared.getUser()?.balance ?? 0) рублей"
        navigationItem.hidesBackButton = true
    }
    
    private func configureRx() {
        //Получение списка карт
        viewModel.getUserCards()
        historyViewModel.getHistory()
        
        viewModel.onCardReceive
            .subscribe(onNext: { [unowned self] (list) in
                self.creditCards = list
            }).disposed(by: disposeBag)
        
        //Добавление карты
        addCardButtonView.button.rx.tap
            .subscribe { [unowned self] _ in
                self.alertForAddingCard()
            }.disposed(by: disposeBag)
        
        //Событие удачного добавления карты
        viewModel.onCardAddedSuccess
            .subscribe(onNext: { [unowned self] (event) in
                if !event {
                    self.showWireframe(yesHandler: nil, noHandler: nil, title: "Ошибка", description: "Карта не была добавлена!")
                } else {
                   self.viewModel.getUserCards()
                }
            }).disposed(by: disposeBag)
        
        //Событие удачного редактирования карты
        viewModel.onCardEditSuccess
            .subscribe(onNext: { [unowned self] (event) in
                if !event {
                    self.showWireframe(yesHandler: nil, noHandler: nil, title: "Ошибка", description: "Карта не была редактирована!")
                } else {
                    self.viewModel.getUserCards()
                }
            }).disposed(by: disposeBag)
        
        //Событие удачного удаления карты
        viewModel.onCardRemoveSuccess
            .subscribe(onNext: { [unowned self] (event) in
                if !event {
                    self.showWireframe(yesHandler: nil, noHandler: nil, title: "Ошибка", description: "Карта не была удалена!")
                } else {
                    self.viewModel.getUserCards()
                }
            }).disposed(by: disposeBag)
    }
    
    
    private func alertForAddingCard(for number: String = "", id: Int = -1) {
        let alert = UIAlertController(title: "Добавление карты", message: "Введите номер", preferredStyle: .alert)
        alert.addTextField { [unowned self] (textField) in
            textField.keyboardType = .numberPad
            textField.text = number
            textField.rx.text.orEmpty
                .subscribe(onNext: { (text) in
                    if text.count == 4 || text.count == 9 || text.count == 14 {
                        textField.text?.append(" ")
                    }
                    if text.count > 19 {
                        textField.text?.removeLast()
                    }
                }).disposed(by: self.disposeBag)
        }
        
        let actionYes = UIAlertAction(title: "Добавить", style: .default) { [unowned self] (_) in
            if alert.textFields![0].text!.count == 19 {
                if number == "" {
                    self.viewModel.addCard(number: alert.textFields![0].text!)
                } else {
                    self.viewModel.editCard(cardId: id, newNumber: alert.textFields![0].text!)
                }
            } else {
                alert.view.shakeAnimation()
            }
        }
        let actionNo = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alert.addAction(actionYes)
        alert.addAction(actionNo)
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectedBy(tabController: TabViewController) {
        tabController.navigationItem.rightBarButtonItem = nil
    }
    
   
    
    @IBAction func touchDepositButton(_ sender: Any) {
        let balanceVC = BalanceViewController.instanceFromStoryboard as! BalanceViewController
        balanceVC.items = creditCards
        pushVC(balanceVC)
    }
}

extension MyCardsViewController: UITableViewDataSource, UITableViewDelegate {
    
    private func subscribeOnEditCardFor(_ cell: CardTableViewCell) {
        cell.editCardOn
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (cardId) in
                self.alertForAddingCard(for: self.creditCards.first { $0.id == cardId}!.card, id: cardId)
            }).disposed(by: disposeBag)
    }
    
    private func subscribeOnRemoveCardFor(_ cell: CardTableViewCell) {
        
        cell.deleteCardOn
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (cardId) in
                
                self.showWireframe(yesHandler: {
                     self.viewModel.removeCard(cardId: cardId)
                }, noHandler: nil, title: "Внимание", description: "Вы уверены, что хотите удалить карту?")
                cell.buttonsToEditView.isHidden = true
                cell.shadowCardView.isHidden = false
               
            }).disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return creditCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CardTableViewCell.dequeue(tableView: tableView)
        
        cell.setCardForCell(creditCards[indexPath.row])
        subscribeOnEditCardFor(cell)
        subscribeOnRemoveCardFor(cell)
        return cell
    }
}
