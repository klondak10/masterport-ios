//
//  CardTableViewCell.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CardTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var cardImageView: UIImageView!
    
    @IBOutlet weak var cardNumberText: UITextView! {
        didSet {
            cardNumberText.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var goBackButton: UIButton!
    
    @IBOutlet weak var shadowCardView: ShadowView!
    @IBOutlet weak var buttonsToEditView: ShadowView!
    
    let editCardOn = PublishSubject<Int>()
    let deleteCardOn = PublishSubject<Int>()
    
    private var cardModel: CreditCardModel?
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        
        deleteButton.layer.cornerRadius = 3
        editButton.layer.cornerRadius = 3
        
        deleteButton.layer.borderWidth = 2
        editButton.layer.borderWidth = 2
        
        deleteButton.layer.borderColor = UIColor.darkGreen.cgColor
        editButton.layer.borderColor = UIColor.darkGreen.cgColor
        
        configureRx()
    }
    
    private func configureRx() {
        
        optionsButton.rx.tap
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] (_) in
                guard let `self` = self else { return }
                
                UIView.transition(with: self, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                    self.buttonsToEditView.isHidden = false
                    self.shadowCardView.isHidden = true
                }, completion: nil)
                
            }.disposed(by: disposeBag)
        
        goBackButton.rx.tap
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] (_) in
                guard let `self` = self else { return }
                
                UIView.transition(with: self, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                    self.buttonsToEditView.isHidden = true
                    self.shadowCardView.isHidden = false
                }, completion: nil)
                
            }.disposed(by: disposeBag)
        
        editButton.rx.tap
            .subscribe { [weak self] (_) in
                guard let `self` = self else { return }
                self.editCardOn.onNext(self.cardModel!.id)
        }.disposed(by: disposeBag)
        
        deleteButton.rx.tap
            .subscribe { [weak self] (_) in
                guard let `self` = self else { return }
                self.deleteCardOn.onNext(self.cardModel!.id)
            }.disposed(by: disposeBag)
    }
    
    
    func setCardForCell(_ card: CreditCardModel){

        self.cardModel = card
        self.cardNumberText.text = (card.card.prefix(4) + " **** **** " + card.card.suffix(4))
        cardImageView.image = card.card.first == "5" ? UIImage(named: "mastercard-card-icon") :
                                                        UIImage(named: "visa-card-icon")
    }
}
