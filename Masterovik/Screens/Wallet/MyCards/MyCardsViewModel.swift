//
//  MyCardsViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/15/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya
import SwiftyJSON

class MyCardViewModel {
        
    //Fields
    private let cardsWasRecieved = PublishSubject<[CreditCardModel]>()
    private let cardAddedSuccess = PublishSubject<Bool>()
    private let cardEditSuccess = PublishSubject<Bool>()
    private let cardRemoveSuccess = PublishSubject<Bool>()
    
    private let provider: myRxProvider<WalletRequestAPI>
    private let disposeBag = DisposeBag()
    
    init() {
        provider = myRxProvider<WalletRequestAPI>()
    }
    
    //Properties
    var onCardReceive : Observable<[CreditCardModel]> {
        return cardsWasRecieved
            .asObservable()
    }
    
    var onCardAddedSuccess: Observable<Bool> {
        return cardAddedSuccess
            .asObservable()
    }
    
    var onCardEditSuccess: Observable<Bool> {
        return cardEditSuccess
            .asObservable()
    }
    
    var onCardRemoveSuccess: Observable<Bool> {
        return cardRemoveSuccess
            .asObservable()
    }
    
    //Mark: Methods
    func getUserCards() {
        provider.request(.getCardList)
            .subscribe { [weak self] (response) in
                switch response {
                case .success(let result):
                    do {
                    let json = try JSON(data: result.data)
                    let list = json["response"].arrayValue.map { CreditCardModel(from: $0) }
                    self?.cardsWasRecieved.onNext(list)
                    } catch { self?.cardsWasRecieved.onNext([])}
                case .error:
                    self?.cardsWasRecieved.onNext([])
                }
        }.disposed(by: disposeBag)
    }
    
    func addCard(number: String) {
        provider.request(.addNewCard(number: number))
            .subscribe { [weak self] (response) in
                switch response {
                case .success(let result):
                    if result.statusCode == 200 {
                        self?.cardAddedSuccess.onNext(true)
                    } else {
                        self?.cardAddedSuccess.onNext(false)
                        
                    }
                case .error:
                    self?.cardAddedSuccess.onNext(false)
                }
        }.disposed(by: disposeBag)
    }
    
    func editCard(cardId: Int, newNumber: String) {
        provider.request(.editCard(cardID: cardId, number: newNumber))
            .subscribe { [weak self] (response) in
                switch response {
                case .success(let result):
                    if result.statusCode == 200 {
                        self?.cardEditSuccess.onNext(true)
                    } else {
                        self?.cardEditSuccess.onNext(false)
                        
                    }
                case .error:
                    self?.cardEditSuccess.onNext(false)
                }
            }.disposed(by: disposeBag)
    }
    
    func removeCard(cardId: Int) {
        provider.request(.removeCard(cardID: cardId))
            .subscribe { [weak self] (response) in
                switch response {
                case .success(let result):
                    if result.statusCode == 200 {
                        self?.cardRemoveSuccess.onNext(true)
                    } else {
                        self?.cardRemoveSuccess.onNext(false)
                        
                    }
                case .error:
                    self?.cardRemoveSuccess.onNext(false)
                }
            }.disposed(by: disposeBag)
    }
}
