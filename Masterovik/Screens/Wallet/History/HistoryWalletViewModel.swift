//
//  HistoryWalletViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya
import SwiftyJSON

class HistoryWalletViewModel {
    
    private static let operarionsType = ["Пополнение баланса", "Списание с внутреннего баланса"]
    
    private let historyWasRecieved = PublishSubject<[HistoryWalletModel]>()
    
    private let provider: myRxProvider<WalletRequestAPI>
    private let disposeBag = DisposeBag()
    
    init() {
        provider = myRxProvider<WalletRequestAPI>()
    }
    
    //Properties
    var onCardReceive : Observable<[HistoryWalletModel]> {
        return historyWasRecieved
            .asObservable()
    }
    
    ///ascend: true = поиск от новых операций к старым.
    ///Если forOperation: = nil, значит поиск не требуется, true = пополнение баланса, false = поиск на списание.
    ///forCard: Поиск для конкретной карты.
    func getHistory(ascend: Bool = true, forOperation: Bool? = nil, forCard: Int? = nil) {
        
        var operation: String? = nil
        if let needOperation = forOperation {
            operation = needOperation ? HistoryWalletViewModel.operarionsType[0] :
                                        HistoryWalletViewModel.operarionsType[1]
        }
        provider.request(.paymentHistory(ascend: ascend, forOperation: operation, forCard: forCard))
            .subscribe { [weak self] (response) in
                guard let `self` = self else { return }
                switch response {
                case .success(let result):
                    do {
                        let json = try JSON(data: result.data)
                        let error = json["error_message"].stringValue
                        if error != "" {
                            print(error)
                            self.historyWasRecieved.onNext([])
                            return
                        }
                        
                        let list = json["response"].arrayValue.map { HistoryWalletModel(from: $0)}
                        self.historyWasRecieved.onNext(list)
                    } catch (let error){
                        print(error.localizedDescription)
                        print("Ошибка в блоке do-catch")
                        self.historyWasRecieved.onNext([])
                    }
                    
                case .error(let error):
                    print(error.localizedDescription)
                    self.historyWasRecieved.onNext([])
                }
        }.disposed(by: disposeBag)
    }
    
}
