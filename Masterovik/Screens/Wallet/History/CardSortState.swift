//
//  CardSortState.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

class CardSortState {
    
    var ascend: Bool = true
    
    var operation: Bool? = nil
    
    var forCard: Int? = nil
    
    var cardList: [CreditCardModel] = []
}
