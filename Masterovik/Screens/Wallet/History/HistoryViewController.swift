//
//  HistoryViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 5/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, TabControllerSelectable {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            HistoryTableViewCell.registerNib(for: tableView)
        }
    }
    
    private let emptyListLabel: UILabel = {
        let label = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: 200, height: 70)))
        label.text = "Ваша история пока что пуста 😔"
        label.textAlignment = NSTextAlignment.center
        label.adjustsFontSizeToFitWidth = true
        label.isHidden = false
        return label
    }()
    
    private var sortState = CardSortState() {
        didSet {
            self.historyViewModel.getHistory(ascend: sortState.ascend,
                                             forOperation: sortState.operation,
                                             forCard: sortState.forCard)
        }
    }
    
    private let historyViewModel = HistoryWalletViewModel()
    private let cardViewModel = MyCardViewModel()
    private let disposeBag = DisposeBag()
    
    private var historyResult: [HistoryWalletModel] = [] {
        didSet {
            tableView.reloadData()
            if historyResult.count == 0 {
                emptyListLabel.isHidden = false
            } else {
                emptyListLabel.isHidden = true
            }
        }
    }
    private var cardList: [CreditCardModel] = [] {
        didSet {
            self.sortState.cardList = cardList
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(emptyListLabel)
    
    }
    
    private func configureRx() {
        self.historyViewModel.getHistory()
        self.cardViewModel.getUserCards()
        
        self.historyViewModel.onCardReceive
            .bind(onNext: { [weak self] in self?.historyResult = $0 })
            .disposed(by: disposeBag)
        self.cardViewModel.onCardReceive
            .bind(onNext: { [weak self] in self?.cardList = $0 })
            .disposed(by: disposeBag)
        
        
    }
    
    func selectedBy(tabController: TabViewController) {
        tabController.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "sort-icon-white"), target: self, action: #selector(touchSortButton(_:)))
        
        self.emptyListLabel.center = self.view.center
        if historyResult.count == 0 {
            configureRx()
        }
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HistoryTableViewCell.dequeue(tableView: tableView)
        cell.translatesAutoresizingMaskIntoConstraints = false
        cell.configure(for: historyResult[indexPath.row])
        if let cardNumber = cardList
            .first(where: { $0.id == historyResult[indexPath.row].cardId })?.card {
            cell.cardNameLabel.text = cardNumber
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    @objc func touchSortButton(_ sender: UIBarButtonItem)
    {        
        let sortVC = storyboard?.instantiateViewController(withIdentifier: "HistorySorting") as! SortWalletHistoryVC
        sortVC.sortState = self.sortState
        sortVC.delegate = self
        navigationController?.pushViewController(sortVC, animated: true)
    }
}

extension HistoryViewController: SortStateProtocol {
    func changeSortState(_ state: CardSortState) {
        self.sortState = state
    }
}
