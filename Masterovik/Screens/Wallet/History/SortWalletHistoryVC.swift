//
//  SortWalletHistoryVC.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol SortStateProtocol {
    func changeSortState(_ state: CardSortState)
}

class SortWalletHistoryVC: BaseViewController {

    @IBOutlet weak var toAscendButton: UIButton!
    
    @IBOutlet weak var onCardSort: UIButton!
    
    @IBOutlet weak var toDescendButton: UIButton!
    
    @IBOutlet weak var forAddMoney: UIButton!
    
    @IBOutlet weak var forDeleteMoney: UIButton!
    
    @IBOutlet var ascendImageCollection: [UIImageView]!
    
    @IBOutlet var forMoneyWorkingCollection: [UIImageView]!
    
    var ascendSorting = true {
        didSet {
            sortState.ascend = ascendSorting
        }
    }
    var operation: Bool? = nil {
        didSet {
            sortState.operation = operation
        }
    }
    
    var sortState: CardSortState = CardSortState()
    private let disposeBag = DisposeBag()
    
    var delegate: SortStateProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "close-icon-white"),
                                                       target: self,
                                                       action: #selector(touchCloseButton(_:)))
        if sortState.ascend == true {
            self.ascRadioButtonHandler()
        } else { self.descRadioButtonHandler()}
        
        if let operation = sortState.operation {
            if operation {
                onAddMoney()
            } else {
                onDeleteMoney()
            }
        }
        configureRx()
    }
    
    private func configureRx() {
        disposeBag.insert(
            
        toAscendButton.rx.tap
            .subscribe { [weak self] (_) in self?.ascRadioButtonHandler() },
        toDescendButton.rx.tap
            .subscribe{ [weak self] (_) in self?.descRadioButtonHandler() },
        forAddMoney.rx.tap
            .subscribe { [weak self] (_) in self?.onAddMoney() },
        forDeleteMoney.rx.tap
            .subscribe{ [weak self] (_) in
                self?.onDeleteMoney() },
            
        onCardSort.rx.tap
            .observeOn(MainScheduler.instance)
            .subscribe({ [unowned self] (_) in
                self.showPopupTable(items: self.sortState.cardList.map { $0.card },
                                    multipleChoise: false,
                        addHandler: { (list) in
                            self.sortState.forCard = self.sortState.cardList
                                .first(where: { $0.card == list[0] })?.id
                })
            })
        )
    }
    
    
    private func ascRadioButtonHandler() {
        ascendImageCollection[0].image = #imageLiteral(resourceName: "check-green")
        ascendImageCollection[1].image = #imageLiteral(resourceName: "check-gray")
        ascendSorting = true
    }
    
    private func descRadioButtonHandler() {
        ascendImageCollection[0].image = #imageLiteral(resourceName: "check-gray")
        ascendImageCollection[1].image = #imageLiteral(resourceName: "check-green")
        ascendSorting = false
    }
    
    private func onAddMoney() {
        if operation == true {
            operation = nil
            forMoneyWorkingCollection.forEach { $0.image = #imageLiteral(resourceName: "check-gray") }
        } else {
            operation = true
            forMoneyWorkingCollection[0].image = #imageLiteral(resourceName: "check-green")
            forMoneyWorkingCollection[1].image = #imageLiteral(resourceName: "check-gray")
        }
    }
    
    private func onDeleteMoney() {
        if operation == false {
            operation = nil
            forMoneyWorkingCollection.forEach { $0.image = #imageLiteral(resourceName: "check-gray") }
        } else {
            operation = false
            forMoneyWorkingCollection[0].image =  #imageLiteral(resourceName: "check-gray")
            forMoneyWorkingCollection[1].image =  #imageLiteral(resourceName: "check-green")
        }
    }
    
    
    @objc func touchCloseButton(_ sender: UIBarButtonItem) {
        delegate?.changeSortState(self.sortState)
        navCon?.popViewController(animated: true)
    }
}
