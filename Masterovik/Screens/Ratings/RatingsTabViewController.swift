//
//  RatingsTabViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 04.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class RatingsTabViewController: TabViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Мой рейтинг"
        let sortSettingsButton = UIBarButtonItem(image: UIImage(named: "settings-icon-black"), target: self, action: #selector(touchSortSettingsButton(_:)), leftInset: 10.0)
        
        navigationItem.rightBarButtonItems = [sortSettingsButton]
        
  //      let ratingVC = RatingViewController.instanceFromStoryboard
        let reviewsVC = ReviewsViewController.instanceFromStoryboard
        setControllers([reviewsVC])
        
        navCon?.navigationBar.isHidden = false
        navCon?.viewControllers = [self]
    }
    
    @objc func touchSortSettingsButton(_ sender: UIBarButtonItem) {
        pushVC(SortOrdersViewController.instanceFromStoryboard)
    }
}
