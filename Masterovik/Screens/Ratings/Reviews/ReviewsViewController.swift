//
//  ReviewsViewController.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 04.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ReviewsViewController: UIViewController, TabControllerSelectable, UITableViewDataSource, UITableViewDelegate {
    
    var items: Array<Any> = [NSObject(), NSObject(), NSObject(), NSObject()]

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            ReviewsTableViewCell.registerNib(for: tableView)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func selectedBy(tabController: TabViewController) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ReviewsTableViewCell.dequeue(tableView: tableView)
        return cell
    }
}
