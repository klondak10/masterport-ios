//
//  ReviewsTableViewCell.swift
//  Masterovik
//
//  Created by Серафим Ковальчук on 04.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ReviewsTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var customerNameLabel: UILabel! {
        didSet {
            customerNameLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var ratingLabel: UILabel! {
        didSet {
            ratingLabel.font = FontHelper.font(type: .myriad, size: .title)
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
}
