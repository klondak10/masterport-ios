//
//  Wireframe.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/6/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class Wireframe: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var yesButton: NextButton!
    @IBOutlet weak var noButton: NextButton!
    @IBOutlet weak var okBotton: NextButton!
    
    var yesButtonHandler: (() -> Void)?
    var noButtonHandler: (() -> Void)?
    var okButtonHandler: (() -> Void)?
    
    @IBAction func yesButtonCtion(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
         yesButtonHandler?()
    }
    
    
    @IBAction func noButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
         noButtonHandler?()
    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        okButtonHandler?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if yesButtonHandler == nil && noButtonHandler == nil {
            okBotton.isHidden = false
            yesButton.isHidden = true
            noButton.isHidden = true
        }
        animateView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
    }

    
    private func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
}
