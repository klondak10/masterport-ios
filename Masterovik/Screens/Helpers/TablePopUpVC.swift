//
//  TablePopUpVC.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/14/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class TablePopUpVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            tableView.rowHeight = self.rowHeiht
        }
    }
    
    
    private let rowHeiht:CGFloat = 50
    private var selectedItems: [Int] = []
    private var items: [String] = []
    private var outPutItems: [String] = []
    
    var action: (([String]) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.layer.cornerRadius = 10
        let maxHeight: CGFloat = 350
        let itemsCount = CGFloat(exactly: items.count)!
        let tableWidth = UIScreen.main.bounds.width - 80
        let tableHeight = rowHeiht*itemsCount > maxHeight ? maxHeight: rowHeiht*itemsCount
        tableView.frame = CGRect(origin: .zero,
                                 size: CGSize(width: tableWidth, height: tableHeight))

        animateView()
        view.layoutIfNeeded()
    }
    @IBAction func addButtonAction(_ sender: Any) {
        action?(outPutItems)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        selectedItems.forEach {
            let indexPath = IndexPath(row: $0, section: 0)
            self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            self.tableView(self.tableView, didSelectRowAt: indexPath)
        }
        super.viewDidAppear(animated)
    }
    
    private func animateView() {
        tableView.alpha = 0;
        self.tableView.center = CGPoint(x: view.center.x, y: view.center.y + 50)
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.tableView.alpha = 1.0;
            self.tableView.center = self.view.center
        })
    }
    
    func setItems(_ list:[String]){
        self.items = list
    }
    
    func setSelectedItems(_ indexes: [Int]) {
        self.selectedItems = indexes
    }
}

extension TablePopUpVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = items[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            if let text = cell.textLabel?.text {
                self.outPutItems.append(text)
            }
            cell.accessoryType = .checkmark
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            if let text = cell.textLabel?.text {
                self.outPutItems.removeAll { $0 == text }
            }
            cell.accessoryType = .none
        }
    }
}

