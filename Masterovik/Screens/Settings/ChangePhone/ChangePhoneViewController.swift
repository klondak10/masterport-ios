//
//  ChangePhoneViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChangePhoneViewController: BaseViewController, RoundTimerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var phoneLabeledField: LabeledFieldView! {
        didSet {
            phoneLabeledField.validateType = .phoneNumber
            phoneLabeledField.isPhoneNumberTF = true
        }
    }
    @IBOutlet weak var smsCodeLabeledField: LabeledFieldView! {
        didSet {
            smsCodeLabeledField.validateType = .sms
        }
    }
    @IBAction func tapOnSuperview(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBOutlet weak var getCodeButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var timerView: RoundTimerView!
    
    private let viewModel = SignInViewModel()
    private let disposeBag = DisposeBag()
    private var isRegister = true {
        didSet {
            if !isRegister {
                self.showWireframe(yesHandler: nil, noHandler: nil, title: "Внимание!",
                                   description: "Номер существует, проверьте данные")
            }
        }
    }
    private let sendCodeAgain = "Выслать код повторно"
    private let sendCodeFirst = "Получить код"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureRx()
        timerView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func configureUI() {
        
        getCodeButton.layer.borderColor = UIColor.darkGreen.cgColor
        getCodeButton.layer.borderWidth = 1.0
        if #available(iOS 12.0, *) {
            smsCodeLabeledField.textField.textContentType = .oneTimeCode
        }
        topViewHeightConstraint?.isActive = false
        topGradientView.autoPinEdge(toSuperviewEdge: .bottom)
        phoneLabeledField.textField.keyboardType = .phonePad
        
        phoneLabeledField.label.textColor = UIColor(white: 0.86, alpha: 1)
        smsCodeLabeledField.label.textColor = UIColor(white: 0.86, alpha: 1)
    }
    
    private func configureRx() {
        
        ///ViewModel Subscribes
        
        viewModel.onChangePhoneFirstStep
            .bind { [weak self] (next) in
                self?.isRegister = !next
            }.disposed(by: disposeBag)
        
        
        viewModel.onChangePhoneLastStep
            .subscribeOn(MainScheduler.instance)
            .subscribe { [weak self] (event) in
                guard let result = event.element, let `self` = self else { return }
                if !result {
                    self.showWireframe(yesHandler: nil, noHandler: nil,
                                       title: "Неправильный код", description: "Пожалуйста, попробуйте снова")
                } else {
                    UserCoreDataManager.shared.updateUserNumber(on: self.phoneLabeledField.textField.text!)
                    self.navigationController?.popViewController(animated: true)
                }
            }.disposed(by: disposeBag)
        
        
        /// TextFields Subscribes
        
        phoneLabeledField.isValidText.subscribe(onNext: {  [weak self] (value) in
            guard let`self` = self else { return }
            
            if self.getCodeButton.isUserInteractionEnabled == false {
                if value {
                    self.getCodeButton.setTitle(self.sendCodeFirst, for: .normal)
                    self.timerEnded()
                }
            }
            self.getCodeButton.isUserInteractionEnabled = value
            self.nextButton.isUserInteractionEnabled = false
        }).disposed(by: disposeBag)
        
        
        /// Buttons Subscribes
        
        getCodeButton.rx.tap.subscribe { [weak self] (_) in
            guard let `self` = self else { return }
            self.viewModel.changingPhoneGetSms(on: self.getPhoneNumber())
            self.getCodeButton.setTitle(self.sendCodeAgain, for: .normal)
            self.smsCodeLabeledField.isUserInteractionEnabled = true
            self.timerView.elapsedTime = 0
            self.timerView.startTimer()
            self.timerView.isHidden = false
            self.getCodeButton.isUserInteractionEnabled = false
            self.nextButton.isUserInteractionEnabled = true
            self.smsCodeLabeledField.textField.becomeFirstResponder()
            
            }.disposed(by: disposeBag)
        
        nextButton.rx.tap.subscribe { [weak self] (_) in
            guard let `self` = self else { return }
            self.viewModel.changingPhoneCheckSms(on: self.getPhoneNumber(),
                                                 with: self.smsCodeLabeledField.textField.text ?? "")
            }.disposed(by: disposeBag)
    }
    
    
    
    private func getPhoneNumber() -> String {
        if let text = phoneLabeledField.textField.text {
            let unsafeChars = CharacterSet.alphanumerics.inverted
            var cleanChars  = text.components(separatedBy: unsafeChars).joined(separator: "")
            cleanChars = "+" + cleanChars
            return cleanChars
        } else {
            return "+"
        }
    }
    
    // MARK: - Timer delegate method
    
    func timerEnded() {
        timerView.isHidden = true
        self.getCodeButton.isUserInteractionEnabled = true
        self.view.endEditing(true )
    }
}
