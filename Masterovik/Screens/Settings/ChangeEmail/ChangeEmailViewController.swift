//
//  ChangeEmailViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChangeEmailViewController: BaseViewController {
    
    @IBOutlet weak var saveButton: NextButton!
    @IBOutlet weak var emailField: LabeledFieldView!
    @IBOutlet weak var confirmEmailField: LabeledFieldView!
    
    private let viewModel = ChangeEmailViewModel()
    private let disposeBag = DisposeBag()
    
    private let emailValid = BehaviorRelay<Bool>(value: false)
    private let confirmValid = BehaviorRelay<Bool>(value: false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailField.textField.keyboardType = .emailAddress
        confirmEmailField.textField.keyboardType = .emailAddress
        
        emailField.validateType = .email
        confirmEmailField.validateType = .email
        
        // Валидация текстовых полей
        emailField.textField.rx.text.orEmpty
            .subscribe {[unowned self] _ in
                do {
                    _ = try self.emailField.validateText()
                    self.emailValid.accept(true)
                }
                catch { return self.emailValid.accept(false)}
            }.disposed(by: disposeBag)
        
        confirmEmailField.textField.rx.text.orEmpty
            .subscribe {[unowned self] _ in
                do {
                    _ = try self.confirmEmailField.validateText()
                    self.confirmValid.accept(true)
                }
                catch { return self.confirmValid.accept(false)}
            }.disposed(by: disposeBag)

        let everythingValid = Observable.combineLatest(emailValid, confirmValid) { [unowned self] in
            $0 && $1 && (self.emailField.textField.text == self.confirmEmailField.textField.text)
        }
        
        everythingValid
            .bind(to: saveButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        
        // События нажатия на return
        emailField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe { [unowned self] _ in
            self.confirmEmailField.textField.becomeFirstResponder()
            }.disposed(by: disposeBag)
        
        confirmEmailField.textField.rx.controlEvent([.editingDidEndOnExit]).subscribe { _ in }.disposed(by: disposeBag)
    
        // Событие тапа на кнопку
        saveButton.rx.tap
            .subscribe { [unowned self] (_) in
                self.viewModel.changeEmail(on: self.emailField.textField.text!)
            }.disposed(by: disposeBag)
        
        // Обработка результата запроса на измненение email
        viewModel.onEmailChanging
            .subscribe(onNext: { [unowned self]  (next) in
                if next {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.showWireframe(yesHandler: nil, noHandler: nil, title: "Ошибка", description: "Почта уже существует")
                }
            }).disposed(by: disposeBag)
    }
}
