//
//  ChangeEmailViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya

class ChangeEmailViewModel {
    
    private let networkProvider: myRxProvider<UserRequestAPI>
    private let emailChangingSuccess = PublishSubject<Bool>()
    private let disposeBag = DisposeBag()
    
    init() {
        self.networkProvider = myRxProvider()
    }
    
    var onEmailChanging: Observable<Bool> {
        return emailChangingSuccess
            .asObserver()
    }
    
    func changeEmail(on email: String) {
        networkProvider.request(.changeEmail(email))
            .subscribe { [unowned self] (response) in
                switch response {
                case .success(let result):
                    self.emailChangingSuccess.onNext(result.statusCode == 200)
                    UserCoreDataManager.shared.updateUserEmail(on: email)
                case .error(let error):
                    assertionFailure(error.localizedDescription)
                }
        }.disposed(by: disposeBag)
    }
}
