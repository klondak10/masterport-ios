//
//  ServiceCollectionViewCell.swift
//  Masterovik
//
//  Created by Haiduk Roman on 31.07.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit


class ServiceCollectionViewCell: UICollectionViewCell {
    
    static let identyfier = "ServiceCell"
    
    @IBOutlet weak var serviceNameLabel: UILabel!
    
    @IBOutlet weak var serviceImage: UIImageView!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(from model: ServiceModel) {
        
        DispatchQueue.main.async {
            
            self.serviceNameLabel.text = model.name
            guard let path = model.logo,
                let url = URL(string: path),
                let data = try? Data(contentsOf: url) else { self.serviceImage.image = #imageLiteral(resourceName: "question"); return }
            
            let image = UIImage(data: data)
            self.serviceImage.image = image
        }
    }
}
