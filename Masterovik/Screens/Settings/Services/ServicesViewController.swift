//
//  ServicesViewController.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/14/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ServicesViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var addServiceButton: AddButtonView!
    
    @IBOutlet weak var createServiceButton: AddButtonView!
    
    @IBOutlet weak var saveEditButton: NextButton!
    
    private let viewModel = ServiceViewModel()
    private let disposeBag = DisposeBag()
    
    private var allServices: Set<ServiceModel> = []
    
    private var userServices: Set<ServiceModel> = [] {
        didSet {
            refreshActionsResult()
            collectionView.reloadData()
        }
    }
    
    private var addedServices: Set<ServiceModel> = [] {
        didSet {
            refreshActionsResult()
            collectionView.reloadData()
        }
    }
    
    private var removeServices: Set<ServiceModel> = [] {
        didSet {
            refreshActionsResult()
            collectionView.reloadData()
        }
    }
    
    private var resultSet: [ServiceModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureRx()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ServiceCollectionViewCell.identyfier)
        collectionView.contentInset = UIEdgeInsets(top: CellConstantSize.topOffset,
                                                   left: CellConstantSize.leftOffset,
                                                   bottom: CellConstantSize.bottomOffset,
                                                   right: CellConstantSize.rightOffset)
    }
    
    private func configureRx() {
        
        UIView.showLoadIndicatiorOn(view: collectionView)
        
        //Открывает контроллер по созданию нового сервиса
        createServiceButton.button.rx.tap
            .subscribe{ [weak self] _ in self?.pushVC(AddServiceViewController.instanceFromStoryboard) }
            .disposed(by: disposeBag)
        
        //Получает данные о текущих сервисах пользователя
        viewModel.onUserServices.drive(onNext: { [unowned self] (services) in
            UIView.removeLoadIndicatorFrom(view: self.collectionView)
            self.userServices = Set(services)
        }).disposed(by: disposeBag)
        
        //Получает список всех сервисов
        viewModel.onServiceList.drive(onNext: {[unowned self] (list) in
            self.allServices = Set(list)
        }).disposed(by: disposeBag)
        
        //Открывает список всех сервисов
        addServiceButton.button.rx.tap
            .subscribe { [unowned self] _ in
            
                let setUserServices = self.userServices.union(self.addedServices)
            
                
                let result = self.allServices.subtracting(setUserServices)
                                            .union(self.removeServices)
                                            .map { $0.name }
                                            .sorted (by: >)
                
                if result.count > 0 {
                    self.showPopupTable(items: result, addHandler: { (list) in
                    
                   self.addedServices = self.addedServices.union(list.map { search in
                        self.allServices.first{ $0.name == search}!
                    })
                })
                } else {
                    self.showWireframe(yesHandler: nil, noHandler: nil, title: "", description: "У вас выбраны все возможные заявки.")
                }
                
        }.disposed(by: disposeBag)
        
        //Сохранение изменений
        saveEditButton.rx.tap
            .subscribe { [unowned self] (_) in
                self.viewModel.removeServices(servicesID: self.removeServices.map { $0.id })
                self.viewModel.addServices(servicesID: self.addedServices.map { $0.id })
        }.disposed(by: disposeBag)
    }
    
    private func refreshActionsResult() {
        self.resultSet = userServices
                            .subtracting(removeServices)
                            .union(addedServices)
                            .sorted(by: {$0.name > $1.name })
    }
}

extension ServicesViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return resultSet.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:
                    ServiceCollectionViewCell.identyfier, for: indexPath) as! ServiceCollectionViewCell
        
        cell.configure(from: resultSet[indexPath.row])
        
        //Подписка на события нажатия на кнопку удаления
        cell.deleteButton.rx.tap
            .subscribe { [weak self] (_) in
                if let item = self?.allServices.first(where: { $0.name == cell.serviceNameLabel.text!}) {
                    self?.removeServices.insert(item)
                }
        }.disposed(by: disposeBag)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CellConstantSize.width, height: CellConstantSize.height)
    }
}
