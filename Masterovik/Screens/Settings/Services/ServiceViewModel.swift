//
//  ServiceViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya
import SwiftyJSON

class ServiceViewModel {
    
    //Mark: Fields
    private let networkProvider: myRxProvider<ServiceRequestAPI>
    private let serviceList = BehaviorRelay<[ServiceModel]>.init(value: [])
    private let userServices = BehaviorRelay<[ServiceModel]>.init(value: [])
    private let addServicesSuccess = PublishSubject<Bool>()
    private let removeServicesSuccess = PublishSubject<Bool>()
    private let disposeBag = DisposeBag()
    
    //Mark: Init
    init() {
        self.networkProvider = myRxProvider()
        self.getServiceList()
        self.getUserServices()
    }
    
    //Mark: Properties
    var onServiceList: Driver<[ServiceModel]> {
        return serviceList
            .asObservable()
            .distinctUntilChanged{ $0.count == $1.count }
            .asDriver(onErrorJustReturn: [])
    }
    
    var onUserServices: Driver<[ServiceModel]> {
        return userServices
            .asObservable()
            .distinctUntilChanged{ $0.count == $1.count }
            .asDriver(onErrorJustReturn: [])
    }
    
    var onAddServicesSuccess: Observable<Bool> {
        return addServicesSuccess
            .asObservable()
    }
    
    var onRemoveServicesSuccess: Observable<Bool> {
        return removeServicesSuccess
            .asObservable()
    }
    
    //Mark: Methods
    func getServiceList(){
        networkProvider.request(.getAllServices)
            .subscribe(onSuccess: { [unowned self] (result) in
                do {
                    let json = try JSON(data: result.data)
                    let list = json["response"].array!.map { ServiceModel(from: $0)}
                    self.serviceList.accept(list)
                } catch {
                    self.serviceList.accept([])
                }
            }).disposed(by: disposeBag)
    }
    
    func getUserServices() {
        networkProvider.request(.getUserServices)
            .subscribe(onSuccess: { [unowned self] (result) in
                do {
                    let json = try JSON(data: result.data)
                    let list = json["response"].array!.map { ServiceModel(from: $0)}
                    self.userServices.accept(list)
                }
                catch {
                    print("Сервисы пользователя не были конвертированы")
                    self.serviceList.accept([])
                }
            }).disposed(by: disposeBag)
    }
    
    func addServices(servicesID: [Int]) {
        networkProvider.request(.addServices(servicesID: servicesID))
            .subscribe { [unowned self] (response) in
                switch response {
                case .success(let result):
                    if result.statusCode == 200 {
                        self.addServicesSuccess.onNext(true)
                    } else { self.addServicesSuccess.onNext(false)}
                case .error:
                    self.addServicesSuccess.onNext(false)
                }
            }.disposed(by: disposeBag)
    }
    
    func removeServices(servicesID: [Int]) {
        networkProvider.request(.removeServices(servicesID: servicesID))
            .subscribe { [unowned self] (response) in
                switch response {
                case .success(let result):
                    if result.statusCode == 200 {
                        self.removeServicesSuccess.onNext(true)
                    } else { self.removeServicesSuccess.onNext(false)}
                case .error:
                    self.removeServicesSuccess.onNext(false)
                }
            }.disposed(by: disposeBag)
    }
}
