//
//  PersonalDataChangingViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya
import RxSwift
import RxCocoa
import SwiftyJSON

class PersonalDataChanchingViewModel {
    
    private let provider: myRxProvider<UserRequestAPI>
    
    private let personalDataChanchingSuccess = PublishSubject<Bool>()
    private let cityList = PublishSubject<[CityModel]>()
    
    private let disposeBag = DisposeBag()
    
    var onCityList: Observable<[CityModel]> {
        return cityList
            .asObservable()
    }
    
    var onPersonalDataChangingSuccess: Observable<Bool> {
        return personalDataChanchingSuccess
            .asObservable()
    }
    
    init() {
        self.provider = myRxProvider()
    }
    
    func changePersonalData(firstName: String, lastName: String, cityId: Int,
                            companyName: String, companyDescription: String) {
        provider.request(.changePersonalData(firstName: firstName, lastName: lastName, cityId: cityId, companyName: companyName, companyDescription: companyDescription))
            .subscribe(onSuccess: { [unowned self] (response) in
                if response.statusCode == 200 {
                    self.personalDataChanchingSuccess.onNext(true)
                } else {
                    self.personalDataChanchingSuccess.onNext(false)
                }
            }).disposed(by: disposeBag)
    }
    
    func getCityList() {
       provider.request(.cityList).subscribe { [weak self] (response) in
            guard let `self` = self else { return }
            switch response {
            case .success(let result):
                do {
                    
                    let json = try JSON(data: result.data)
                    let list = json["response"].arrayValue.map { CityModel(from: $0) }
                    self.cityList.onNext(list)
                } catch {
                    assertionFailure("Ошибка преобразования из Data в [CityModel]")
                }
                
            case .error(let error):
                assertionFailure(error.localizedDescription)
                self.cityList.onNext([])
            }
            }.disposed(by: disposeBag)
    }
}
