//
//  PersonalDataViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/24/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PersonalDataViewController: BaseViewController {
    
    @IBOutlet weak var nameField: LabeledFieldView! {
        didSet {
            nameField.validateType = .userName
        }
    }
    @IBOutlet weak var surnameField: LabeledFieldView! {
        didSet {
            surnameField.validateType = .userName
        }
    }
    @IBOutlet weak var cityField: DropDownFieldView!
    @IBOutlet weak var companyField: LabeledFieldView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var nextButton: NextButton!
    @IBOutlet var tagCollectionsForReturn: [LabeledFieldView]!
   
    
    private let viewModel = PersonalDataChanchingViewModel()
    private let disposeBag = DisposeBag()
    private var cityList: [CityModel] = [] {
        didSet {
            cityField.setCityList = cityList
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getCityList()
        configureUI()
        configureRx()
    }
    
    private func configureUI() {
        descriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
        descriptionTextView.font = FontHelper.font(type: .roboto, size: .small)
        descriptionTextView.setRoundedRect()
        nextButton.backgroundColor = .darkGreen
       
        descriptionTextView.addDoneOnKeyboardWithTarget(self, action: #selector(self.doneAction(_:)), shouldShowPlaceholder: true)
        
        addActionOnReturnKey()
    }
    
    @objc func doneAction(_ sender : UITextField!) {
        self.view.endEditing(true)
    }
    
    private func configureRx() {
        
        disposeBag.insert(
            
            companyField.textField.rx.text.orEmpty
                .subscribe(onNext: { [weak self] text in
                    if text.count > 25 { self?.companyField.textField.text!.removeLast() }
                }),
            
            viewModel.onCityList.subscribe(onNext: { [unowned self] (list) in
                self.cityList = list
                
                guard let user = UserCoreDataManager.shared.getUser() else { return }
                
                self.nameField.textField.text = user.firstName
                self.surnameField.textField.text = user.lastName
                self.cityField.textField.text = self.cityList.first { $0.cityId == user.cityId }!.cityName
                self.companyField.textField.text = user.companyName
                self.descriptionTextView.text = user.companyDescription ?? ""
            }),
            
            nextButton.rx.tap
                .observeOn(MainScheduler.instance)
                .subscribe { [unowned self] (_) in
                    /// 1 проверка
                    if self.checkValidation(textField: self.nameField) &&
                        self.checkValidation(textField: self.surnameField) {
                        /// 2 проверка
                        guard let city  = self.cityList.first(where: { (city) -> Bool in
                            city.cityName == self.cityField.textField.text ?? ""
                        })
                            else {
                                self.cityField.shakeAnimation()
                                return
                        }
                        /// отправка запроса
                        self.viewModel.changePersonalData(firstName: self.nameField.textField.text!,
                                                          lastName: self.surnameField.textField.text!,
                                                          cityId: city.cityId,
                                                          companyName: self.companyField.textField.text!,
                                                          companyDescription: self.descriptionTextView.text)
                    } else {
                        return
                    }
            },
            
            viewModel.onPersonalDataChangingSuccess
                .subscribe(onNext: { [unowned self] (event) in
                    if event {
                        UserCoreDataManager.shared.updateUserName(firstName: self.nameField.textField.text!,
                                                                  lastName: self.surnameField.textField.text!)
                        if let city  = self.cityList.first(where: { (city) -> Bool in
                            city.cityName == self.cityField.textField.text ?? ""
                        }) {
                            UserCoreDataManager.shared.updateUserCity(on: city.cityId)
                        }
                        UserCoreDataManager.shared.updateUserCompanyInfo(on: self.companyField.textField.text!,
                                                                         description: self.descriptionTextView.text)
                        self.navigationController?.popViewController(animated: true)
                        
                    } else {
                        self.showWireframe(yesHandler: nil, noHandler: nil,
                                           title: "Ошибка!", description: "Ошибка обновления данных.")
                    }
                })
        )
    }
    
    private func addActionOnReturnKey() {
        tagCollectionsForReturn.forEach { (item) in
            
             item.textField.rx.text.orEmpty
                .subscribe({ (_) in
                    if item.textField.text?.count ?? 0 > 25 {
                         item.textField.text?.removeLast()
                    }
                }).disposed(by: disposeBag)
            
            item.textField.rx.controlEvent([.editingDidEndOnExit])
                .subscribe { _ in
                    let nextTag = item.tag + 1
                    
                    if let nextResponder = item.superview?.viewWithTag(nextTag) {
                        nextResponder.becomeFirstResponder()
                    } else {
                        item.resignFirstResponder()
                    }
                }.disposed(by: disposeBag)
        }
    }
    
    private func checkValidation(textField: LabeledFieldView) -> Bool {
        guard textField.validateType != nil else { return false }
        do {
            _ = try textField.validateText()
            return true
        } catch {
            textField.shakeAnimation()
            return false }
    }
}
