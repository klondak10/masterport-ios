//
//  AddServiceViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/25/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AddServiceViewController: BaseViewController {
    
    @IBOutlet weak var serviceNameField: LabeledFieldView!
    
    @IBOutlet weak var shortDescriptionField: LabeledFieldView!
    
    @IBOutlet weak var addReasonTextView: UITextView!
    
    @IBAction func tapOnView(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @IBOutlet weak var sendRequestButton: UIButton!
    
    private let viewModel = AddServiceViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendRequestButton.isEnabled = false
        
        // Обработка кнопки return
         serviceNameField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe { [unowned self] _ in
                self.shortDescriptionField.becomeFirstResponder()
            }
            .disposed(by: disposeBag)
        shortDescriptionField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe { [unowned self] _ in
                self.addReasonTextView.becomeFirstResponder()
            }
            .disposed(by: disposeBag)
        serviceNameField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe { [unowned self] _ in self.serviceNameField.resignFirstResponder()}
            .disposed(by: disposeBag)
        
        // Валидация полей
        let nameValid = serviceNameField.textField.rx.text.orEmpty
            .map { $0.count > 5 && $0.count < 20  }
            .share(replay: 1)
        
        let descriptionValid = shortDescriptionField.textField.rx.text.orEmpty
            .map { $0.count > 5 && $0.count < 50  }
            .share(replay: 1)
        
        let reasonValid = addReasonTextView.rx.text.orEmpty
            .map { $0.count > 5 && $0.count < 2000  }
            .share(replay: 1)
        
        let everythingValid = Observable
            .combineLatest(nameValid, descriptionValid, reasonValid) {
                $0 && $1 && $2
            }.share(replay: 1)
        
        everythingValid
            .bind(to: sendRequestButton.rx.isUserInteractionEnabled)
            .disposed(by: disposeBag)
        
        //Обработка нажатия на кнопку
        sendRequestButton.rx.tap
            .subscribe(onNext: { [unowned self] (_) in
                self.viewModel.createService(name: self.serviceNameField.textField.text!,
                                             description: self.shortDescriptionField.textField.text!,
                                             reason: self.addReasonTextView.text)
            }).disposed(by: disposeBag)
        
        //Результат запроса
        viewModel.onRequestSuccess
            .subscribe(onNext: { [unowned self] (event) in
                if event {
                    self.showWireframe(yesHandler: nil,
                                       noHandler: nil,
                                       title: "",
                                       description: "Письмо успешно отправленно!",
                                       okButtonHandler: { [weak self] in
                                        self?.navigationController?.popViewController(animated: true)})
                } else {
                    self.showWireframe(yesHandler: nil,
                                       noHandler: nil,
                                       title: "Ошибка!",
                                       description: "Запрос не был отправлен")
                }
            }).disposed(by: disposeBag)
    }
}
