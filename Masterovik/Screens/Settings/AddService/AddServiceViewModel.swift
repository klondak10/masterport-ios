//
//  AddServiceViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya

class AddServiceViewModel {
    
    private let networkProvider: myRxProvider<ServiceRequestAPI>
    private let requestSuccess = PublishSubject<Bool>()
    private let disposeBag = DisposeBag()
    
    init() {
        self.networkProvider = myRxProvider()
    }
    
    var onRequestSuccess: Observable<Bool> {
        return requestSuccess
            .asObservable()
    }
    
    func createService(name: String, description: String, reason: String) {
        networkProvider.request(.createService(name: name, description: description, reason: reason))
            .subscribe { [unowned self] (response) in
                switch response {
                case .success(let result):
                    self.requestSuccess.onNext(result.statusCode == 200)
                case .error(let error):
                    assertionFailure(error.localizedDescription)
                }
            }.disposed(by: disposeBag)
    }
}

