//
//  RegistrationViewModel.swift
//  Masterovik
//
//  Created by Roman Haiduk on 8/7/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON
import Moya

class RegistrationViewModel {
    
    private let registrationSuccessful = PublishSubject<(suc: Bool,err: String?)> ()
    
    private let cityList = BehaviorRelay<[CityModel]>(value: [])
    
    private let userRegProvider: MoyaProvider<UserRequestAPI>
    private let getUserViewModel = SignInViewModel()
    
    private let disposeBag = DisposeBag()
    
    var onRegistrationSuccessful: Observable<(suc: Bool,err: String?)> {
        return registrationSuccessful
            .asObservable()
    }
    
    var onCityList: Observable<[CityModel]> {
        return cityList
            .asObservable()
    }
    
    init() {
        userRegProvider = MoyaProvider<UserRequestAPI>.init(plugins: [NetworkLoggerPlugin(verbose: true)])
        getCityList()
    }
    
    func registration(_ user: Personable) {

        userRegProvider.rx.request(.createUser(firstName: user.firstName,
                                               lastName: user.lastName,
                                               email: user.email,
                                               cityID: user.cityId,
                                               companyName: currentRoleIsExecutor ? (user as? Executor)?.companyName : nil ,
                                               companyDescription: currentRoleIsExecutor ? (user as? Executor)?.companyDescription : nil,
                                               phone: user.phone,
                                               roleID: user.roleId!,
                                               invitedBy: user.invitedBy))
            .subscribe { [weak self] (response) in
                guard let `self` = self else { return }
                switch response {
                case .success(let result):
                        if let json = try? JSONSerialization.jsonObject(
                            with: result.data, options: .mutableContainers) as? [String:String] {
                            
                            guard let errMessage = json["error_message"] else { self.registrationSuccessful.onNext((suc: false, err: "Ошибка регистрации!"))
                                return
                            }
                            if errMessage == "" {
                                TokenManager.changeAccess(token:  json["access_token"]!)
                                TokenManager.changeRefreshToken(token:  json["refresh_token"]!)
                                
                                self.getUserViewModel.getUserData()
                                return
                                
                            } else if errMessage.contains("email") {
                                self.registrationSuccessful.onNext((suc:false, err: "Пользователь с таким email адресом уже зарегистрирован."))
                                return
                            }
                    }
                    self.registrationSuccessful.onNext((suc: false, err: "Ошибка регистрации!"))
                case .error(let error):
                    self.registrationSuccessful.onError(error)
                    
                }
            }.disposed(by: disposeBag)
    }
    
    func getCityList() {
        
        userRegProvider.rx.request(.cityList).subscribe { [weak self] (response) in
            guard let `self` = self else { return }
            switch response {
            case .success(let result):
                do {
 
                    let json = try JSON(data: result.data)
                    let list = json["response"].arrayValue.map { CityModel(from: $0) }
                    self.cityList.accept(list)
                } catch {
                    print("Ошибка преобразования из Data в [CityModel]")
                }
                
            case .error(let error):
                print(error.localizedDescription)
                self.cityList.accept([])
            }
        }.disposed(by: disposeBag)
    }
}

