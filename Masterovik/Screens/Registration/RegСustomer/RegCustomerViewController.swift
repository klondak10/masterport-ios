//
//  RegCustomerViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import WebKit
import IQKeyboardManagerSwift
import RxSwift
import RxCocoa

class RegCustomerViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet var labeledFields: [UIView]! /*{
        didSet {
            labeledFields.forEach {
                if let labeled = $0 as? LabeledFieldView {
                    labeled.getTextField().delegate = self
                }
            }
        }
    }*/
    @IBOutlet weak var firstName: LabeledFieldView!
    @IBOutlet weak var lastName: LabeledFieldView!
    @IBOutlet weak var emailField: LabeledFieldView!
    @IBOutlet weak var cityField: DropDownFieldView!
    @IBOutlet weak var checkmarkButton: UIButton!
    @IBOutlet weak var licenseButton: UIButton!
    @IBOutlet weak var licenseView: UIView!
    @IBOutlet weak var nextButton: NextButton!
    
    private var acceptedLicense: Bool = false {
        didSet {
            setupCheckBox(acceptedLicense)
        }
    }
    private var phoneNumber: String?
    private var viewModel = RegistrationViewModel()
    private var disposeBag = DisposeBag()
    
    private let maxLength = 25
    private var tempUser: Executor?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureRx()
        
       
    }
    

    
    private func configureUI() {
        var attributes: [NSAttributedString.Key : Any] = [:]
        attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
        attributes[.font] = FontHelper.font(type: .franklin, size: .small)
        let underlinedString = NSAttributedString(string: licenseButton.titleLabel?.text ?? "", attributes: attributes)
        licenseButton.setAttributedTitle(underlinedString, for: .normal)
        
        firstName.validateType = .userName
        lastName.validateType = .userName
        emailField.validateType = .email
        
        emailField.textField.keyboardType = .emailAddress
    }
    
    private func configureRx(){
        
        ///View Model Get Cities
        viewModel.onCityList.subscribe(onNext:{ [unowned self ] cityList in
            self.cityField.setCityList = cityList
        }).disposed(by: disposeBag)
        
        ///View Model Register result
        viewModel.onRegistrationSuccessful.observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (result) in
                UIView.removeLoadIndicatorFrom(view: self.view)
                if result.suc {
                    
                } else {
                    self.showWireframe(yesHandler: nil, noHandler: nil, title: "Ошибка", description: result.err!)
                }
            }).disposed(by: disposeBag)
        
        /// CheckBox subscribe
        checkmarkButton.rx.tap.subscribe {[unowned self] (_) in
            self.acceptedLicense = !self.acceptedLicense
            }.disposed(by: disposeBag)
        
        
        ///TextField Change Text Subscribe
        firstName.textField.rx.text.orEmpty
            .subscribe(onNext: { [unowned self] text in
                if text.count > self.maxLength { self.firstName.textField.text!.removeLast() }
            }).disposed(by: disposeBag)
        
        lastName.textField.rx.text.orEmpty
            .subscribe(onNext: { [unowned self] text in
                if text.count > self.maxLength { self.lastName.textField.text!.removeLast() }
            }).disposed(by: disposeBag)
        
        cityField.textField.rx.text.orEmpty
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] text in
                if text.count > self.maxLength { self.cityField.textField.text!.removeLast() }
            }).disposed(by: disposeBag)
        
        emailField.textField.rx.controlEvent([.editingDidEndOnExit]).subscribe { _ in }.disposed(by: disposeBag)
        
        
        /// Button Next Subscribe
        nextButton.rx.tap
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] in
                if self.checkTextFieldWithShake(self.firstName) &&
                    self.checkTextFieldWithShake(self.lastName) {
                    if  !self.cityField.setCityList.contains(where: { (city) -> Bool in
                        
                        city.cityName == self.cityField.textField.text
                    }) {
                        _ = self.cityField.shakeAnimation()
                        return
                    }
                    if self.emailField.textField.text! != "" {
                        if !self.checkTextFieldWithShake(self.emailField) {
                            return
                        }
                    }
                    let cityId = self.cityField.setCityList.first {
                        $0.cityName == self.cityField.textField.text
                        }!
                    
                    if !self.acceptedLicense {
                        self.licenseView.backgroundColor = UIColor.red.withAlphaComponent(0.5)
                        self.licenseView.cornerRadius = 5.0
                        return
                    }
                    
                    let user = Executor(id: 0, cityId: cityId.cityId, firstName: self.firstName.textField.text!, lastName: self.lastName.textField.text!, phone: self.phoneNumber!, balance: nil, roleId: 2, staticticsId: nil, email: self.emailField.textField.text, invite: nil, invitedBy: nil, companyName: nil, companyDescription: nil, createdAt: nil)
                    self.tempUser = user
                    UIView.showLoadIndicatiorOn(view: self.view)
                    self.viewModel.registration(user)
                }
            }).disposed(by: disposeBag)
    }
    
    
    func setPhoneNumber(with number: String) {
        self.phoneNumber = number
    }
    
    private func checkTextFieldWithShake(_ textfield: LabeledFieldView) -> Bool {
        do {
            _ = try textfield.validateText()
            textfield.layer.borderColor = UIColor.lightGrayText.cgColor
            return true
        } catch {
            textfield.shakeAnimation()
            textfield.layer.borderColor = UIColor.warningRed.cgColor
            return false
        }
    }
    
    fileprivate func setupCheckBox(_ isOn: Bool) {
        if isOn {
            let checkmarkImage = UIImage(named: "ok-icon-black-vector")
            checkmarkButton.contentMode = .scaleAspectFit
            checkmarkButton.tintColor = .black
            checkmarkButton.setImage(checkmarkImage, for: .normal)
            licenseView.backgroundColor = .white
        } else {
            checkmarkButton.setImage(nil, for: .normal)
            checkmarkButton.backgroundColor = .clear
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func touchCheckmarkButton(_ sender: Any) {
        
    }
    @IBAction func touchLicenseButton(_ sender: Any) {
      //  let webView = WKWebView()
    }
}

