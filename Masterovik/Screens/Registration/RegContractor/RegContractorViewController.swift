//
//  RegContractorViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import WebKit
import RxSwift
import RxCocoa

class RegContractorViewController: BaseViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet var labeledFields: [UIView]! /*{
        didSet {
            labeledFields.forEach {
                if let labeled = $0 as? LabeledFieldView {
                labeled.getTextField().delegate = self }
            }
        }
    }*/
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstName: LabeledFieldView!
    @IBOutlet weak var lastName: LabeledFieldView!
    @IBOutlet weak var emailField: LabeledFieldView!
    @IBOutlet weak var cityField: DropDownFieldView!
    @IBOutlet weak var companyField: LabeledFieldView!
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.font = FontHelper.font(type: .franklin, size: .small)
        }
    }
    
    @IBOutlet weak var descriptionTextView: UITextView! {
        didSet {
            descriptionTextView.delegate = self
            descriptionTextView.setRoundedRect()
            descriptionTextView.font = FontHelper.font(type: .roboto, size: .small)
        }
    }
    
    @IBOutlet weak var checkInviteCodeButton: UIButton!
    @IBOutlet weak var checkResultImageView: UIImageView!
    @IBOutlet weak var checkResultLabel: UILabel!
    @IBOutlet weak var licenseContentView: UIView!
    @IBOutlet weak var licenseCheckmarkButton: UIButton!
    @IBOutlet weak var licenseTextButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var checkResultHeightConstraint: NSLayoutConstraint!
    
//    var correctInviteCode: Bool = false
    var acceptedLicense: Bool = false {
        didSet {
            setupCheckBox(acceptedLicense)
        }
    }
    
    private var phoneNumber: String?
    private var viewModel = RegistrationViewModel()
    private var disposeBag = DisposeBag()
    
    private let maxLength = 25
    private var tempUser: Executor?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureRx()
    }
    
    private func configureUI() {
        var attributes: [NSAttributedString.Key : Any] = [:]
        attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
        attributes[.font] = FontHelper.font(type: .franklin, size: .small)
        let underlinedCheckInvite = NSAttributedString(string: checkInviteCodeButton.titleLabel?.text ?? "", attributes: attributes)
        checkInviteCodeButton.setAttributedTitle(underlinedCheckInvite, for: .normal)
        if checkResultHeightConstraint != nil {
            checkResultHeightConstraint.constant = 0
        }
        let underlinedLicense = NSAttributedString(string: licenseTextButton.titleLabel?.text ?? "", attributes: attributes)
        licenseTextButton.setAttributedTitle(underlinedLicense, for: .normal)
        
        firstName.validateType = .userName
        lastName.validateType = .userName
        emailField.validateType = .email
        companyField.validateType = .userName
        
        emailField.textField.keyboardType = .emailAddress
    }
    
    private func configureRx() {
        
        ///View Model Get Cities
        viewModel.onCityList.subscribe(onNext:{ [unowned self ] cityList in
            self.cityField.setCityList = cityList
        }).disposed(by: disposeBag)
        
        ///View Model Register result
        viewModel.onRegistrationSuccessful.observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (result) in
            UIView.removeLoadIndicatorFrom(view: self.view)
            if result.suc {
              
            } else {
                self.showWireframe(yesHandler: nil, noHandler: nil, title: "Ошибка", description: result.err!)
            }
        }).disposed(by: disposeBag)
        
        /// CheckBox subscribe
        licenseCheckmarkButton.rx.tap.subscribe {[unowned self] (_) in
            self.acceptedLicense = !self.acceptedLicense
        }.disposed(by: disposeBag)
        
        
        ///TextField Change Text Subscribe
       firstName.textField.rx.text.orEmpty
        .subscribe(onNext: { [unowned self] text in
            if text.count > self.maxLength { self.firstName.textField.text!.removeLast() }
        }).disposed(by: disposeBag)
        
        lastName.textField.rx.text.orEmpty
            .subscribe(onNext: { [unowned self] text in
                if text.count > self.maxLength { self.lastName.textField.text!.removeLast() }
            }).disposed(by: disposeBag)
        
        cityField.textField.rx.text.orEmpty
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] text in
                if text.count > self.maxLength { self.cityField.textField.text!.removeLast() }
            }).disposed(by: disposeBag)
        
        companyField.textField.rx.text.orEmpty
            .subscribe(onNext: { [unowned self] text in
                if text.count > self.maxLength { self.companyField.textField.text!.removeLast() }
            }).disposed(by: disposeBag)
        
        emailField.textField.rx.controlEvent([.editingDidEndOnExit]).subscribe { _ in }.disposed(by: disposeBag)
        
        
        /// Button Next Subscribe
        nextButton.rx.tap
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] in
                if self.checkTextFieldWithShake(self.firstName) &&
                    self.checkTextFieldWithShake(self.lastName) {
                    if  !self.cityField.setCityList.contains(where: { (city) -> Bool in
                        
                        city.cityName == self.cityField.textField.text
                    }) {
                        _ = self.cityField.shakeAnimation()
                        return
                    }
                    if self.emailField.textField.text! != "" {
                        if !self.checkTextFieldWithShake(self.emailField) {
                            return
                        }
                    }
                    let cityId = self.cityField.setCityList.first {
                        $0.cityName == self.cityField.textField.text
                    }!
                    
                    if !self.acceptedLicense {
                        self.licenseContentView.backgroundColor = UIColor.red.withAlphaComponent(0.5)
                        self.licenseContentView.cornerRadius = 5.0
                            return
                    }
                    
                    let user = Executor(id: 0, cityId: cityId.cityId, firstName: self.firstName.textField.text!, lastName: self.lastName.textField.text!, phone: self.phoneNumber!, balance: nil, roleId: 1, staticticsId: nil, email: self.emailField.textField.text, invite: nil, invitedBy: nil, companyName: self.companyField.textField.text, companyDescription: self.descriptionTextView.text, createdAt: nil)
                    self.tempUser = user
                    UIView.showLoadIndicatiorOn(view: self.view)
                    self.viewModel.registration(user)
                }
            }).disposed(by: disposeBag)
    }
    
    
    private func checkTextFieldWithShake(_ textfield: LabeledFieldView) -> Bool {
        do {
            _ = try textfield.validateText()
            textfield.layer.borderColor = UIColor.lightGrayText.cgColor
            return true
        } catch {
            textfield.shakeAnimation()
            textfield.layer.borderColor = UIColor.warningRed.cgColor
            return false
        }
    }
    
    func setPhoneNumber(with number: String) {
        self.phoneNumber = number

    }
    
//    func checkInviteCode(code: String) {
//        correctInviteCode = true
//        if correctInviteCode {
//            checkResultImageView.tintColor = .darkGreen
//            checkResultLabel.text = "Код введен верно"
//            checkResultLabel.textColor = .darkGreen
//        } else {
//            checkResultImageView.tintColor = .red
//        }
//        if checkResultHeightConstraint != nil {
//            checkResultHeightConstraint.isActive = false
//        }
//    }
    

    // MARK: - IBActions
    
    @IBAction func touchCheckInviteButton(_ sender: Any) {
//        let alert = UIAlertController(title: "Код приглашения пользователя", message: nil, preferredStyle: .alert)
//        
//        alert.addTextField(configurationHandler: nil)
//        let textField = alert.textFields?.first
//        let checkCodeAction = UIAlertAction(title: "Продолжить", style: .default, handler: { (action) in
//            if let text = textField?.text {
//                self.checkInviteCode(code: text)
//            }
//        })
//        alert.addAction(checkCodeAction)
//        
//        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func setupCheckBox(_ isOn: Bool) {
        if isOn {
            let checkmarkImage = UIImage(named: "ok-icon-black-vector")
            licenseCheckmarkButton.contentMode = .scaleAspectFit
            licenseCheckmarkButton.tintColor = .black
            licenseCheckmarkButton.setImage(checkmarkImage, for: .normal)
            licenseContentView.backgroundColor = .white
        } else {
            licenseCheckmarkButton.setImage(nil, for: .normal)
            licenseCheckmarkButton.backgroundColor = .clear
        }
    }

    
    @IBAction func touchLicenseTextButton(_ sender: Any) {
       // let webView = WKWebView()
    }
}
