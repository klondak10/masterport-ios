//
//  ChooseRoleViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

enum Role {
    case none
    case contractor
    case customer
}

class ChooseRoleViewController: BaseViewController {
    
    @IBOutlet weak var chooseLabel: UILabel! {
        didSet {
            chooseLabel.font = FontHelper.font(type: .franklin, size: .medium)
        }
    }
    
    @IBOutlet weak var selectContractorView: CodeExpandingDescriptionView! {
        didSet {
            selectContractorView.button.titleLabel?.textColor = .darkGreen
            selectContractorView.button.addTarget(self, action: #selector(touchSelectContractorButton(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var selectCustomerView: CodeExpandingDescriptionView! {
        didSet {
            selectCustomerView.button.titleLabel?.textColor = .darkGreen
            selectCustomerView.button.addTarget(self, action: #selector(touchSelectCustomerButton(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var nextButton: NextButton!
    
    private var role: Role = .none
    private var phoneNumber: String?
    
    func setPhoneNumber(with number: String) {
        self.phoneNumber = number
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        navigationItem.hidesBackButton = true
        
        nextButton.isGreenButton = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = true
    }
    
    //MARK: - @objc button actions
    
    @objc func touchSelectContractorButton(_ sender: UIButton) {
        role = .contractor
        selectContractorView.toggle(expanded: true)
        selectCustomerView.toggle(expanded: false)
        nextButton.isUserInteractionEnabled = true
    }
    
    @objc func touchSelectCustomerButton(_ sender: UIButton) {
        role = .customer
        selectContractorView.toggle(expanded: false)
        selectCustomerView.toggle(expanded: true)
        nextButton.isUserInteractionEnabled = true
    }
    
    // MARK: - IBActions
    
    @IBAction func touchNextButton(_ sender: Any) {
        guard let phoneNumber = phoneNumber else { showWireframe(yesHandler: nil,
                                                                 noHandler: nil,
                                                                 title: "Ошибка",
                                                                 description: "Номер телефона отсутствует в экземпляре 'ChooseRoleViewController")
                                                    return
        }
        
        navigationItem.hidesBackButton = false
        if role == .contractor {
            let regVC = RegContractorViewController.instanceFromStoryboard as! RegContractorViewController
            regVC.setPhoneNumber(with: phoneNumber)
            navigationController?.pushViewController(regVC, animated: true)
        } else if role == .customer {
            let regVC = RegCustomerViewController.instanceFromStoryboard as! RegCustomerViewController
            regVC.setPhoneNumber(with: phoneNumber)
            navigationController?.pushViewController(regVC, animated: true)
        }
    }
}
