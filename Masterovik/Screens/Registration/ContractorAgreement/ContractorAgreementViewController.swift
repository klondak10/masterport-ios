//
//  ContractorAgreementViewController.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ContractorAgreementViewController: BaseViewController {
  
    @IBOutlet weak var acceptButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        contractLabel.font = FontHelper.font(type: .roboto, size: .small)
    }
    
    @IBAction func touchAcceptButton(_ sender: Any) {
        pushVC(OrdersTabViewController())
    }
}
